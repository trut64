/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "avrusb.h"

#include "fcpu.h"
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>

#include "debugleds.h"
#include "common.h"
#include "usbdescriptors.h"
#include "dbuf.h"

#define usb_ep_size(size) (size == 8 ? 0 : (size == 16 ? 1 : (size == 32 ? 2 : (size == 64 ? 3 : -1))))

#define EP_CONTROL     0
#define EP_ISOCHRONOUS 1
#define EP_BULK        2
#define EP_INTERRUPT   3

#define EP_OUT 0 // host-to-device
#define EP_IN  1 // device-to-host

// Build endpoint configuration 0
// type is the endpoint type:
//   0 for control
//   1 for isochronous
//   2 for bulk
//   3 for interrupt
// dir is host-centric direction of pipe:
//   0 for OUT/control (host-to-device)
//   1 for IN          (device-to-host)
#define usb_build_epcfg0(type, dir) (((type) << EPTYPE0) | (dir))

#define EP_SINGLEBUF 0
#define EP_DOUBLEBUF 1

// Build endpoint configuration 1
// size is the endpoint buffer size: 8, 16, 32 or 64
// banks is number of banks:
//   0 for single-buffering
//   1 for double-buffering
#define usb_build_epcfg1(size, banks) ((usb_ep_size(size) << EPSIZE0) | ((banks) << EPBK0))

static inline uint8_t usb_ep_hasinterrupt(uint8_t ep) { return UEINT & _BV(ep); }
static inline uint8_t usb_cansend() { return UEINTX & _BV(TXINI); }
static inline uint8_t usb_packetreceived() { return UEINTX & _BV(RXOUTI); }
static inline uint8_t usb_setupreceived() { return UEINTX & _BV(RXSTPI); }
static inline void usb_ackpacketreceived() { UEINTX &= ~_BV(RXOUTI); }
static inline void usb_acksetupreceived() { UEINTX &= ~_BV(RXSTPI); }
static inline void usb_sendsetuppacket() { UEINTX &= ~_BV(TXINI); }

// Wait for status packet to arrive, and clear the interrupt flag to ack it.
static inline void usb_recvsetupstatus()
{
	while(!usb_packetreceived());
	usb_ackpacketreceived();
}

// Send status packet, and wait for host to acknowledge.
static inline void usb_sendsetupstatus()
{
	usb_sendsetuppacket();
	while(!usb_cansend());
}

static inline uint8_t usb_txini() { return UEINTX & _BV(TXINI); }
static inline void usb_clearrxouti() { UEINTX = (uint8_t) ~_BV(RXOUTI); }

#if HWBOARD==DEVTRUT
static inline uint8_t usb_bytecount()
{
	return UEBCLX;
}
#else
static inline uint16_t usb_bytecount()
{
	return (UEBCHX << 8) | UEBCLX;
}
#endif

#define TRUT_DATAMODE_LOAD 0
#define TRUT_DATAMODE_DUMP 1

#define USB_STATE_INIT 0             // USB device is initialized.
#define USB_STATE_DEFAULT 1          // USB device has been reset and answers at address 0.
#define USB_STATE_ADDRESS 2          // USB device has been assigned a unique address.
#define USB_STATE_CONFIGURED 3       // USB device has been configured.
uint8_t usb_state;
uint8_t usb_if1setting;

uint8_t usb_error;

static void usb_setup_request();
static void usb_configure_ep(uint8_t ep, uint8_t cfg0, uint8_t cfg1);
static void usb_configure_control_ep();
static void usb_configure_comm_interface();
static void usb_configure_data_interface(uint8_t altnum);
static void usb_unconfigure_ep(uint8_t ep);
static void usb_unconfigure_comm_interface();
static void usb_unconfigure_data_interface();

static void usb_sendsetupdata_P(PGM_P data, uint8_t count);
static void usb_sendsetupdata(uint8_t* data, uint8_t count);

static void usb_readdata_fix(uint8_t* dst, uint8_t count);
uint8_t* usb_writedata_fix(uint8_t* src, uint8_t count);
PGM_P usb_writedata_fix_P(PGM_P src, uint8_t count);

void usb_init()
{
	usb_error = 0;

    // Do the correct initialization depending on the hardware used.
#if HWBOARD==DEVTRUT
	// Enable PLL with 8 MHz reference clock.
	PLLCSR = (1 << PLLE);

	// Wait for PLL to acquire lock.
	while((PLLCSR & (1 << PLOCK)) == 0);

	// Enable USB macro.
	USBCON = (1 << USBE);

	// Start USB clock. FRZCLK is apparently set when USBE is set, but this
    // is not documented in the datasheet.
	USBCON &= ~(1 << FRZCLK);

    // Reset all endpoints.
	UERST = 0x1f;

    // Configure control endpoint.
	usb_configure_control_ep();

	// Attach USB device.
    UDCON = 0;

	// Enable end of reset interrupt.
	UDIEN = (1 << EORSTE);

	// Set default state.
    usb_state = USB_STATE_DEFAULT;
#else
	// Enable pad regulator and set usb device mode.
	UHWCON = (1 << UVREGE) | (1 << UIMOD);

	// Bring up the usb controller and enable VBUS interrupt.
	USBCON = (1 << USBE) | (1 << FRZCLK) | (1 << OTGPADE) | (1 << VBUSTE);

	// Set initialized state.
	usb_state = USB_STATE_INIT;
#endif
}

void usb_disable()
{
    // Detach USB device.
	UDCON = (1 << DETACH);

    // Disable USB macro and freeze clock.
	USBCON = (1 << FRZCLK);

	// Disable PLL.
	PLLCSR = 0;
}

ISR(USB_GEN_vect)
{
#if HWBOARD!=DEVTRUT
	if(USBINT & (1 << VBUSTI))            // VBUS transition.
	{
		USBINT = ~(1 << VBUSTI);          // Acknowledge interrupt.
		if(USBSTA & (1 << VBUS))          // VBUS is connected.
		{
			// Enable end of reset interrupt.
			//UDIEN |= (1 << EORSTE) | (1 << UPRSME) | (1 << EORSME) | (1 << SOFE) | (1 << SUSPE);
			UDIEN = (1 << EORSTE);

			// Enable PLL. Assume 8MHz clock.
			PLLCSR = (1 << PLLP0) | (1 << PLLP1) | (1 << PLLE);

			// Wait until PLL is stable.
			while(!(PLLCSR & (1 << PLOCK)));

			// Start USB clock.
			USBCON &= ~(1 << FRZCLK);

			// Configure control endpoint.
			usb_configure_control_ep();

			// Set high-speed mode and attach D+ and D-.
			UDCON = 0;

			// Set default state.
			usb_state = USB_STATE_DEFAULT;
		}
	}
#endif

	if(UDINT & (1 << EORSTI))              // Reset interrupt.
	{
		UDINT = ~(1 << EORSTI);            // Acknowledge interrupt.

		// Reset all endpoints.
#if HWBOARD==DEVTRUT
		UERST = 0x1f;
#else
		UERST = 0x7f;
#endif

		// Configure control endpoint.
		usb_configure_control_ep();

		// Set default state.
		usb_state = USB_STATE_DEFAULT;
	}
}

void usb_task()
{
	usb_select_ep(0);

	if(usb_setupreceived())             // Received SETUP request.
		usb_setup_request();
}

// Handles a setup request. Assumes the control endpoint is selected.
static void usb_setup_request()
{
	uint16_t count;
	usb_device_request request;
	PGM_P data = 0;
	uint8_t a = 0;

	count = usb_bytecount();

	if(count != 8)                          // Require that the SETUP packet is 8 bytes.
	{
		usb_error = USBERROR_BADSETUP;      // If not, set error and stall the endpoint.
		usb_stall();
		return;
	}

	// Read the packet.
	usb_readdata_fix((uint8_t*) &request, (uint8_t) count);

	// Acknowledge packet reception.
	usb_acksetupreceived();

	switch(request.requestType & USB_REQUESTTYPE)
	{
	case USB_STDOUT:                              // Host-to-device, standard
		switch(request.request)
		{
		case USB_REQUEST_CLEARFEATURE:
			switch(request.requestType)
			{
			case 0:
			case 1:
				usb_stall();
				return;
			case 2:
				if(request.value == 0 && request.index < NUM_EPS)
				{
					usb_select_ep(request.index);
					usb_clearstall();
					usb_reset_ep(request.index);
					usb_resetdatatoggle();
					usb_select_ep(0);
				}
				else
				{
					usb_stall();
					return;
				}
				usb_sendsetupstatus();
				break;
			default:
				usb_stall();
				return;
			}
			break;
		case USB_REQUEST_SETFEATURE:
			// No features are defined. Respond with request error.
			usb_stall();
			return;
		case USB_REQUEST_SETADDRESS:
			if(request.requestType != 0x00)
			{
				usb_stall();
				return;
			}

			// a is the assigned address.
			a = (uint8_t) request.value;

			// Record the assigned address.
			UDADDR = a;

			usb_sendsetupstatus();

			// Enable address.
			UDADDR |= (1 << ADDEN);

			// Set address state if address is non-zero, otherwise default state.
			if(a == 0)
				usb_state = USB_STATE_DEFAULT;
			else
				usb_state = USB_STATE_ADDRESS;

			break;
		case USB_REQUEST_SETDESCRIPTOR:
			// This request is optional. We choose not to implement it.
			usb_stall();
			return;
		case USB_REQUEST_SETCONFIGURATION:
			if(request.requestType != 0x00)
			{
				usb_stall();
				return;
			}

			// a is the requested configuration
			a = (request.value & 0xff);
			if(a == 0)
			{
				if(usb_state != USB_STATE_ADDRESS)
				{
					// Unconfigure the interfaces.
					usb_unconfigure_data_interface();
					usb_unconfigure_comm_interface();

					usb_select_ep(0);
					usb_state = USB_STATE_ADDRESS;
				}
			}
			else if(a == 1)
			{
				if(usb_state != USB_STATE_CONFIGURED)
				{
					// Configure the interfaces.
					usb_configure_comm_interface();

					// 0 is the default alternate number
					usb_configure_data_interface(0);

					usb_select_ep(0);
					usb_state = USB_STATE_CONFIGURED;
				}
			}
			else
			{
				// Bad configuration request. Stall the endpoint.
				usb_stall();
				usb_error = USBERROR_BADCONFIGURATION;
				return;
			}

			usb_sendsetupstatus();

			break;
		case USB_REQUEST_SETINTERFACE:
			if(request.requestType != 0x01)
			{
				usb_stall();
				return;
			}

			if(usb_state != USB_STATE_CONFIGURED)
			{
				// Not a valid request if not in configured state. Stall the endpoint.
				usb_stall();
				return;
			}

			if(request.index == 0)
			{
				if(request.value != 0)
				{
					// Alternate setting 0 is the only valid setting for interface 0.
					usb_stall();
					return;
				}
			}
			else if(request.index == 1)
			{
				if(0 <= request.value && request.value <= 3) {
					usb_configure_data_interface(request.value);
				} else {
					usb_stall();
					return;
				}
			}
			else
			{
				// Received request for non-existent interface. Stall endpoint.
				usb_stall();
				return;
			}

			usb_select_ep(0);
			usb_sendsetupstatus();
			break;
		default:
			usb_error = USBERROR_UNKNOWNDEVICEREQUEST;
			usb_stall();
		}
		break;
	case USB_STDIN:                              // Device-to-host, standard
		switch(request.request)
		{
		case USB_REQUEST_GETSTATUS:
			//while(!usb_cansend());
			switch(request.requestType)
			{
			case 0x80:                           // Device status
				// Device status is always bus-powered, and does not support remote wakeup.
				usb_writebyte(0);
				usb_writebyte(0);
				break;
			case 0x81:                           // Interface status
				if(usb_state == USB_STATE_ADDRESS && request.index != 0)
				{
					// Only interface 0 is valid in address state.
					usb_stall();
					return;
				}
				if(usb_state == USB_STATE_CONFIGURED && request.index > 1)
				{
					// Non-existent interface requested. Stall the endpoint.
					usb_stall();
					return;
				}

				// There are no features for interfaces anyway, so why the hazzle..

				while(!usb_cansend() && !usb_packetreceived());

				UEDATX = 0;
				UEDATX = 0;

				break;
			case 0x82:                           // Endpoint status
				if(usb_state == USB_STATE_ADDRESS && request.index != 0)
				{
					// Only endpoint 0 is valid in address state.
					usb_stall();
					return;
				}
				if(usb_state == USB_STATE_CONFIGURED && request.index > 2)
				{
					// Non-existent endpoint requested. Stall the endpoint.
					usb_stall();
					return;
				}

				usb_select_ep(request.index);
				a = usb_isstalled();
				usb_select_ep(0);

				while(!usb_cansend() && !usb_packetreceived());

				if(a)
					usb_writebyte(1);
				else
					usb_writebyte(0);

				usb_writebyte(0);

				break;
			default:
				usb_stall();
				return;
			}

			usb_sendsetuppacket();
			usb_recvsetupstatus();

			break;
		case USB_REQUEST_GETDESCRIPTOR:     // Device descriptor request
			if(request.requestType != 0x80)
			{
				usb_stall();
				return;
			}

			switch(request.value >> 8)
			{
			case USB_DESCRIPTOR_DEVICE:
				usb_sendsetupdata_P((PGM_P) &device_descriptor, min(request.length, sizeof(usb_device_descriptor)));
				break;
			case USB_DESCRIPTOR_CONFIGURATION:
				if((request.value & 0xff) == 0)
				{
					usb_sendsetupdata_P((PGM_P) &configuration_1, min(request.length, sizeof(configuration_descriptor_1)));
				}
				else
				{
					usb_error = USBERROR_UNKNOWNCONFIGURATION;
					usb_stall();
					return;
				}
				break;
			case USB_DESCRIPTOR_STRING:
				if((request.value & 0xff) == 0)
				{
					// Return string descriptor zero (list of supported languages)
					usb_sendsetupdata_P((PGM_P) &string_languages, min(request.length, sizeof(string_descriptor_0)));
				}
				else if((request.index == USB_LANGID) && ((request.value & 0xff) <= USB_NUMSTRINGS))
				{
					// Return the requested string descriptor.
					data = (PGM_P) pgm_read_word(&string_descriptors[(request.value & 0xff)-1]);
					usb_sendsetupdata_P(data, min(request.length, pgm_read_byte(data)));
				}
				else
				{
					// Invalid string descriptor requested. Respond with REQUEST ERROR.
					usb_stall();
					return;
				}
				break;
			case 0x09:
				debugbuf[0] = 2+debugi;
				debugbuf[1] = 0x0f;
				usb_sendsetupdata(debugbuf, min(request.length, 2+debugi));
				break;
			case USB_DESCRIPTOR_DEVICE_QUALIFIER:
			case USB_DESCRIPTOR_OTHER_SPEED_CONFIGURATION:
			case USB_DESCRIPTOR_INTERFACE_POWER:
				// These are unsupported descriptors. Give REQUEST ERROR answer.
				// The stalled status is cleared when the next SETUP packet is received.
				usb_stall();
				return;
			default:
				// Invalid descriptor request. Should not happen. Stall the endpoint.
				usb_error = USBERROR_UNKNOWNDESCRIPTOR;
				usb_stall();
				return;
			}

			usb_recvsetupstatus();
			break;
		case USB_REQUEST_GETCONFIGURATION:
			if(request.requestType != 0x80)
			{
				usb_stall();
				return;
			}

			if(usb_state == USB_STATE_DEFAULT)
			{
				// Not a valid request in default state.
				usb_stall();
				return;
			}
			else if(usb_state == USB_STATE_ADDRESS)
				// Configuration value is zero in address state.
				a = 0;
			else if(usb_state == USB_STATE_CONFIGURED)
				// There is only one configuration.
				a = 1;

			// Return the configuration value.
			usb_writebyte(a);

			usb_sendsetuppacket();
			usb_recvsetupstatus();
			break;
		case USB_REQUEST_GETINTERFACE:
			if(request.requestType != 0x81)
			{
				usb_stall();
				return;
			}

			if(usb_state != USB_STATE_CONFIGURED)
			{
				// Only valid in configured state.
				usb_stall();
				return;
			}

			if(request.index == 0)
				// Only one alternate setting for interface 0.
				a = 0;
			else if(request.index == 1)
				a = usb_if1setting;
			else
			{
				// Invalid interface. Stall the endpoint.
				usb_stall();
				return;
			}

			// Return the alternate setting.
			UEDATX = a;

			usb_sendsetuppacket();
			usb_recvsetupstatus();
			break;
		case USB_REQUEST_SYNCHFRAME:
			// This request is only valid for isochronous endpoints.
			usb_stall();
			return;
		default:
			usb_error = USBERROR_UNKNOWNDEVICEREQUEST;
			usb_stall();
		}
		break;
	default:
		usb_error = USBERROR_UNKNOWNDEVICEREQUEST;
		usb_stall();
	}
}

static void usb_readdata_fix(uint8_t* dst, uint8_t count)
{
	while(count--)
		*(dst++) = UEDATX;
}

static void usb_sendsetupdata_P(PGM_P data, uint8_t count)
{
	uint8_t bytes_sent;

	do
	{
		while(!usb_cansend() && !usb_packetreceived());

		if(usb_packetreceived())
		{
			// In status stage.
			break;
		}

		bytes_sent = 0;
		while(bytes_sent < EP_CONTROL_SIZE && bytes_sent < count)
		{
			usb_writebyte(pgm_read_byte(data++));
			bytes_sent++;
		}
		count -= bytes_sent;

		usb_sendsetuppacket();
	} while(bytes_sent == EP_CONTROL_SIZE);
}

void usb_sendsetupdata(uint8_t* data, uint8_t count)
{
	uint8_t bytes_sent;

	do
	{
		while(!usb_cansend() && !usb_packetreceived());

		if(usb_packetreceived())
		{
			// In status stage.
			break;
		}

		bytes_sent = 0;
		while(bytes_sent < EP_CONTROL_SIZE && bytes_sent < count)
		{
			usb_writebyte(*(data++));
			bytes_sent++;
		}
		count -= bytes_sent;

		usb_sendsetuppacket();
	} while(bytes_sent == EP_CONTROL_SIZE);
}

static void usb_configure_ep(uint8_t ep, uint8_t cfg0, uint8_t cfg1)
{
	// Clear reset condition.
	UERST &= ~(1 << ep);

	// Select the endpoint.
	usb_select_ep(ep);

	// Enable the endpoint.
	UECONX |= (1 << EPEN);

	// Clear stall condition.
	UECONX |= (1 << STALLRQC);

	// Reset data toggle bit.
	UECONX |= (1 << RSTDT);

	// Disable all interrupts.
	UEIENX = 0;

	// Configure the endpoint.
	UECFG0X = cfg0;
	UECFG1X = cfg1;

	// Allocate the endpoint buffer.
	UECFG1X |= (1 << ALLOC);

    // Check if configuration is ok.
	if((UESTA0X & (1 << CFGOK)) == 0)
		usb_error = USBERROR_ENDPOINT_CONFIG;
}

static void usb_configure_control_ep()
{
	usb_configure_ep(EP_CONTROL,
					 usb_build_epcfg0(EP_CONTROL, EP_OUT),
					 usb_build_epcfg1(EP_CONTROL_SIZE, EP_SINGLEBUF));
}

static void usb_configure_comm_interface()
{
	usb_configure_ep(TRUT_EP_MSG,
					 usb_build_epcfg0(EP_BULK, EP_IN),
					 usb_build_epcfg1(TRUT_EP_MSG_SIZE, EP_SINGLEBUF));

	usb_configure_ep(TRUT_EP_CMD,
					 usb_build_epcfg0(EP_BULK, EP_OUT),
					 usb_build_epcfg1(TRUT_EP_MSG_SIZE, EP_SINGLEBUF));
}

static void usb_configure_data_interface(uint8_t altnum)
{
	uint8_t cfg0;

	switch(altnum) {
	case 0:
		cfg0 = usb_build_epcfg0(EP_BULK, EP_OUT);
		break;
	case 1:
		cfg0 = usb_build_epcfg0(EP_BULK, EP_IN);
		break;
	case 2:
		cfg0 = usb_build_epcfg0(EP_INTERRUPT, EP_OUT);
		break;
	case 3:
		cfg0 = usb_build_epcfg0(EP_INTERRUPT, EP_IN);
		break;
	default:
		return;
		// FIXME do something sensible here
	}

	usb_configure_ep(TRUT_EP_DATA, cfg0,
					 usb_build_epcfg1(TRUT_EP_DATA_SIZE, EP_DOUBLEBUF));

	usb_if1setting = altnum;
}

static void usb_unconfigure_ep(uint8_t ep)
{
	// Select the endpoint.
	usb_select_ep(ep);

	// Disable all endpoint interrupts.
	UEIENX = 0;

	// Disable the endpoint, also sets reset condition.
	UECONX = 0;

	// Deallocate endpoint buffer.
	UECFG1X &= ~(1 << ALLOC);

	// Clear endpoint configuration.
	UECFG1X = 0;
	UECFG0X = 0;
}

static void usb_unconfigure_comm_interface()
{
	usb_unconfigure_ep(TRUT_EP_MSG);
	usb_unconfigure_ep(TRUT_EP_CMD);
}

static void usb_unconfigure_data_interface()
{
	usb_unconfigure_ep(TRUT_EP_DATA);
}

uint8_t usb_transin_block(const void* b, uint16_t count, uint8_t progmem)
{
	uint8_t nwritten = 0;

	if(!usb_fifofree())
		return 0;

	if(usb_txini())
		usb_cleartxini();

	if(progmem)
		while(usb_rwal() && nwritten < count)
		{
			usb_writebyte(pgm_read_byte(b++));
			nwritten++;
		}
	else
		while(usb_rwal() && nwritten < count)
		{
			usb_writebyte(*((uint8_t*)b++));
			nwritten++;
		}

	if(!usb_rwal())
		usb_sendpacket();

	return nwritten;
}

uint8_t usb_pktout_block(uint8_t* b, uint8_t count)
{
	uint8_t nread = 0;

	while(usb_rwal() && nread < count)
	{
		*(b++) = usb_readbyte();
		nread++;
	}

	return nread;
}
