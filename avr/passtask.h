/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef PASSTASK_H
#define PASSTASK_H

// Call to initialize at program start
void passtask_init();

// Call to setup interrupt vectors at mode switch to pass-through
void passtask_setup();

// Call to disable interrupts at mode switch
void passtask_disable();

// Call regularly during pass-through mode
void passtask();

#endif
