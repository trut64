/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "trutcontrol.h"
#include "trutcomm.h"
#include "loadtask.h"
#include "passtask.h"
#include "dumptask.h"
#include "timer1.h"
#include "avrusb.h"
#include "motor.h"
#include "stackusage.h"
#include "trutprofile.h"

#if HWBOARD==DEVTRUT
static inline uint8_t hwb_pushed() { return PIND & (1 << PIND7); }
#else
static inline uint8_t hwb_pushed() { return PINE & (1 << PINE2); }
#endif

uint8_t cmdbuf[TRUT_EP_CMD_SIZE-2];

#define TRUT_MODE_MASK      0xE0

uint8_t trut_control;

static inline uint8_t trut_mode()
{
	return trut_control & TRUT_MODE_MASK;
}

static inline void trut_setmode(uint8_t mode)
{
	trut_control &= ~(TRUT_MODE_MASK);
	trut_control |= mode;

	passtask_disable();
	timer1_setmode(TIMER1_MODE_OFF);

	switch(mode)
	{
	case TRUT_MODE_PASS:
		passtask_setup();
		break;
	case TRUT_MODE_LOAD:
		loadtask_setup(LOADTASK_MODE_LOAD);
		break;
	case TRUT_MODE_DUMP:
		dumptask_setup(DUMPTASK_MODE_DUMP);
		break;
	case TRUT_MODE_C64WRITE:
		dumptask_setup(DUMPTASK_MODE_C64WRITE);
		break;
	}
}

void control_processcmd(uint8_t cmd, const uint8_t* args, uint8_t arglen);

void controller()
{
	uint8_t cmd = 0;
	uint8_t count = 0;
	uint8_t hwb_pressed = 0;

    // Set starting mode
	trut_setmode(TRUT_MODE_PASS);

	for(;;)
	{
		if(!hwb_pushed())
			hwb_pressed = 0;

		if(hwb_pressed == 0 && hwb_pushed())
		{
			hwb_pressed = 1;
		}

		if(trut_has_cmd())
		{
			if(trut_cmd_read(&cmd, cmdbuf, &count) == 0)
			{
				control_processcmd(cmd, cmdbuf, count);
			}
			else
			{
				trut_msg_protoerror();
			}
		}

		usb_task();
		motor_task();
		trutcomm_task();
		timer1_task();

		switch(trut_mode())
		{
		case TRUT_MODE_PASS:
			passtask();
			break;
		case TRUT_MODE_LOAD:
		case TRUT_MODE_WRITETAPE:
			loadtask();
			if(loadtask_finished())
				trut_setmode(TRUT_MODE_PASS);
			break;
		case TRUT_MODE_DUMP:
		case TRUT_MODE_C64WRITE:
			dumptask();
			break;
		}

		if(timer1_hadtimingerror())
			trut_msg_timing_error();
	}
}

void control_processcmd(uint8_t cmd, const uint8_t* args, uint8_t arglen)
{
	switch(cmd)
	{
	case TRUT_CMD_PING:
		trut_msg_pong();
		break;
	case TRUT_CMD_SETMODE:
		if(arglen == 1)
		{
			switch(args[0])
			{
			case TRUT_MODE_PASS:
			case TRUT_MODE_LOAD:
			case TRUT_MODE_WRITETAPE:
			case TRUT_MODE_C64WRITE:
			case TRUT_MODE_DUMP:
				trut_setmode(args[0]);
				break;
			default:
				trut_msg_invalidcmd();
			}
		}
		else
		{
			trut_msg_invalidcmd();
		}
		break;
	case TRUT_CMD_SETMSG:
		if(arglen == 1)
		{
			switch(args[0])
			{
			case TRUT_CMD_SETMSG_ENABLED:
				trut_setmsgenabled(1);
				break;
			case TRUT_CMD_SETMSG_DISABLED:
				trut_setmsgenabled(0);
				break;
			default:
				trut_msg_invalidcmd();
			}
		}
		else
		{
			trut_msg_invalidcmd();
		}
		break;
	case TRUT_CMD_DATASTART:
		if(arglen == 2)
		{
			switch(trut_mode())
			{
			case TRUT_MODE_LOAD:
				loadtask_setup(LOADTASK_MODE_LOAD);
				loadtask_setdatalength((args[0] << 8) | args[1]);
				break;
			case TRUT_MODE_WRITETAPE:
				loadtask_setup(LOADTASK_MODE_WRITETAPE);
				loadtask_setdatalength((args[0] << 8) | args[1]);
				break;
			default:
				trut_msg_invalidcmd();
			}
		}
		else
		{
			trut_msg_invalidcmd();
		}

		trut_msg_acceptingdata();

		break;
	case TRUT_CMD_QUERYBUFFERUSAGE:
		if(arglen == 0)
		{
			if(trut_mode() == TRUT_MODE_LOAD ||
			   trut_mode() == TRUT_MODE_WRITETAPE)
			{
				trut_msg_bufferusage(loadtask_bufferusage());
			}
			else if(trut_mode() == TRUT_MODE_DUMP ||
					trut_mode() == TRUT_MODE_C64WRITE)
				trut_msg_bufferusage(dumptask_bufferusage());
			else
				trut_msg_bufferusage(0);
		}
		else
		{
			trut_msg_invalidcmd();
		}
		break;
	case TRUT_CMD_QUERYSTACKUSAGE:
		if(arglen == 0)
		{
			trut_msg_stackusage(stack_usage());
		}
		else
		{
			trut_msg_invalidcmd();
		}
		break;
	case TRUT_CMD_QUERYCRC:
		if(arglen == 0)
		{
			volatile uint32_t crc_len;
			volatile uint16_t crc;
			cli();
			crc_len = buffer_crc_len;
			crc = buffer_crc;
			sei();

			trut_msg_crc(crc_len, crc);
		}
		else
		{
			trut_msg_invalidcmd();
		}
		break;
	case TRUT_CMD_PROFILE_WC:
#ifdef TRUT_PROFILE
		if(arglen == 1)
		{
			if(args[0] < NPROFILE)
				trut_msg_profile_wc(args[0], profile_wc(args[0]));
			else
				trut_msg_protoerror();
		}
		else
		{
			trut_msg_invalidcmd();
		}
#else
		trut_msg_noprofile();
#endif
		break;
	default:
		trut_msg_invalidcmd();
	}
}
