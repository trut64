/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef DBUF_H
#define DBUF_H

#include <stdint.h>

#define DBUF_SIZE 32

extern uint8_t debugbuf[DBUF_SIZE];
extern uint8_t debugi;

void dbuf_init();
void dbuf(uint8_t a);
void dbufp(uint8_t* p, uint8_t count);
void dbufn(uint8_t n, uint8_t a);

#endif
