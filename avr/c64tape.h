/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef C64TAPE_H
#define C64TAPE_H

#include <avr/io.h>

// Pin definitions for C64 and TAPE interfaces.

// For dev-trut:
// MOTOR on PB6
// C64READ on PC6
// C64WRITE on PD1
// C64SENSE on PC2
// TAPEREAD on PD0
// TAPEWRITE on PC5
// TAPESENSE on PC4
// RSIG on PC7 (Multiplexed input of TAPEREAD and C64WRITE)
// RSIGSEL on PB5 (Control signal for RSIG multiplexer)

// For usbkey:
// MOTOR on PB0
// C64READ on PB5

#if HWBOARD==DEVTRUT

#define DDR_MOTOR DDRB
#define PORT_MOTOR PORTB
#define PIN_MOTOR PINB
#define P_MOTOR 6

#define DDR_C64READ DDRC
#define PORT_C64READ PORTC
#define PIN_C64READ PINC
#define P_C64READ 6

#define DDR_C64WRITE DDRD
#define PORT_C64WRITE PORTD
#define PIN_C64WRITE PIND
#define P_C64WRITE 1

#define DDR_C64SENSE DDRC
#define PORT_C64SENSE PORTC
#define PIN_C64SENSE PINC
#define P_C64SENSE 2

#define DDR_TAPEREAD DDRD
#define PORT_TAPEREAD PORTD
#define PIN_TAPEREAD PIND
#define P_TAPEREAD 0

#define DDR_TAPEWRITE DDRC
#define PORT_TAPEWRITE PORTC
#define PIN_TAPEWRITE PINC
#define P_TAPEWRITE 5

#define DDR_TAPESENSE DDRC
#define PORT_TAPESENSE PORTC
#define PIN_TAPESENSE PINC
#define P_TAPESENSE 4

#define DDR_RSIGSEL DDRB
#define PORT_RSIGSEL PORTB
#define PIN_RSIGSEL PINB
#define P_RSIGSEL 5

#else /* HWBOARD==DEVTRUT */

#define DDR_MOTOR DDRB
#define PORT_MOTOR PORTB
#define PIN_MOTOR PINB
#define P_MOTOR 0

#define DDR_C64READ DDRB
#define PORT_C64READ PORTB
#define PIN_C64READ PINB
#define P_C64READ 5

#endif

void c64tape_init();

#define RSIG_C64 0
#define RSIG_TAPE 1

#if HWBOARD==DEVTRUT
static inline void c64read_on() { PORT_C64READ |= _BV(P_C64READ); }
static inline void c64read_off() { PORT_C64READ &= ~(_BV(P_C64READ)); }

static inline void c64sense_on() { PORT_C64SENSE &= ~(1 << P_C64SENSE); }
static inline void c64sense_off() { PORT_C64SENSE |= (1 << P_C64SENSE); }

static inline uint8_t c64write_ison()
{
	return (PIN_C64WRITE & _BV(P_C64WRITE));
}

static inline uint8_t taperead_ison()
{
	return (PIN_TAPEREAD & _BV(P_TAPEREAD));
}

static inline uint8_t tapesense_ison()
{
	return !(PIN_TAPESENSE & _BV(P_TAPESENSE));
}

static inline void tapewrite_on() { PORT_TAPEWRITE |= _BV(P_TAPEWRITE); }
static inline void tapewrite_off() { PORT_TAPEWRITE &= ~(_BV(P_TAPEWRITE)); }


static inline void rsig(uint8_t sel)
{
	if(sel == RSIG_C64)
		PORT_RSIGSEL &= ~(_BV(P_RSIGSEL));
	else
		PORT_RSIGSEL |= _BV(P_RSIGSEL);
}

#else
static inline void c64sense_on() { }
static inline void c64sense_off() { }
#endif

#endif
