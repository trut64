/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef DEBUGLEDS_H
#define DEBUGLEDS_H

#include <stdint.h>

void debugled_init();
void debugled(uint8_t led, uint8_t state);
void toggleled(uint8_t led);
void debugled4(uint8_t b);
void debugled8(uint8_t b);
void debugled8i(uint8_t b);

#endif
