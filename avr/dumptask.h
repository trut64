/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef DUMPTASK_H
#define DUMPTASK_H

#define DUMPTASK_MODE_DUMP 0
#define DUMPTASK_MODE_C64WRITE 1

#include <inttypes.h>
#include "buffer.h"

void dumptask_init();
void dumptask_setup(uint8_t mode);
buf_t dumptask_bufferusage();
void dumptask();

#endif
