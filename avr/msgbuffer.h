/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef MSGBUFFER_H
#define MSGBUFFER_H

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

// Defines the buffer size in words.
#define MSGBUFFER_CAPACITY 32

/* This buffer is used by the trutcomm functions. Status messages are stored
   in the buffer and sent by trutcomm_task(). Each message is sent as a
   transaction, i.e., in a separate packet. When adding to the buffer, the
   adding function must verify that there is space for the whole message, or
   communication will get out of sync. The format for messages in the buffer
   is:
   byte length - length of the status message
   byte data[0 .. length-1] - status message
   */

extern uint8_t msgbuffer_start;
extern uint8_t msgbuffer_size;

static inline void msgbuffer_init() { }

static inline void msgbuffer_clear()
{
	msgbuffer_start = 0;
	msgbuffer_size = 0;
}

static inline uint8_t msgbuffer_isempty()
{
	return msgbuffer_size == 0;
}

static inline uint8_t msgbuffer_isfull()
{
	return msgbuffer_size == MSGBUFFER_CAPACITY;
}

static inline uint8_t msgbuffer_numfree()
{
	return MSGBUFFER_CAPACITY - msgbuffer_size;
}

static inline uint8_t msgbuffer_numused()
{
	return msgbuffer_size;
}

// Add one byte to buffer.
void msgbuffer_addbyte(uint8_t a);

// Reads one byte from buffer.
uint8_t msgbuffer_getbyte();

#endif
