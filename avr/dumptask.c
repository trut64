/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "dumptask.h"
#include "timer1.h"
#include "buffer.h"
#include "fcpu.h"
#include "c64tape.h"
#include "motor.h"
#include <avr/io.h>
#include "trutcomm.h"
#include <util/delay.h>

#define DUMPTASK_MODE_MASK 0x01
#define DUMPTASK_FLAGS_DUMPING 0x02
#define DUMPTASK_FLAGS_SENDPACKET 0x04

static uint8_t dumptask_flags;
static uint16_t dumptask_blocksxferd;
static buf_t dumptask_bufferextremeusage;

static inline uint8_t dumptask_isdumping()
{
	return dumptask_flags & DUMPTASK_FLAGS_DUMPING;
}

static inline void dumptask_setdumping()
{
	dumptask_flags |= DUMPTASK_FLAGS_DUMPING;
}

static inline void dumptask_cleardumping()
{
	dumptask_flags &= ~DUMPTASK_FLAGS_DUMPING;
}

static inline uint8_t dumptask_sendpacket()
{
	return dumptask_flags & DUMPTASK_FLAGS_SENDPACKET;
}

static inline void dumptask_setsendpacket()
{
	dumptask_flags |= DUMPTASK_FLAGS_SENDPACKET;
}

static inline void dumptask_clearsendpacket()
{
	dumptask_flags &= ~DUMPTASK_FLAGS_SENDPACKET;
}

static inline uint8_t dumptask_mode()
{
	return dumptask_flags & DUMPTASK_MODE_MASK;
}

static inline void dumptask_setmode(uint8_t mode)
{
	dumptask_flags |= mode;
}

void dumptask_init()
{
	dumptask_flags = 0;
	dumptask_blocksxferd = 0;
	dumptask_bufferextremeusage = 0;
}

void dumptask_setup(uint8_t mode)
{
	buffer_clear();

	// Set sense low, to stop motor.
	c64sense_off();

	dumptask_flags = 0;
	dumptask_blocksxferd = 0;
	dumptask_bufferextremeusage = 0;
	dumptask_setmode(mode);

	if(mode == DUMPTASK_MODE_DUMP) {
		// Select TAPEREAD as ICP input
		rsig(RSIG_TAPE);
		timer1_setmode(TIMER1_MODE_DUMP);
	} else {
		// Select C64 as ICP input
		rsig(RSIG_C64);
		timer1_setmode(TIMER1_MODE_SAVE);
	}
}

buf_t dumptask_bufferusage()
{
	return dumptask_bufferextremeusage;
}

void dumptask()
{
	if(dumptask_mode() == DUMPTASK_MODE_DUMP) {
		if(!dumptask_isdumping() && tapesense_ison())
		{
			// Start dumping and tell C64 to turn motor on
			c64sense_on();
			timer1_start();

			// Send status message to host
			trut_msg_dumpstarted();

			dumptask_setdumping();
		}
		else if(dumptask_isdumping() && !tapesense_ison())
		{
			// Stop dumping and tell C64 to turn motor off
			c64sense_off();
			timer1_pause();

			trut_msg_dumpfinished();
			dumptask_setsendpacket();

			dumptask_cleardumping();
		}
	} else { /* dumptask_mode() == DUMPTASK_MODE_C64WRITE */
		if(!dumptask_isdumping())
		{
			// Start dumping and tell C64 to turn motor on
			c64sense_on();
			timer1_start();

			// Send status message to host
			trut_msg_dumpstarted();

			dumptask_setdumping();
		}
	}

	// If we are dumping, check if there is enough data in buffer.
	if(dumptask_isdumping())
	{
		if(buffer_numused() >= (TRUT_EP_DATA_SIZE >> 1) &&
		   trut_can_send_data())
		{
			dumptask_setsendpacket();
		}
	}

	if(dumptask_sendpacket())
	{
		if(buffer_numused() > dumptask_bufferextremeusage)
		   dumptask_bufferextremeusage = buffer_numused();

		if(trut_send_buffer_data() == 0)
			dumptask_blocksxferd++;
		else
			trut_msg_commerror(TRUT_MSG_COMMERROR_WRITEDATA);

		dumptask_clearsendpacket();
	}
}
