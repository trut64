/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "c64tape.h"

void c64tape_init()
{
    // Configure MOTOR as input.
	DDR_MOTOR &= ~(1 << P_MOTOR);

	// Configure C64READ as output.
	PORT_C64READ |= (1 << P_C64READ);
	DDR_C64READ |= (1 << P_C64READ);

#if HWBOARD==DEVTRUT
	// Configure C64WRITE as input.
	DDR_C64WRITE &= ~(_BV(P_C64WRITE));

	// Configure C64SENSE as output.
	PORT_C64SENSE |= (1 << P_C64SENSE);
	DDR_C64SENSE |= (1 << P_C64SENSE);

	// Configure TAPEREAD as input
	DDR_TAPEREAD &= ~(_BV(P_TAPEREAD));
	PORT_TAPEREAD &= ~(_BV(P_TAPEREAD));

	// Configure TAPEWRITE as output
	DDR_TAPEWRITE |= _BV(P_TAPEWRITE);

	// Configure TAPESENSE as input.
	DDR_TAPESENSE &= ~(_BV(P_TAPESENSE));
	//PORT_TAPESENSE |= _BV(P_TAPESENSE);
	PORT_TAPESENSE &= ~(_BV(P_TAPESENSE));

	// Configure RSIGSEL as output.
	DDR_RSIGSEL |= _BV(P_RSIGSEL);
	PORT_RSIGSEL |= _BV(P_RSIGSEL);
#endif
}
