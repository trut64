/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "timer1.h"

#include <avr/interrupt.h>
#include "buffer.h"
#include "c64tape.h"
#include "trutprofile.h"

#define WRAP_POINT 0x8000
#define TIMER1_PRESCALING 1		// 0 = 8 MHz, 1 = 1 MHz

// Variables for LOAD
uint8_t long_count;
uint16_t long_length;
uint16_t pulse_length;
uint16_t next_pulse;
uint16_t next_pulse_extension;
uint16_t next_pulse_remainder;

// Variables for DUMP and WRITE
uint16_t previous_stamp;
uint16_t pulse_remainder;
uint16_t overflows;

uint8_t timer1_mode;
static volatile uint8_t timing_error;

void timer1_reset();

void timer1_init()
{
	timer1_reset();

	timer1_mode = TIMER1_MODE_OFF;

	// Set the "waveform" pin to output mode
	DDR_C64READ |= (1 << P_C64READ);
}

void timer1_reset() {
	// Reset control registers used by TRUT64
	TCCR1B = 0x00;
	TCCR1A = 0x00;
	TIMSK1 = 0x00;

	// Reset counter
	TCNT1 = 0x0000;

	// Reset comparator
	OCR1A = 0x0000;

	// Reset input capture register
	ICR1 = 0x0000;

	// Clear the timing error flag
	timing_error = 0;
}

void timer1_setmode(uint8_t timer_mode) {
	timer1_mode = timer_mode;

	timer1_reset();

	switch(timer_mode)
	{
	case TIMER1_MODE_OFF:
		break;

	case TIMER1_MODE_LOAD:
	case TIMER1_MODE_WRITE:
		long_count=0;
		long_length=0;
		next_pulse_extension=0;

		// Set CASS READ to low
		PORT_C64READ &= ~(1 << P_C64READ);

		// Enable output compare interrupt for OCR1A
		TIMSK1 |= (1 << OCIE1A);

		// Use CTC mode, i.e., clear the timer on match
		TCCR1A &= ~((1 << WGM11) | (1 << WGM10)); //WGM11:0=0;
		TCCR1B |= (1 << WGM12);		//WGM12=1;
		TCCR1B &= ~(1 << WGM13); 	//WGM13=0;

		// Enable toggle mode
		TCCR1A |= (1 << COM1A0);	// COM1A0=1
		TCCR1A &= ~(1 << COM1A1);	// COM1A1=0
		break;

	case TIMER1_MODE_DUMP:
	case TIMER1_MODE_SAVE:
		previous_stamp=0;
		overflows=0;

		// Enable Input Capture interrupt
		TIMSK1 |= (1 << ICIE1);

		// Enable timer overflow interrupt
		TIMSK1 |= (1 << TOIE1);

		// Trigger at high->low
		TCCR1B &= ~(1 << ICES1);		// 0 = high->low, 1=low->high
		break;

	default:
		// Not implemented yet.
		break;
	}
}

void timer1_start() {
	uint8_t interrupts_enabled;

	switch(timer1_mode)
	{
	case TIMER1_MODE_LOAD:
	case TIMER1_MODE_WRITE:
		interrupts_enabled = (SREG & (1 << SREG_I));

		// So, we want to set the comparator, OCR1A, according to the
		// initial pulse length. The reason why we do this, instead of
		// setting OCR1A to 0x0000, is because the interrupt code will
		// be quicker (otherwise we will get problems with CTC and
		// toggle mode for the first pulse, if we don't introduce special
		// checks in the interrupt)

		pulse_length=buffer_getword();
		next_pulse=pulse_length;		// Save the pulse for the high part
		if (pulse_length==0) {
			// First pulse is long (do not toggle!)
			TCCR1A &= ~(1 << COM1A0);	// COM1A0=0
			TCCR1A |= (1 << COM1A1);	// COM1A1=1 -> "Clear on Compare Match" mode.
			long_count=2;			// Divide the pulse into two halves
			OCR1A=(WRAP_POINT-1);	// Set the comparator to the first half
		} else {
			// First pulse is short (toggle!)
			TCCR1A |= (1 << COM1A0);	// COM1A0=1
			TCCR1A &= ~(1 << COM1A1);	// COM1A1=0 -> Toggle mode
			OCR1A=pulse_length;	// New (half) pulse length
		}

		// Start timer
		TCCR1B |= (1 << TIMER1_PRESCALING);

		if(interrupts_enabled)
			sei();
		break;

	case TIMER1_MODE_DUMP:
	case TIMER1_MODE_SAVE:
		// Start timer
		TCCR1B |= (1 << TIMER1_PRESCALING);
		break;

	default:
		// Not implemented yet.
        break;
	}
}

void timer1_pause()
{
	TCCR1B &= ~((1 << CS10) | (1 << CS11) | (1 << CS12));
}

void timer1_resume()
{
	TCCR1B |= (1 << TIMER1_PRESCALING);
}

uint8_t timer1_hadtimingerror()
{
	if(timing_error)
	{
		timing_error = 0;
		return 1;
	}
	return 0;
}

void timer1_task()
{
}

ISR(TIMER1_COMPA_vect){
#ifdef TRUT_PROFILE
	profile_start(0);
#endif

	// The timer is already cleared (and has started counting again)
	// The pulse level is also toggled.
	// Hence, no "timing" is needed, and we can set the new (half) pulse length without involving asm!

	if (long_count>0) {
		// We are in the middle of a long pulse

		if (long_length==0) {
			// The entire pulse is not yet fetched!
			// Process it!

			// pulse_length=buffer_getword();
			// FIX MACRO
			if (PIN_C64READ & (1 << P_C64READ)) {
				// We are in the last half of pulse
				if (next_pulse_extension) {
					// next_pulse_extension>0 -> keep extending
					pulse_length=0;
					next_pulse_extension--;
				} else {
					// next_pulse_extension==0 -> no extending!
					pulse_length=next_pulse_remainder;
				}
			} else {
				// We are in the first part of the pulse
				pulse_length=buffer_getword();
				if (pulse_length==0) {
					next_pulse_extension++;
				} else {
					next_pulse_remainder=pulse_length;
				}
			}

			if (pulse_length==0) {
				// Extend the pulse further! (It is _long_)
				long_count+=2;
			} else {
				// We reached the end of the long pulse
				// We need to split it in a timing-wise convenient way
				if (pulse_length<=WRAP_POINT) {
					// The remaining part is small enough. Merge it with the last split overflow pulse
					// (allows for handling of pulses of length $10001 etc)
					long_length=(WRAP_POINT-1)+pulse_length;
					long_count--;
				} else {
					// The remaining part is large. Let's give it an iterrupt of its own!
					long_length=pulse_length;
				}
			}
		}

		// Ok, now it is time to set the pulse length

		long_count--;
		if (long_count==0) {
			// End the long pulse

			OCR1A=long_length;

			// Reenable toggle mode
			TCCR1A |= (1 << COM1A0);	// COM1A0=1
			TCCR1A &= ~(1 << COM1A1);	// COM1A1=0

			// Disable long pulse mode
			long_length=0;

		} else {
			// Keep on extending the pulse.

			OCR1A=(WRAP_POINT-1);
		}

	} else {
		// No long pulse is being processed

		if (PIN_C64READ & (1 << P_C64READ)) {
			// Last half of the pulse
			pulse_length=next_pulse;
		} else {
			// First half of the pulse
			pulse_length=buffer_getword();
			next_pulse=pulse_length;
		}

		if (pulse_length==0) {
			// Overflow. Initiate long pulse mode!

			OCR1A=(WRAP_POINT-1);

			// Divide the pulse into two parts
			long_count=2;

			// Disable toggle mode
			if ((PIN_C64READ & (1 << P_C64READ)) > 0)  {
				// Signal is high, so set "Set on Compare Match" mode.
				TCCR1A |= (1 << COM1A0);	// COM1A0=1
				TCCR1A |= (1 << COM1A1);	// COM1A1=1
			} else {
				// Signal is low, so set "Clear on Compare Match" mode.
				TCCR1A |= (1 << COM1A1);	// COM1A1=1
				TCCR1A &= ~(1 << COM1A0);	// COM1A0=0
			}
		} else {
			// Short pulse
			OCR1A=pulse_length;	// New (half) pulse length
		}
	}

	// Will the pulse be treated in time by the interrupt?
	// 20 is the estimation fo the cycles left of the interrupt routine. Should be set pessimistically.
	// Maybe it is better to use an overflow interrupt, but that requires that the timer mode is set to
	// normal at the beginning of the interrupt (and reset to CTC at the end).
	// Also, the probability that the result will be OK despite the timing error will be much
	// much smaller when using the overflow interrupt.
	if (TCNT1+9>OCR1A) {
		timing_error = 1;
#if HWBOARD!=DEVTRUT
		PORTD ^= (1 << 6);
#endif

		// Nope.
		//printf("Timing Error! Counter: %d - Comparator: %d\n",TCNT1,OCR1A);
	}
#ifdef TRUT_PROFILE
	profile_stop(0);
#endif
}

// Overflow interrupt. I leave the old printf()'s to demonstrate my
// original ideas. Right now, only the DUMP/SAVE overflow is
// implemented, for various reasons.
ISR(TIMER1_OVF_vect){
#ifdef TRUT_PROFILE
	profile_start(1);
#endif

	if (TIMSK1 & (1 << OCIE1A)) {
		// Overflow in LOAD mode
		// -> Timing error

		//PORTD |= (1 << 6);	// AT90USBKEY: Set green upper led
		//printf("Timing Error! Comparator: %d\n",OCR1A);
	} else if (TIMSK1 & (1 << ICIE1)) {
		// Overflow in DUMP/WRITE mode
		// -> Preliminary length of pulse increases by 0x10000

		overflows+=1;
	} else {
		//printf("WARNING: OVF1 interrupt was triggered without reason!");
	}
#ifdef TRUT_PROFILE
	profile_stop(1);
#endif
}

// The capture interrupt is the same for DUMP and SAVE
ISR(TIMER1_CAPT_vect) {
#ifdef TRUT_PROFILE
	profile_start(2);
#endif
	uint16_t icr1 = ICR1;

	// Since the capture interrupt has higher priority than the overflow
	// interrupt, sometimes (for example when ICR1==0) the "overflows" variable
	// will not be updated until after the capture interrupt has been carried
	// out. To handle this, we check if the overflow interrupt will be triggered
	// immediately after the capture interrupt, and if that is the case, we will
	// take care of increasing the "overflows" variable and then prevent the
	// overflow interrupt from starting.
	// However, we do not want to cancel overflow interrupts generated AFTER the
	// capture interrupt has been triggered, and therefore the ICR1<0x8000 check
	// is done.
	if (icr1<0x8000 && (TIFR1 & (1 << TOV1))) {
		overflows++;
		// We do not want the overflow interrupt to be triggered, since it will
		// increase the "overflows" variable further (i.e., set it to 1).
		TIFR1 = (1 << TOV1);
	}

	if (icr1<previous_stamp) {
		// The difference between the time stamp of this pulse
		// and the previous one is negative.
		// pulse_remainder=0x10000+icr1-previous_stamp;
		// (which is the same as icr1-previous_stamp, hence no special
		// treatment is needed)

		// One overflow was registered before 0x10000 cycles
		// had passed.
		overflows--;
	}
	pulse_remainder=icr1-previous_stamp;

	if (overflows>0) {
		buffer_addword(0x0000);		// 0x0000 means extended pulse
		buffer_addword(overflows);	// multiple of 0x10000
	}

	buffer_addword(pulse_remainder);
	previous_stamp=icr1;
	overflows=0;
#ifdef TRUT_PROFILE
	profile_stop(2);
#endif
}
