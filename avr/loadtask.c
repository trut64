/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "loadtask.h"
#include "timer1.h"
#include "buffer.h"
#include "fcpu.h"
#include "c64tape.h"
#include "motor.h"
#include "avrusb.h"
#include <avr/io.h>
#include "trutcomm.h"
#include <util/delay.h>

#define LOADTASK_FLAGS_LOADING 0x01
#define LOADTASK_FLAGS_MOTOR 0x02
#define LOADTASK_FLAGS_MEASUREBUFFER 0x04
#define LOADTASK_FLAGS_FINISHED 0x08

static uint8_t loadtask_flags;

// Used to measure extreme usage of the buffer (least amount used).
static buf_t loadtask_bufferextremeusage;

static uint16_t loadtask_datalength;
static uint16_t loadtask_blocksxferd;

static inline uint8_t loadtask_isloading()
{
	return loadtask_flags & LOADTASK_FLAGS_LOADING;
}

static inline void loadtask_setloading()
{
	loadtask_flags |= LOADTASK_FLAGS_LOADING;
}

static inline void loadtask_clearloading()
{
	loadtask_flags &= ~LOADTASK_FLAGS_LOADING;
}

static inline uint8_t loadtask_motorison()
{
	return loadtask_flags & LOADTASK_FLAGS_MOTOR;
}

static inline void loadtask_setmotor()
{
	loadtask_flags |= LOADTASK_FLAGS_MOTOR;
}

static inline void loadtask_clearmotor()
{
	loadtask_flags &= ~LOADTASK_FLAGS_MOTOR;
}

static inline uint8_t loadtask_measurebuffer()
{
	return loadtask_flags & LOADTASK_FLAGS_MEASUREBUFFER;
}

static inline void loadtask_setmeasurebuffer()
{
	loadtask_flags |= LOADTASK_FLAGS_MEASUREBUFFER;
}

static inline void loadtask_clearmeasurebuffer()
{
	loadtask_flags &= ~LOADTASK_FLAGS_MEASUREBUFFER;
}

static inline void loadtask_setfinished()
{
	loadtask_flags |= LOADTASK_FLAGS_FINISHED;
}

static inline void loadtask_clearfinished()
{
	loadtask_flags &= ~LOADTASK_FLAGS_FINISHED;
}

void loadtask_init()
{
	loadtask_flags = 0;
	loadtask_bufferextremeusage = 0xff;
	loadtask_datalength = 0;
	loadtask_blocksxferd = 0;
}

void loadtask_setup(uint8_t mode)
{
	// Only LOAD mode implemented so far.

	buffer_clear();

	timer1_setmode(TIMER1_MODE_LOAD);

	loadtask_flags = 0;
	loadtask_bufferextremeusage = 0xff;
	loadtask_datalength = 0;
	loadtask_blocksxferd = 0;
}

uint8_t loadtask_finished()
{
	return loadtask_flags & LOADTASK_FLAGS_FINISHED;
}

void loadtask_setdatalength(uint16_t length)
{
	loadtask_datalength = length;
}

buf_t loadtask_bufferusage()
{
	return loadtask_bufferextremeusage;
}

uint8_t loadtask_xfercomplete()
{
	return loadtask_datalength == loadtask_blocksxferd;
}

void loadtask()
{
	// Read data from usb if there is free space in the buffer
	if(buffer_numfree() >= (TRUT_EP_DATA_SIZE >> 1) && trut_has_data())
	{
		// If buffer usage measuring has started, update the lowest usage
		// level
		if(loadtask_measurebuffer())
			if(buffer_numused() < loadtask_bufferextremeusage)
				loadtask_bufferextremeusage = buffer_numused();

		// Read data to buffer, update number of blocks transferred
		if(trut_read_data_to_buffer() == 0)
		{
			if(loadtask_blocksxferd < loadtask_datalength)
			{
				loadtask_blocksxferd++;
				if(loadtask_blocksxferd == loadtask_datalength)
				{
					loadtask_clearmeasurebuffer();
					trut_msg_loadfinished();
				}
			}
		}
		else
			trut_msg_commerror(TRUT_MSG_COMMERROR_READDATA);
	}

	// Enable buffer usage measuring if the buffer is sufficiently full
	if(buffer_numfree() <= (TRUT_EP_DATA_SIZE >> 1) &&
	   !loadtask_measurebuffer() && loadtask_bufferextremeusage == 0xff)
	{
		loadtask_setmeasurebuffer();
	}

	// If there was a buffer underflow, stop the loading
	// If the last block has not been received, send a buffer underflow
	// message.
	if(buffer_hadunderflow())
	{
		if(!loadtask_xfercomplete())
			trut_msg_commerror(TRUT_MSG_COMMERROR_BUFFERUF);
		loadtask_setfinished();
	}

	// Buffer overflows should not happen, but check for it anyway
	if(buffer_hadoverflow())
	{
		trut_msg_commerror(TRUT_MSG_COMMERROR_BUFFEROF);
		loadtask_setfinished();
	}

	// Pause or resume timer and send status message if the motor has changed
	if (motor_isoff())
	{
		// Motor is off
		if (loadtask_motorison())
		{
			// Motor was on, stop timer
			timer1_pause();
			loadtask_clearmotor();
			trut_msg_motor(TRUT_MSG_MOTOR_OFF);
		}
	}
	else
	{
		// Motor is on
		if (!loadtask_motorison())
		{
			// Motor was off, resume timer if we are loading
			if (loadtask_isloading())
			{
				// Restart timer
				timer1_resume();
			}
			loadtask_setmotor();
			trut_msg_motor(TRUT_MSG_MOTOR_ON);
		}
	}

	// Set sense and start the timer if the buffer is sufficiently full
	if(!loadtask_isloading() && buffer_numused() > 64)
	{
		trut_msg_loadstarted();
		c64sense_on();
		loadtask_setloading();
		// Start the timer and immediately pause, so the data will come
		// when motor goes high.
		timer1_start();
		timer1_pause();
	}
}
