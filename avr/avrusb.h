/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef AVRUSB_H
#define AVRUSB_H

#include <stdint.h>
#include <avr/pgmspace.h>

#define KILLBK RXOUTI

#define USBERROR_ENDPOINT_CONFIG 1
#define USBERROR_BADSETUP 2
#define USBERROR_UNKNOWNDESCRIPTOR 3
#define USBERROR_UNKNOWNDEVICEREQUEST 4
#define USBERROR_BADCONFIGURATION 5
#define USBERROR_UNKNOWNCONFIGURATION 6

#define USB_REQUESTTYPE 0xe0
#define USB_STDOUT 0x00
#define USB_CLASSOUT 0x20
#define USB_VENDOROUT 0x40
#define USB_STDIN 0x80
#define USB_CLASSIN 0xa0
#define USB_VENDORIN 0xc0

#define USB_REQUEST_GETSTATUS 0
#define USB_REQUEST_CLEARFEATURE 1
#define USB_REQUEST_SETFEATURE 3
#define USB_REQUEST_SETADDRESS 5
#define USB_REQUEST_GETDESCRIPTOR 6
#define USB_REQUEST_SETDESCRIPTOR 7
#define USB_REQUEST_GETCONFIGURATION 8
#define USB_REQUEST_SETCONFIGURATION 9
#define USB_REQUEST_GETINTERFACE 10
#define USB_REQUEST_SETINTERFACE 11
#define USB_REQUEST_SYNCHFRAME 12

#define USB_DESCRIPTOR_DEVICE 1
#define USB_DESCRIPTOR_CONFIGURATION 2
#define USB_DESCRIPTOR_STRING 3
#define USB_DESCRIPTOR_INTERFACE 4
#define USB_DESCRIPTOR_ENDPOINT 5
#define USB_DESCRIPTOR_DEVICE_QUALIFIER 6
#define USB_DESCRIPTOR_OTHER_SPEED_CONFIGURATION 7
#define USB_DESCRIPTOR_INTERFACE_POWER 8

#define STORAGE_RESET 0xff
#define STORAGE_GETMAXLUN 0xfe

extern uint8_t usb_error;
extern uint8_t usb_debug;

typedef struct
{
	uint8_t requestType;
	uint8_t request;
	uint16_t value;
	uint16_t index;
	uint16_t length;
} usb_device_request;

void usb_init();
void usb_disable();

void usb_task();

#define USB_FRAMENUMBER_TOP 2048

static inline uint16_t usb_framenumber() { return UDFNUM; }

static inline uint8_t usb_current_ep() { return UENUM; }
static inline void usb_select_ep(uint8_t ep) { UENUM = ep; }
static inline void usb_reset_ep(uint8_t ep)
{
	UERST = (uint8_t) (1 << ep); UERST = 0;
}

static inline uint8_t usb_readbyte() { return UEDATX; }
static inline void usb_writebyte(uint8_t b) { UEDATX = b; }
static inline uint8_t usb_fifofree() { return UEINTX & _BV(FIFOCON); }
static inline uint8_t usb_rwal() { return UEINTX & _BV(RWAL); }

static inline void usb_cleartxini()
{
	UEINTX = ~(_BV(TXINI) | _BV(KILLBK));
}
static inline void usb_sendpacket()
{
	UEINTX = (uint8_t) ~(_BV(FIFOCON) | _BV(KILLBK));
}

static inline uint8_t usb_transin_canstart() { return UEINTX & _BV(TXINI); }
static inline void usb_transin_start() { UEINTX = (uint8_t) ~_BV(TXINI); }
uint8_t usb_transin_block(const void* b, uint16_t count, uint8_t progmem);
static inline uint8_t usb_transin_byte(uint8_t b)
{
	if(!usb_fifofree())
		return 0;

	usb_cleartxini();
	usb_writebyte(b);

	if(!usb_rwal())
		usb_sendpacket();

	return 1;
}
static inline uint8_t usb_transin_end()
{
	if(!usb_fifofree())
		return 0;

	usb_cleartxini();
	usb_sendpacket();

	return 1;
}

static inline uint8_t usb_pktout_received() { return UEINTX & _BV(RXOUTI); }
static inline void usb_pktout_start() { UEINTX = (uint8_t) ~_BV(RXOUTI); }
static inline uint8_t usb_pktout_byte(uint8_t* b)
{
	if(usb_rwal())
	{
		*b = usb_readbyte();
		return 1;
	}
	else
		return 0;
}
uint8_t usb_pktout_block(uint8_t* b, uint8_t count);
static inline void usb_pktout_end() { UEINTX = (uint8_t) ~_BV(FIFOCON); }

static inline void usb_stall() { UECONX |= _BV(STALLRQ); }
static inline uint8_t usb_isstalled() { return UEINTX & _BV(STALLEDI); }
static inline void usb_clearstall() { UECONX |= _BV(STALLRQC); }
static inline void usb_resetdatatoggle() { UECONX |= _BV(RSTDT); }

#endif
