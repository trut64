/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef TRUTCOMM_H
#define TRUTCOMM_H

#include <stdint.h>
#include <avr/pgmspace.h>
#include "trutproto.h"
#include "msgbuffer.h"

// Flags for communication
// Sending messages is allowed
#define TRUTCOMM_MSGENABLED      0x01
// Set if the functions should block on the _previous_ message sent.
// If not set, and the previous message has not been sent, the message
// is simply discarded.
#define TRUTCOMM_MSGBLOCKS       0x02

extern uint8_t trut_flags;

static inline void trut_setmsgenabled(uint8_t e)
{
	if(e)
		trut_flags |= TRUTCOMM_MSGENABLED;
	else
		trut_flags &= (uint8_t) ~TRUTCOMM_MSGENABLED;
}

static inline uint8_t trut_msgenabled() { return trut_flags & TRUTCOMM_MSGENABLED; }

static inline void trut_setmsgblocks(uint8_t b)
{
	if(b)
		trut_flags |= TRUTCOMM_MSGBLOCKS;
	else
		trut_flags &= (uint8_t) ~TRUTCOMM_MSGBLOCKS;
}

static inline uint8_t trut_msgblocks() { return trut_flags & TRUTCOMM_MSGBLOCKS; }

// Send a string message to host. This function blocks.
// Arguments:
//   data: block to send
//   len: length of data
void trut_msg_str(const char* str);

// Send a HEX message to host. This function blocks.
// Arguments:
//   data: block to send
//   len: length of data
void trut_msg_hex(const uint8_t* data, uint8_t len);

// Used for simple (one byte) status messages.
static inline void trut_msg_simple(uint8_t msg)
{
	if(msgbuffer_numfree() >= 2)
	{
		msgbuffer_addbyte(1);
		msgbuffer_addbyte(msg);
	}
}

// Used for messages with one-byte arguments.
static inline void trut_msg_arg(uint8_t msg, uint8_t arg)
{
	if(msgbuffer_numfree() >= 3)
	{
		msgbuffer_addbyte(2);
		msgbuffer_addbyte(msg);
		msgbuffer_addbyte(arg);
	}
}

// Used for messages with word arguments.
static inline void trut_msg_argw(uint8_t msg, uint16_t arg)
{
	if(msgbuffer_numfree() >= 4)
	{
		msgbuffer_addbyte(3);
		msgbuffer_addbyte(msg);
		msgbuffer_addbyte(arg >> 8);
		msgbuffer_addbyte(arg & 0xff);
	}
}

static inline void trut_msg_pong()
{
	trut_msg_simple(TRUT_MSG_PONG);
}

static inline void trut_msg_invalidcmd()
{
	trut_msg_simple(TRUT_MSG_INVALIDCMD);
}

static inline void trut_msg_protoerror()
{
	trut_msg_simple(TRUT_MSG_PROTOERROR);
}

static inline void trut_msg_loadstarted()
{
	trut_msg_simple(TRUT_MSG_LOADSTARTED);
}

static inline void trut_msg_loadfinished()
{
	trut_msg_simple(TRUT_MSG_LOADFINISHED);
}

static inline void trut_msg_dumpstarted()
{
	trut_msg_simple(TRUT_MSG_DUMPSTARTED);
}

static inline void trut_msg_dumpfinished()
{
	trut_msg_simple(TRUT_MSG_DUMPFINISHED);
}

static inline void trut_msg_profile_wc(uint8_t idx, uint16_t m)
{
	if(msgbuffer_numfree() >= 5)
	{
		msgbuffer_addbyte(4);
		msgbuffer_addbyte(TRUT_MSG_PROFILE_WC);
		msgbuffer_addbyte(idx);
		msgbuffer_addbyte(m >> 8);
		msgbuffer_addbyte(m & 0xff);
	}
}

static inline void trut_msg_noprofile()
{
	trut_msg_simple(TRUT_MSG_NOPROFILE);
}

static inline void trut_msg_motor(uint8_t s)
{
	trut_msg_arg(TRUT_MSG_MOTOR, s);
}

static inline void trut_msg_commerror(uint8_t s)
{
	trut_msg_arg(TRUT_MSG_COMMERROR, s);
}

static inline void trut_msg_acceptingdata()
{
	trut_msg_simple(TRUT_MSG_ACCEPTINGDATA);
}

static inline void trut_msg_bufferusage(uint8_t s)
{
	trut_msg_arg(TRUT_MSG_BUFFERUSAGE, s);
}

static inline void trut_msg_stackusage(uint8_t s)
{
	trut_msg_arg(TRUT_MSG_STACKUSAGE, s);
}

static inline void trut_msg_crc(uint32_t len, uint16_t crc)
{
	if(msgbuffer_numfree() >= 8)
	{
		msgbuffer_addbyte(7);
		msgbuffer_addbyte(TRUT_MSG_CRC);
		msgbuffer_addbyte((len & 0xff000000) >> 24);
		msgbuffer_addbyte((len & 0xff0000)   >> 16);
		msgbuffer_addbyte((len & 0xff00)     >> 8);
		msgbuffer_addbyte(len & 0xff);
		msgbuffer_addbyte((crc & 0xff00) >> 8);
		msgbuffer_addbyte(crc & 0xff);
	}
}

static inline void trut_msg_timing_error()
{
	if(msgbuffer_numfree() >= 2)
	{
		msgbuffer_addbyte(1);
		msgbuffer_addbyte(TRUT_MSG_TIMERERROR);
	}
}

// Should be called periodically. The function will send the status messages
// stored in the msgbuffer.
void trutcomm_task();

// Returns 1 if there is a command received, 0 otherwise.
uint8_t trut_has_cmd();

// Receive a command from the host. The function does not block.
// Arguments:
//   cmd: will be filled with the command type
//   data: buffer that will be filled with the command arguments.
//         At most TRUT_EP_CMD_SIZE-2 bytes will be written.
//   len: will be filled with the number of argument bytes written.
// Returns:
//   0 on success, 1 on error.
uint8_t trut_cmd_read(uint8_t* cmd, uint8_t* data, uint8_t* len);

// Returns 1 if there is data in the usb receive buffer.
uint8_t trut_has_data();

// Writes a packet of data to the circular buffer. The packet is supposed
// to contain an even number of bytes. If not, 1 is returned, although
// this will not happen if the whole data block is sent as a transaction.
// Returns:
//   0 on success, 1 on error
uint8_t trut_read_data_to_buffer();

// Returns 1 if a data packet can be sent.
uint8_t trut_can_send_data();

// Sends a packet of data to the host from the circular buffer.
// Returns:
//   0 on success, 1 on error
uint8_t trut_send_buffer_data();

#endif
