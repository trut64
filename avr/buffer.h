/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef BUFFER_H
#define BUFFER_H

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

// Defines the buffer size in words.
#if HWBOARD==DEVTRUT
#  define BUFFER_CAPACITY 128
#else
#  define BUFFER_CAPACITY 1024
#endif

#if BUFFER_CAPACITY >= 256
typedef uint16_t buf_t;
#else
typedef uint8_t buf_t;
#endif

enum buffer_status {BUFFER_OK = 0, BUFFER_UNDERFLOW = 1, BUFFER_OVERFLOW = 2};
volatile extern enum buffer_status buffer_flags;
volatile extern uint16_t buffer[BUFFER_CAPACITY];
volatile extern buf_t buffer_start;
volatile extern buf_t buffer_size;

volatile extern uint16_t buffer_crc;
volatile extern uint32_t buffer_crc_len;
void update_crc(uint16_t w);

static inline void buffer_init() { }

static inline void buffer_clear()
{
	buffer_flags = BUFFER_OK;
	buffer_start = 0;
	buffer_size = 0;
	buffer_crc_len = 0;
	buffer_crc = 0xffff;
}

static inline uint8_t buffer_isempty()
{
	return buffer_size == 0;
}

static inline uint8_t buffer_isfull()
{
	return buffer_size == BUFFER_CAPACITY;
}

static inline buf_t buffer_numfree()
{
	return BUFFER_CAPACITY - buffer_size;
}

static inline buf_t buffer_numused()
{
	return buffer_size;
}

static inline uint8_t buffer_isok()
{
	return buffer_flags == BUFFER_OK;
}

static inline uint8_t buffer_hadunderflow()
{
	if(buffer_flags & BUFFER_UNDERFLOW)
	{
		buffer_flags &= (uint8_t) ~BUFFER_UNDERFLOW;
		return 1;
	}
	else
		return 0;
}

static inline uint8_t buffer_hadoverflow()
{
	if(buffer_flags & BUFFER_OVERFLOW)
	{
		buffer_flags &= (uint8_t) ~BUFFER_OVERFLOW;
		return 1;
	}
	else
		return 0;
}

// Add one word to buffer.
static inline void buffer_addword(uint16_t a)
{
	uint8_t interrupts_enabled = (SREG & _BV(SREG_I));
	buf_t idx;

	cli();

	// The CRC computations are disabled for now due to latency
	// problems when dumping.
	// update_crc(a);

	if(buffer_size >= BUFFER_CAPACITY)
	{
		buffer_flags |= BUFFER_OVERFLOW;
	}
	else
	{
		idx = buffer_start+buffer_size;
		if(idx >= BUFFER_CAPACITY)
			idx -= BUFFER_CAPACITY;

		buffer[idx] = a;
		buffer_size++;
	}

	if(interrupts_enabled)
		sei();
}

// Reads one word from buffer.
static inline uint16_t buffer_getword()
{
	uint8_t interrupts_enabled = (SREG & _BV(SREG_I));
	uint16_t a;

	cli();

	if(buffer_size == 0)
	{
		buffer_flags |= BUFFER_UNDERFLOW;
		a = 0xffff;
	}
	else
	{
		a = buffer[buffer_start];

		buffer_size--;
		buffer_start++;
		if(buffer_start >= BUFFER_CAPACITY)
			buffer_start = 0;
	}

	if(interrupts_enabled)
		sei();

	return a;
}

#endif
