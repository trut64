/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "msgbuffer.h"

uint8_t msgbuffer[MSGBUFFER_CAPACITY];
uint8_t msgbuffer_start = 0;
uint8_t msgbuffer_size = 0;

void msgbuffer_addbyte(uint8_t a)
{
	uint8_t idx;

	if(msgbuffer_size < MSGBUFFER_CAPACITY)
	{
		idx = msgbuffer_start+msgbuffer_size;
		if(idx >= MSGBUFFER_CAPACITY)
			idx -= MSGBUFFER_CAPACITY;

		msgbuffer[idx] = a;
		msgbuffer_size++;
	}
}

uint8_t msgbuffer_getbyte()
{
	uint8_t a = 0xff;

	if(msgbuffer_size > 0)
	{
		a = msgbuffer[msgbuffer_start];

		msgbuffer_size--;
		msgbuffer_start++;
		if(msgbuffer_start >= MSGBUFFER_CAPACITY)
			msgbuffer_start = 0;
	}

	return a;
}
