/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "passtask.h"
#include "fcpu.h"
#include "c64tape.h"
#include "motor.h"
#include <avr/io.h>
#include "trutcomm.h"
#include <util/delay.h>

void passtask_init()
{
}

void passtask_setup()
{
	// First clear the interrupt enable flags
	EIMSK &= ~(_BV(INT0) | _BV(INT1));

	// Enable interrupts of both edges for taperead and c64write
	EICRA &= ~(_BV(ISC11));
	EICRA |= _BV(ISC10);
	EICRA &= ~(_BV(ISC01));
	EICRA |= _BV(ISC00);

	// Clear possible interrupt flags
	EIFR |= (_BV(INTF1) | _BV(INTF0));

	// Set the interrupt enable flags
	EIMSK |= (_BV(INT0) | _BV(INT1));

	// Finally update the signals
	if(taperead_ison())
		c64read_on();
	else
		c64read_off();

	if(tapesense_ison())
		c64sense_on();
	else
		c64sense_off();

	if(c64write_ison())
		tapewrite_on();
	else
		tapewrite_off();
}

void passtask_disable()
{
	// Clear the interrupt enable flags
	EIMSK &= ~(_BV(INT0) | _BV(INT1));
}

void passtask()
{
	if(tapesense_ison())
		c64sense_on();
	else
		c64sense_off();
}

// Interrupt handler for TAPEREAD
ISR(INT0_vect)
{
	if(taperead_ison())
		c64read_on();
	else
		c64read_off();
}

// Interrupt handler for C64WRITE
ISR(INT1_vect)
{
	if(c64write_ison())
		tapewrite_on();
	else
		tapewrite_off();
}
