/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef TIMER1_H
#define TIMER1_H

#include <stdint.h>

#define TIMER1_MODE_OFF   0
#define TIMER1_MODE_LOAD  1  // Host -> C64
#define TIMER1_MODE_DUMP  2  // Tape -> Host
#define TIMER1_MODE_WRITE 3  // Host -> Tape
#define TIMER1_MODE_SAVE  4  //  C64 -> Host

// Call at power-up.
void timer1_init();

// Set the timer mode
void timer1_setmode(uint8_t timer_mode);

// Start, pause, and resume timer
void timer1_start();
void timer1_pause();
void timer1_resume();

// Returns a non-zero value if there was a timing error since the last call
// to timer1_hadtimingerror() or timer1_setmode()
uint8_t timer1_hadtimingerror();

// Call periodically
void timer1_task();

#endif
