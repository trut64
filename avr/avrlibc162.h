/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef AVRLIBC162_H
#define AVRLIBC162_H

#include <avr/io.h>

/* avrlibc does not yet include complete support for at90usb162. The lacking
   functionality is defined in this file. */

#if __AVR_LIBC_VERSION__ < 10600ul
typedef enum
{
    clock_div_1 = 0,
    clock_div_2 = 1,
    clock_div_4 = 2,
    clock_div_8 = 3,
    clock_div_16 = 4,
    clock_div_32 = 5,
    clock_div_64 = 6,
    clock_div_128 = 7,
    clock_div_256 = 8
} clock_div_t;

#define clock_prescale_set(x) \
{ \
        uint8_t tmp = _BV(CLKPCE); \
        __asm__ __volatile__ ( \
                "in __tmp_reg__,__SREG__" "\n\t" \
                "cli" "\n\t" \
                "sts %1, %0" "\n\t" \
                "sts %1, %2" "\n\t" \
                "out __SREG__, __tmp_reg__" \
                : /* no outputs */ \
                : "d" (tmp), \
                  "M" (_SFR_MEM_ADDR(CLKPR)), \
                  "d" (x) \
                : "r0"); \
}
#endif

#endif
