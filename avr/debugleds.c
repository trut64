/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "debugleds.h"

#include "fcpu.h"
#include <util/delay.h>
#include <avr/io.h>

#if HWBOARD==DEVTRUT
void debugled_init()
{
	//DDRD |= 1;
}

void debugled(uint8_t led, uint8_t state)
{
	/*
	if(led == 0)
	{
		switch(state)
		{
		case 0:
			PORTD &= ~1;
			break;
		case 1:
			PORTD |= 1;
			break;
		}
	}
	*/
}

void toggleled(uint8_t led)
{
	/*
	if(led == 0)
	{
		PIND = 1;
	}
	*/
}

void debugled4(uint8_t b)
{
}

void debugled8(uint8_t b)
{
}

void debugled8i(uint8_t b)
{
}

#else /* HWBOARD==DEVTRUT */

void debugled_init()
{
	DDRD = 0xff;
}

void debugled(uint8_t led, uint8_t state)
{
	uint8_t val = (0x10 << led);

	switch(state)
	{
	case 0:
		PORTD &= ~val;
		break;
	case 1:
		PORTD |= val;
		break;
	}
}

void toggleled(uint8_t led)
{
	uint8_t val = (0x10 << led);

	PIND = val;
}

void debugled4(uint8_t b)
{
	PORTD = (PORTD & 0x0f) | (b << 4);
}

void debugled8(uint8_t b)
{
	debugled4(0x0f);
	_delay_ms(200);
	debugled4(b >> 4);
	_delay_ms(250);
	_delay_ms(250);
	_delay_ms(250);
	debugled4(b & 0x0f);
	_delay_ms(250);
	_delay_ms(250);
	_delay_ms(250);
	debugled4(0);
	_delay_ms(250);
	_delay_ms(250);
}

void debugled8i(uint8_t b)
{
	debugled4(0x0f);
	_delay_ms(200);
	debugled4(b >> 4);
	while(PINE & (1 << PINE2));
	debugled4(b & 0xff);
	while(!(PINE & (1 << PINE2)));
	while(PINE & (1 << PINE2));
	debugled4(0);
	while(!(PINE & (1 << PINE2)));
}
#endif /* HWBOARD==DEVTRUT */
