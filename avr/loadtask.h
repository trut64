/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef LOADTASK_H
#define LOADTASK_H

#include "buffer.h"

#define LOADTASK_MODE_LOAD 0
#define LOADTASK_MODE_WRITETAPE 1

// loadtask handles both load and writetape modes.
void loadtask_init();

// Call loadtask_reset() when operating mode is changed, or when reset is
// needed.
void loadtask_setup(uint8_t mode);

// Returns non-zero when the task is finished and TRUT can go back to
// pass mode.
uint8_t loadtask_finished();

// Call to set the expected length of the data. Loading will continue after
// all data has been sent, but buffer underrun messages will not be sent.
void loadtask_setdatalength(uint16_t length);

buf_t loadtask_bufferusage();

// Should be called periodically.
void loadtask();

#endif
