/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef USBDESCRIPTORS_H
#define USBDESCRIPTORS_H

#include <avr/pgmspace.h>

#include "trutproto.h"

#define USB_LANGID 0x0809          // English (United Kingdom)

extern uint8_t ep_sizes[NUM_EPS];

typedef struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t usb;
	uint8_t deviceClass;
	uint8_t deviceSubClass;
	uint8_t deviceProtocol;
	uint8_t maxPacketSize;
	uint16_t vendorID;
	uint16_t productID;
	uint16_t device;
	uint8_t manufacturer;
	uint8_t product;
	uint8_t serialNumber;
	uint8_t numConfigurations;
} usb_device_descriptor;

typedef struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t totalLength;
	uint8_t numInterfaces;
	uint8_t configurationValue;
	uint8_t configuration;
	uint8_t attributes;
	uint8_t maxPower;
} usb_configuration_descriptor;

typedef struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint8_t interfaceNumber;
	uint8_t alternateSetting;
	uint8_t numEndpoints;
	uint8_t interfaceClass;
	uint8_t interfaceSubClass;
	uint8_t interfaceProtocol;
	uint8_t interface;
} usb_interface_descriptor;

typedef struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint8_t endpointAddress;
	uint8_t attributes;
	uint16_t maxPacketSize;
	uint8_t interval;
} usb_endpoint_descriptor;

typedef struct
{
	usb_configuration_descriptor configuration;
	usb_interface_descriptor interface_0;
	usb_endpoint_descriptor if1_endpoint_1;
	usb_endpoint_descriptor if1_endpoint_2;
	usb_interface_descriptor interface_1_setting_0;
	usb_endpoint_descriptor if1_endpoint_3_bulkout;
	usb_interface_descriptor interface_1_setting_1;
	usb_endpoint_descriptor if1_endpoint_3_bulkin;
	usb_interface_descriptor interface_1_setting_2;
	usb_endpoint_descriptor if1_endpoint_3_introut;
	usb_interface_descriptor interface_1_setting_3;
	usb_endpoint_descriptor if1_endpoint_3_intrin;
} configuration_descriptor_1;

typedef struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t language00;
} string_descriptor_0;

#define USB_NUMSTRINGS 9
extern PGM_P PROGMEM string_descriptors[USB_NUMSTRINGS];

extern usb_device_descriptor PROGMEM device_descriptor;
extern configuration_descriptor_1 PROGMEM configuration_1;
extern string_descriptor_0 PROGMEM string_languages;

#endif
