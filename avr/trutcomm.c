/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <string.h>

#include "trutcomm.h"

#include "avrusb.h"
#include "debugleds.h"
#include "usbdescriptors.h"
#include "dbuf.h"
#include "buffer.h"

uint8_t trut_flags = 0;

// Common logic for messages. Checks if message transmission is enabled,
// and if the previous message has been sent in case blocking is disabled.
static inline uint8_t trut_msg_start()
{
	// Select the endpoint.
	usb_select_ep(TRUT_EP_MSG);

	// If not blocking and the previous message has not been sent, do not
	// send this message.
	if(!usb_transin_canstart())
		return 1;

	// Signal the start of a transaction.
	usb_transin_start();

	return 0;
}

static inline void trut_msg_end()
{
	// Signal the end of a transaction.
	usb_transin_end();
}

void trut_msg_str(const char* str)
{
	uint8_t written;
	uint8_t len = strlen(str);

	if(!trut_msgenabled())
		return;

	while(trut_msg_start());

	// Write the message type: STR message
	usb_writebyte(TRUT_MSG_STR);

	// Write the length of the message.
	usb_writebyte(len);

	while(len)
	{
		written = usb_transin_block(str, len, 0);
		str += written;
		len -= written;
	}

	trut_msg_end();
}

void trut_msg_hex(const uint8_t* data, uint8_t len)
{
	uint8_t written;

	if(!trut_msgenabled())
		return;

	while(trut_msg_start());

	// Write the message type: HEX message
	usb_writebyte(TRUT_MSG_HEX);

	// Write the length of the message.
	usb_writebyte(len);

	while(len)
	{
		written = usb_transin_block(data, len, 0);
		data += written;
		len -= written;
	}

	trut_msg_end();
}

void trutcomm_task()
{
	uint8_t len;

	// Return if messages are not enabled.
	if(!trut_msgenabled())
		return;

	// Return if there are no messages in the buffer.
	if(msgbuffer_isempty())
		return;

	// Return if the previous message has not been sent.
	if(trut_msg_start())
		return;

	// Get the message length.
	len = msgbuffer_getbyte();

	// Write the message.
	while(len--)
		usb_writebyte(msgbuffer_getbyte());

	// Send the packet.
	trut_msg_end();
}

uint8_t trut_has_cmd()
{
	usb_select_ep(TRUT_EP_CMD);

	return usb_pktout_received();
}

uint8_t trut_cmd_read(uint8_t* cmd, uint8_t* data, uint8_t* len)
{
	uint8_t l = 0;

	// Select the endpoint.
	usb_select_ep(TRUT_EP_CMD);

	// Return if there is no packet received.
	if(!usb_pktout_received())
		return 1;

	// Acknowledge packet reception.
	usb_pktout_start();

	// Read the type of the received command.
	if(!usb_pktout_byte(cmd))
	{
		usb_pktout_end();
		return 1;
	}

	// Read the length of the received command data.
	if(!usb_pktout_byte(&l))
	{
		usb_pktout_end();
		return 1;
	}

	*len = l;

	// Read the command arguments. Return if the length does not match the
	// specified.
	if(!(usb_pktout_block(data, l) == l))
	{
		usb_pktout_end();
		return 1;
	}

	// Clear the receive buffer.
	usb_pktout_end();

	return 0;
}

uint8_t trut_has_data()
{
	usb_select_ep(TRUT_EP_DATA);

	return usb_pktout_received();
}

uint8_t trut_read_data_to_buffer()
{
	uint16_t w;

	// Select the endpoint.
	usb_select_ep(TRUT_EP_DATA);

	// Return if there is no packet received.
	if(!usb_pktout_received())
		return 1;

	// Acknowledge packet reception.
	usb_pktout_start();

	// While there are words left in the usb fifo, write them to the buffer.
	while(usb_pktout_byte(((uint8_t*) &w)+1))
	{
		if(!usb_pktout_byte(((uint8_t*) &w)))
		{
			usb_pktout_end();
			return 1;
		}

		buffer_addword(w);
	}

	// Clear the receive buffer.
	usb_pktout_end();

	return 0;
}

uint8_t trut_can_send_data()
{
	usb_select_ep(TRUT_EP_DATA);

	return usb_fifofree();
}

uint8_t trut_send_buffer_data()
{
	uint16_t w;

	usb_select_ep(TRUT_EP_DATA);

	if(!usb_fifofree())
		return 1;

	usb_cleartxini();

	while(!buffer_isempty() && usb_rwal())
	{
		w = buffer_getword();

		usb_writebyte(w >> 8);
		usb_writebyte(w & 0x00ff);
	}

	usb_sendpacket();

	return 0;
}
