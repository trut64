/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef TRUTPROFILE_H
#define TRUTPROFILE_H

#ifdef TRUT_PROFILE

#include <inttypes.h>

// Define number of measurement points
#define NPROFILE 8
#define PROFILE_PRESCALE 8 // must be 1, 8, 64, or 256

// Call in main() to initialize profiler
void profile_init();

// Call to start/stop profiler for the specified measure.
void profile_start(uint8_t m);
void profile_stop(uint8_t m);

// Returns the worst case measurement for m.
uint16_t profile_wc(uint8_t m);

#endif

#endif
