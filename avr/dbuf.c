/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "dbuf.h"

uint8_t debugbuf[DBUF_SIZE];
uint8_t debugi;

void dbuf_init()
{
	debugi = 0;
}

void dbuf(uint8_t a)
{
	if(debugi >= DBUF_SIZE-2)
		return;

	debugbuf[2+debugi++] = a;
}

void dbufp(uint8_t* p, uint8_t count)
{
	while(count--)
		dbuf(*(p++));
}

void dbufn(uint8_t n, uint8_t a)
{
	if(2+n < DBUF_SIZE)
	{
		debugbuf[2+n] = a;
		if(debugi <= n)
			debugi = n+1;
	}
}
