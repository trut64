/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "usbdescriptors.h"

#include "avrusb.h"

uint8_t ep_sizes[NUM_EPS] =
{ EP_CONTROL_SIZE, TRUT_EP_MSG_SIZE, TRUT_EP_CMD_SIZE, TRUT_EP_DATA_SIZE };

usb_device_descriptor PROGMEM device_descriptor =
{
	sizeof(usb_device_descriptor),   // Length of descriptor.
	USB_DESCRIPTOR_DEVICE,           // Descriptor type.
	0x200,                           // USB specification version.
	0,                               // Device class.
	0,                               // Device subclass.
	0,                               // Device protocol.
	EP_CONTROL_SIZE,                 // Maximum packet size for EP0.
	0x03eb,                          // Vendor ID.
	0,                               // Product ID.
	0x1000,                          // Release number.
	1,                               // ID of manufacturer string.
	2,                               // ID of product string.
	3,                               // ID of serial number string.
	1                                // Number of configurations.
};

configuration_descriptor_1 PROGMEM configuration_1 =
{
	{
		sizeof(usb_configuration_descriptor),    // Length of configuration descriptor.
		USB_DESCRIPTOR_CONFIGURATION,            // Descriptor type.
		sizeof(configuration_descriptor_1),      // Total length of configuration data.
		2,                                       // Number of interfaces.
		1,                                       // Configuration value.
		4,                                       // ID of configuration string.
		0x80,                                    // Attributes (0x40: self-powered, 0x20: remote wakeup).
		50                                       // Maximum power used (in 2mA steps)
	},
	{
		sizeof(usb_interface_descriptor),        // Length of interface descriptor.
		USB_DESCRIPTOR_INTERFACE,                // Descriptor type.
		0,                                       // Number of this interface.
		0,                                       // Number of this alternate setting.
		2,                                       // Number of endpoints (excluding zero).
		0,                                       // Interface class.
		0,                                       // Interface subclass.
		0,                                       // Interface protocol.
		5                                        // ID of interface string.
	},
	{
		sizeof(usb_endpoint_descriptor),         // Length of endpoint descriptor.
		USB_DESCRIPTOR_ENDPOINT,                 // Descriptor type.
		0x80 | TRUT_EP_MSG,                      // Lower nibble: endpoint number.
		// Higher nibble: 0x80 for IN endpoint, 0x00 for OUT endpoint
		0x2,                                     // Endpoint attributes:
		// Bits 1..0: Endpoint type
		//   00: Control, 01: Isochronous, 10: Bulk, 11: Interrupt
		// Bits 5..4: Only used for isochronous endpoints.
		// Bits 7..6: Reserved.
		16,                                      // Endpoint maximum packet size.
		0                                        // Polling interval in ms for interrupt endpoints.
	},
	{
		sizeof(usb_endpoint_descriptor),         // Length of endpoint descriptor.
		USB_DESCRIPTOR_ENDPOINT,                 // Descriptor type.
		TRUT_EP_CMD,                             // Lower nibble: endpoint number.
		// Higher nibble: 0x80 for IN endpoint, 0x00 for OUT endpoint
		0x2,                                     // Endpoint attributes:
		// Bits 1..0: Endpoint type
		//   00: Control, 01: Isochronous, 10: Bulk, 11: Interrupt
		// Bits 5..4: Only used for isochronous endpoints.
		// Bits 7..6: Reserved.
		16,                                      // Endpoint maximum packet size.
		0                                        // Polling interval in ms for interrupt endpoints.
	},
	{
		sizeof(usb_interface_descriptor),        // Length of interface descriptor.
		USB_DESCRIPTOR_INTERFACE,                // Descriptor type.
		1,                                       // Number of this interface.
		0,                                       // Number of this alternate setting.
		1,                                       // Number of endpoints (excluding zero).
		0,                                       // Interface class.
		0,                                       // Interface subclass.
		0,                                       // Interface protocol.
		6                                        // ID of interface string.
	},
	{
		sizeof(usb_endpoint_descriptor),         // Length of endpoint descriptor.
		USB_DESCRIPTOR_ENDPOINT,                 // Descriptor type.
		TRUT_EP_DATA,                            // Lower nibble: endpoint number.
		// Higher nibble: 0x80 for IN endpoint, 0x00 for OUT endpoint
		0x2,                                     // Endpoint attributes:
		// Bits 1..0: Endpoint type
		//   00: Control, 01: Isochronous, 10: Bulk, 11: Interrupt
		// Bits 5..4: Only used for isochronous endpoints.
		// Bits 7..6: Reserved.
		64,                                      // Endpoint maximum packet size.
		0                                        // Polling interval in ms for interrupt endpoints.
	},
	{
		sizeof(usb_interface_descriptor),        // Length of interface descriptor.
		USB_DESCRIPTOR_INTERFACE,                // Descriptor type.
		1,                                       // Number of this interface.
		1,                                       // Number of this alternate setting.
		1,                                       // Number of endpoints (excluding zero).
		0,                                       // Interface class.
		0,                                       // Interface subclass.
		0,                                       // Interface protocol.
		7                                        // ID of interface string.
	},
	{
		sizeof(usb_endpoint_descriptor),         // Length of endpoint descriptor.
		USB_DESCRIPTOR_ENDPOINT,                 // Descriptor type.
		0x80 | TRUT_EP_DATA,                     // Lower nibble: endpoint number.
		// Higher nibble: 0x80 for IN endpoint, 0x00 for OUT endpoint
		0x2,                                     // Endpoint attributes:
		// Bits 1..0: Endpoint type
		//   00: Control, 01: Isochronous, 10: Bulk, 11: Interrupt
		// Bits 5..4: Only used for isochronous endpoints.
		// Bits 7..6: Reserved.
		64,                                      // Endpoint maximum packet size.
		0                                        // Polling interval in ms for interrupt endpoints.
	},
	{
		sizeof(usb_interface_descriptor),        // Length of interface descriptor.
		USB_DESCRIPTOR_INTERFACE,                // Descriptor type.
		1,                                       // Number of this interface.
		2,                                       // Number of this alternate setting.
		1,                                       // Number of endpoints (excluding zero).
		0,                                       // Interface class.
		0,                                       // Interface subclass.
		0,                                       // Interface protocol.
		8                                        // ID of interface string.
	},
	{
		sizeof(usb_endpoint_descriptor),         // Length of endpoint descriptor.
		USB_DESCRIPTOR_ENDPOINT,                 // Descriptor type.
		TRUT_EP_DATA,                            // Lower nibble: endpoint number.
		// Higher nibble: 0x80 for IN endpoint, 0x00 for OUT endpoint
		0x3,                                     // Endpoint attributes:
		// Bits 1..0: Endpoint type
		//   00: Control, 01: Isochronous, 10: Bulk, 11: Interrupt
		// Bits 5..4: Only used for isochronous endpoints.
		// Bits 7..6: Reserved.
		64,                                      // Endpoint maximum packet size.
		1                                        // Polling interval in ms for interrupt endpoints.
	},
	{
		sizeof(usb_interface_descriptor),        // Length of interface descriptor.
		USB_DESCRIPTOR_INTERFACE,                // Descriptor type.
		1,                                       // Number of this interface.
		3,                                       // Number of this alternate setting.
		1,                                       // Number of endpoints (excluding zero).
		0,                                       // Interface class.
		0,                                       // Interface subclass.
		0,                                       // Interface protocol.
		9                                        // ID of interface string.
	},
	{
		sizeof(usb_endpoint_descriptor),         // Length of endpoint descriptor.
		USB_DESCRIPTOR_ENDPOINT,                 // Descriptor type.
		0x80 | TRUT_EP_DATA,                     // Lower nibble: endpoint number.
		// Higher nibble: 0x80 for IN endpoint, 0x00 for OUT endpoint
		0x3,                                     // Endpoint attributes:
		// Bits 1..0: Endpoint type
		//   00: Control, 01: Isochronous, 10: Bulk, 11: Interrupt
		// Bits 5..4: Only used for isochronous endpoints.
		// Bits 7..6: Reserved.
		64,                                      // Endpoint maximum packet size.
		1                                        // Polling interval in ms for interrupt endpoints.
	}
};

string_descriptor_0 PROGMEM string_languages =
{
	sizeof(string_descriptor_0),
	USB_DESCRIPTOR_STRING,
	USB_LANGID
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[4];
} manufacturer_string_t;
manufacturer_string_t PROGMEM manufacturer_string =
{
	sizeof(manufacturer_string_t),
	USB_DESCRIPTOR_STRING,
	{'H', 'E', 'A', 'D'}
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[4];
} product_string_t;
product_string_t PROGMEM product_string =
{
	sizeof(product_string_t),
	USB_DESCRIPTOR_STRING,
	{'T', 'R', 'U', 'T'}
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[4];
} serial_string_t;
serial_string_t PROGMEM serial_string =
{
	sizeof(serial_string_t),
	USB_DESCRIPTOR_STRING,
	{'6', '5', '1', '0'}
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[3];
} configuration_string_t;
configuration_string_t PROGMEM configuration_string =
{
	sizeof(configuration_string_t),
	USB_DESCRIPTOR_STRING,
	{'C', 'F', 'G'}
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[7];
} commif_string_t;
commif_string_t PROGMEM commif_string =
{
	sizeof(commif_string_t),
	USB_DESCRIPTOR_STRING,
	{'C', 'o', 'm', 'm', ' ', 'I', 'F'}
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[12];
} loadifbulk_string_t;
loadifbulk_string_t PROGMEM loadifbulk_string =
{
	sizeof(loadifbulk_string_t),
	USB_DESCRIPTOR_STRING,
	{'L', 'o', 'a', 'd', ' ', 'I', 'F', ' ', 'B', 'u', 'l', 'k'}
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[12];
} dumpifbulk_string_t;
dumpifbulk_string_t PROGMEM dumpifbulk_string =
{
	sizeof(dumpifbulk_string_t),
	USB_DESCRIPTOR_STRING,
	{'D', 'u', 'm', 'p', ' ', 'I', 'F', ' ', 'B', 'u', 'l', 'k'}
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[12];
} loadifintr_string_t;
loadifintr_string_t PROGMEM loadifintr_string =
{
	sizeof(loadifintr_string_t),
	USB_DESCRIPTOR_STRING,
	{'L', 'o', 'a', 'd', ' ', 'I', 'F', ' ', 'I', 'n', 't', 'r'}
};

typedef
struct
{
	uint8_t length;
	uint8_t descriptorType;
	uint16_t str[12];
} dumpifintr_string_t;
dumpifintr_string_t PROGMEM dumpifintr_string =
{
	sizeof(dumpifintr_string_t),
	USB_DESCRIPTOR_STRING,
	{'D', 'u', 'm', 'p', ' ', 'I', 'F', ' ', 'I', 'n', 't', 'r'}
};

PGM_P PROGMEM string_descriptors[] =
{
	(PGM_P)&manufacturer_string,
	(PGM_P)&product_string,
	(PGM_P)&serial_string,
	(PGM_P)&configuration_string,
	(PGM_P)&commif_string,
	(PGM_P)&loadifbulk_string,
	(PGM_P)&dumpifbulk_string,
	(PGM_P)&loadifintr_string,
	(PGM_P)&dumpifintr_string
};
