/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "motor.h"
#include "c64tape.h"
#include "avrusb.h"

uint16_t motor_status = 0;
uint16_t motor_latency_ms = 0;

static inline uint8_t motor_pin()
{
	return PIN_MOTOR & _BV(P_MOTOR);
}

static inline uint8_t motor_transition()
{
	return (motor_status & MOTOR_TRANSITION_MASK) != 0;
}

static inline void motor_cleartransition()
{
	motor_status &= ~MOTOR_TRANSITION_MASK;
}

static inline void motor_settransition()
{
	motor_status |= MOTOR_TRANSITION_MASK;
}

static inline uint16_t motor_timeout()
{
	return motor_status & MOTOR_TIMEOUT_MASK;
}

static inline void motor_settimeout(uint16_t t)
{
	motor_status &= ~MOTOR_TIMEOUT_MASK;
	motor_status |= (t & MOTOR_TIMEOUT_MASK);
}

static inline void motor_seton()
{
	motor_status |= MOTOR_STATUS_MASK;
}

static inline void motor_setoff()
{
	motor_status &= ~MOTOR_STATUS_MASK;
}

void motor_task()
{
	uint8_t pin = motor_pin();

	if((motor_ison() && pin) || (motor_isoff() && !pin))
	{
		motor_cleartransition();
	}
	else
	{
		if(motor_transition())
		{
			// You don't have to understand this. :)
			if(((usb_framenumber() - motor_timeout()) &
				((MOTOR_TIMEOUT_MASK+1) >> 1)) == 0)
			{
				if(pin)
					motor_seton();
				else
					motor_setoff();

				motor_cleartransition();
			}
		}
		else
		{
			motor_settransition();
			motor_settimeout(usb_framenumber() + motor_latency());
		}
	}
}
