/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <util/crc16.h>
#include "buffer.h"

volatile enum buffer_status buffer_flags = BUFFER_OK;
volatile uint16_t buffer[BUFFER_CAPACITY];
volatile buf_t buffer_start = 0;
volatile buf_t buffer_size = 0;
volatile uint16_t buffer_crc = 0xffff;
volatile uint32_t buffer_crc_len = 0;

void update_crc(uint16_t w)
{
	buffer_crc = _crc16_update(buffer_crc, w >> 8);
	buffer_crc = _crc16_update(buffer_crc, w & 0xff);
	buffer_crc_len++;
}
