/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifdef TRUT_PROFILE

#include "trutprofile.h"

#include <avr/io.h>
#include <avr/interrupt.h>

static uint16_t profile_stamps[NPROFILE];
static uint16_t profile_mwc[NPROFILE];

volatile static uint8_t timer0_high;

// Allocated measurements:
// 0 - TIMER1_COMPA interrupt
// 1 - TIMER1_OVF interrupt
// 2 - TIMER1_CAPT interrupt

// Atomic read of timer stamp.
static inline uint16_t profile_stamp()
{
	// Save interrupt enable flag.
	uint8_t sreg = SREG;
	uint8_t low;
	uint8_t high;

	cli();

	low = TCNT0;
	if(low < 0xf0 && (TIFR0 & _BV(TOV0)))
		high = timer0_high + 1;
	else
		high = timer0_high;

	// Reenable interrupts.
	SREG = sreg;

	return (high << 8) | low;
}

void profile_init()
{
	uint8_t i;

	for(i = 0; i < NPROFILE; i++)
	{
		profile_stamps[i] = profile_mwc[i] = 0;
	}

	timer0_high = 0;

	TCCR0A = 0;
#if PROFILE_PRESCALE == 1
	TCCR0B = _BV(CS00);
#elif PROFILE_PRESCALE == 8
	TCCR0B = _BV(CS01);
#elif PROFILE_PRESCALE == 64
	TCCR0B = _BV(CS00) | _BV(CS01);
#elif PROFILE_PRESCALE == 256
	TCCR0B = _BV(CS02);
#else
#error Invalid PROFILE_PRESCALE value
#endif
	TIMSK0 |= _BV(TOIE0);
}

void profile_start(uint8_t m)
{
	profile_stamps[m] = profile_stamp();
}

void profile_stop(uint8_t m)
{
	uint16_t len = profile_stamp() - profile_stamps[m];

	if(len > profile_mwc[m])
		profile_mwc[m] = len;
}

uint16_t profile_wc(uint8_t m)
{
	return profile_mwc[m];
}

ISR(TIMER0_OVF_vect)
{
	timer0_high++;
}

#endif // TRUT_PROFILE
