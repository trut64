/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef MOTOR_H
#define MOTOR_H

#include <stdint.h>

#define MOTOR_TIMEOUT_MASK 0x07ff
#define MOTOR_STATUS_MASK 0x8000
#define MOTOR_TRANSITION_MASK 0x4000

extern uint16_t motor_status;
extern uint16_t motor_latency_ms;

static inline void motor_init() { }

static inline uint8_t motor_ison()
{
	return (motor_status & MOTOR_STATUS_MASK) != 0;
}

static inline uint8_t motor_isoff()
{
	return (motor_status & MOTOR_STATUS_MASK) == 0;
}

static inline uint16_t motor_latency()
{
	return motor_latency_ms;
}

static inline void motor_setlatency(uint16_t t)
{
	motor_latency_ms = t;
}

void motor_task();

#endif
