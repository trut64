/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef COMMON_H
#define COMMON_H

#define min(x, y) (x < y ? x : y)
#define max(x, y) (x > y ? x : y)

#endif
