/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include "fcpu.h"

#include <avr/io.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/power.h>

#include "avrusb.h"
#include "buffer.h"
#include "debugleds.h"
#include "trutcomm.h"
#include "usbdescriptors.h"
#include "dbuf.h"
#include "loadtask.h"
#include "passtask.h"
#include "dumptask.h"
#include "timer1.h"
#include "stackusage.h"
#include "c64tape.h"
#include "motor.h"
#include "trutcontrol.h"
#if HWBOARD==DEVTRUT
#include "avrlibc162.h"
#endif
#include "trutprofile.h"

int main(void)
{
	wdt_disable();
	debugled_init();

	debugled(3, 1);
	_delay_ms(10);
	debugled(3, 0);

#if HWBOARD==DEVTRUT
	// Change to external clock.
	//CKSEL0 |= (1 << EXTE);
	//while(!(CKSTA & (1 << EXTON)));
	//CKSEL0 |= (1 << CLKS);
	//CKSEL0 &= ~(1 << RCE);

	// Enable 3.3V regulator (should be enabled already)
	REGCR &= (1 << REGDIS);
#endif

	_delay_ms(10);

	clock_prescale_set(clock_div_1);

	c64tape_init();
	buffer_init();
	usb_init();
	passtask_init();
	loadtask_init();
	dumptask_init();
	timer1_init();
	motor_init();
#ifdef TRUT_PROFILE
	profile_init();
#endif

	motor_setlatency(200);

	sei();

	trut_setmsgblocks(1);

	controller(); // does not return

	for(;;);
}

