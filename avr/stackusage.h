/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef STACKUSAGE_H
#define STACKUSAGE_H


/* Returns an estimate of the maximal size of the stack (in bytes)
 * during the programs lifetime so far. */
uint16_t stack_usage(void);

#endif
