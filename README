TRUT64 is a hardware device, based on the Atmel AT90USB162 8-bit micro
processor, emulating Commodore's famous 1530 C2N Datasette. The idea
is simple: you store your digitalized tapes on your PC hard drive, and
whenever you want to load your favourite tape, you feed it directly to
the C64 through TRUT64. In other words, connected to the C64's
cassette port and receiving data from a PC via USB, TRUT64 is acting
as a datasette device playing your tape of choice. TRUT64 also
supports recording, and you can use it to backup all of your precious
tapes.

The project was born in 2003 and a working prototype was demonstrated
at LCP2003. Back then, the PC communicated with TRUT64 using the
serial port. However, today's modern PCs are not equipped with serial
ports, and serial communication is neither reliable nor precise enough
for perfectionists like us. Therefore, to obtain sharp precision as
well as a device that will last for many years to come, we decided to
use USB instead.

Note that this repository only contains the software part of the
TRUT64 project. To get a working TRUT64 you also need the hardware
part.

Any questions, comments, and suggestions are kindly directed to one of
the developers:

*  Anton Blad <anton.blad@gmail.com>
*  Fredrik Kuivinen <frekui@gmail.com>
*  Jakob Ros�n <jakob.rosen@gmail.com>

The homepage of TRUT64 can be found at
http://www.ida.liu.se/~jakro/TRUT64


TRUT64 is an open source project, see the file LICENSE for the
licensing details.
