all:
	make -C host
	make -C avr

TAGS:
	rm -f TAGS
	find . -name '*.[hcS]' -print | xargs etags -a

tags:
	rm -f tags
	find . -name '*.[hcS]' -print | xargs ctags -a

.PHONY: clean
clean:
	-rm -f TAGS tags
	find -name "*~" | xargs rm -f
	make -C host clean
	make -C avr clean
