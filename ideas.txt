This file contains ideas that might or might not be realized in the TRUT64
project.

U-Monkey's note: Ja, jag �r lat och skriver p� svenska! Detta f�r att f� ner
id�erna s� snabbt som m�jligt innan de l�mnar mitt huvud. Ja, �ndra vad ni vill!
Huvudsaken �r att ni l�ser dokumentet :)


********************************************************************************
* Idea: Recorder Justage                                                       *
* Added by: U-Monkey                                                           *
********************************************************************************

Detta k�nns som en ganska viktig funktion att ha i GUI:et.

Ett klassiskt exempel p� hur det brukar se ut finns h�r:
http://www.diebek.de/c64/
http://digilander.libero.it/tcengineer/c64/dc2n/images/DC2N_TAPMoni_DC2N_edition.png

Den sistan�mnda bilden kommer fr�n italienarens projekt.

Jag har lyckats luska ut hur det hela funkar. Man f�r v�lja vilken typ av
data man vill analysera (ROM-loader, Turbo Tape etc). Sedan tittar man p�
spridningen av pulsl�ngder relativt de f�rv�ntade l�ngderna enligt valt format.
Turbo tape har tv� pulsl�ngder medan ROM-loadern har tre. Det inneb�r att
alla uppm�tta pulsl�ngder skall ligga n�ra n�gon av dessa. Ju mindre avvikelser
desto b�ttre tonhuvusdsinst�llning.

H�r finns det m�jligheter att lyxa till det ordentligt! T ex hade man f�rutom
ROM loader och Turbo Tape kunnat l�ta anv�ndaren g�ra en egen konfiguration
(som sedan kan v�ljas fr�n en drop down-meny eller motsvarande). M�nga loaders
�terkommer st�ndigt, typ Novaload, Freeload etc. Man hade ju kunnat l�ta
n�gon fanatiker (l�s: SLC) bidra med data om vilka pulsl�ngder som g�ller f�r
dessa loaders och visualisera sj�lva dumpningsprocessen p� detta s�tt.



********************************************************************************
* Idea: Automatisk hantering av ZIP-arkiv                                      *
* Added by: U-Monkey                                                           *
********************************************************************************

Som av en slump testade jag en gameboy-emulator f�r n�gra m�nader sedan. David,
k�nd genom TRUT-projektet, gav mig ett arkiv med ca 1000 spel, och jag testade
2 eller 3 av dem innan jag tr�ttnade. Arkivet var organiserat som kataloger i
vilka det l�g zip-filer inneh�llande speldata i ett helt annat format (precis
s� som jag organiserat v�rt aktuella arkiv).
N�got som jag upplevde som v�ldigt trevligt var att man inte beh�vde packa
upp ZIP-filerna utan emulatorn extraherade det den beh�vde automatiskt.

En TAP-fil kan v�xa 20-30 ggr n�r man packar upp den, s� ZIP-filer k�nns i h�gsta
grad vettigt i ett TAP-arkiv. Det �r inte j�ttejobbigt att packa upp det spel man
f�r tillf�llet vill spela, men det skulle nog upplevas som v�ldigt behagligt att
slippa t�nka p� detta (och att sedan deleta el�ndet).

Finns det n�got tillg�ngligt bibliotek f�r hantering av ZIP-filer s� vore detta
kanske en schyst grej att implementera.



********************************************************************************
* Idea: Kompletterande XML-fil                                                 *
* Added by: U-Monkey                                                           *
********************************************************************************

Visst vore det ganska trevlig om man hade kunnat b�dda in en massa information
i varje TAP-fil? Tex...

- Namn p� personen som rippade filen, datum samt utrustning (TRUT hoppas vi!)
- Highscores (som man hade kunnat redigera p� ett enkelt s�tt via GUI:t!)
- R�knar-bookmarks (f�r multiload-spel)
- Inst�llningar (t ex motor-latency och dylikt)
- Path till covers, som hade kunnat visas medan man laddar (eller varf�r inte n�r man
v�ljer spel, om man vill g�ra en ambiti�s spelv�ljarsk�rm?)
- Allm�nna kommentarer om spelet.

Nu kan vi inte b�dda in saker i TAP-formatet, men en l�sning �r att ha en
XML-fil som kompletterar varje TAP-fil (en annan �r att ha en central databas).

Denna fil kan ha samma namn som TAP-filen fast med annorlunda �ndelse.
Informationen skulle f�rst�s presenteras i GUI:t p� n�got vackert s�tt.

Eftersom jag inte �r bra p� QT vet jag inte hur dessa features skulle
hanteras av GUI:t, men jag t�nker mig en vy med flikar d�r man t ex kan
v�lja "hiscores" och skriva in nya scores (som sorteras p� ett l�mpligt
s�tt), eller varf�r inte "notes" d�r man kan skriva vad man vill.

Observera att jag h�r med hiscores avser lokala och h�gst personliga
hiscores. De sparas ju aldrig, s� man gl�mmer direkt bort vad man
egentligen fick. Att direkt kunna skriva in dem i TRUT-gui:t (som
dessutom sparar datum etc) vore r�tt kul.


********************************************************************************
* Idea: Uppsamling av statistik samt publicering av denna � la last.fm         *
* Added by: U-Monkey                                                           *
********************************************************************************

<@Slow-load> en annan grej som man kan g�ra om man skulle f� en massa tid �ver, och som hade varit schyst, �r last.fm-funktionalitet
<@Slow-load> dvs klienten samlar statistik �ver laddade spel
<@Slow-load> sedan kan man kolla p� det sj�lv, eller ladda upp p� n�gon webserver
<@Slow-load> s� kan man se vad diverse TRUT-anv�ndare laddar
<@Slow-load> spel som laddas ofta m�ste ju ha n�gon typ av underh�llningsv�rde
<@Slow-load> och p� s� s�tt kan man f� tips :)
<@Slow-load> <@Slow-load> f�rdefinierade t om, i den d�r XML-filen jag snackat med <-- snackat om




********************************************************************************
* Idea: Frist�ende verktyg - Turbo Tape-splitter                               *
* Added by: U-Monkey                                                           *
********************************************************************************

Som Fredrik numera �r medveten om st�ter man ofta p� turboband som helt saknar
f�rteckning. Dessa band brukar inneh�lla h�gst tv� filer (Turbo 250
och Recorder Justage) i ROM-loader format och sedan 40-50 spel i Turbo
Tape-format. Det hade varit ganska trevligt om man bara hade kunnat ta en
dump av ett s�dant band och sedan mata in dumpen i ett verktyg som:

1) Identiferade inneh�llet
2) Sparade ner inneh�llet i separata filer.

F�r turbofiler hade man kunnat t�nka sig ett nytt och kompakt format
(TAP funkar ju f�rst�s ocks�, men det �r lite resurssl�seri) d�r en bit
representerar en puls.

Detta verktyg kan ju vara helt frist�ende fr�n GUI:et, �ven om det kanske hade
varit trevligt att integrera dem.


********************************************************************************
* Idea: GUI - Identifiering av fil                                             *
* Added by: U-Monkey                                                           *
********************************************************************************

En schyst grej skulle vara om GUI:t kan analysera den fil som precis skall
laddas, och presentera information om denna.
Detta kan ske genom att man analyserar eventuell ROM-loader-header och
presenterar det som st�r i den. Om ingen ROM-loader-header hittas kan man s�ka
efter en Turbo Tape-header, och samtidigt allts� klassificera filen.
Om ingen k�nd header hittas kan man inte presentera s� mycket, men det �r ju
ocks� relevant information i sig :)



********************************************************************************
* Idea: Central hiscore-databas                                                *
* Added by: U-Monkey                                                           *
********************************************************************************

En central databas d�r alla lokala hiscores samlas!
Om man bara l�ter TRUT-�gare registrera sina highscores h�r (vilket de bara kan
g�ra genom TRUT-klienten - som k�nner av om man har en TRUT innan den submittar)
s� slipper man sannolikt en massa fakade scores inmatade av "emulator kiddies".

Man hade kunnat ha ett webinterface f�r att v�lja spel, anv�ndare (eventuellt)
via valt nick eller bara kolla de senaste registrerade po�ngen.

Man hade ocks� kunnat ha ett tillh�rande webforum � la IPB, SMF eller PHPBB,
d�r de senaste po�ngen hade kunnat diskuteras.

Detta blir som en community, vilket �r bra ur marknadsf�ringsperspektiv
dessutom (TDDC02 i v�ra hj�rtan).
