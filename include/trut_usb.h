/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef TRUT_USB_H
#define TRUT_USB_H

#define NB_ENDPOINTS          5  //  number of endpoints in the application including control endpoint
#define EP_TEMP_IN            1
#define EP_TEMP_OUT           2
#define EP_DEBUG_IN           4
#define EP_TAPE_OUT           3

                  // USB Device descriptor
#define USB_SPECIFICATION     0x0200
#define DEVICE_CLASS          0      //! each configuration has its own class
#define DEVICE_SUB_CLASS      0      //! each configuration has its own sub-class
#define DEVICE_PROTOCOL       0      //! each configuration has its own protocol
#define EP_CONTROL_LENGTH     64
#define VENDOR_ID             0x03EB // Atmel vendor ID = 03EBh
#define PRODUCT_ID            0x0000
#define RELEASE_NUMBER        0x1000
#define MAN_INDEX             0x01
#define PROD_INDEX            0x02
#define SN_INDEX              0x03
#define NB_CONFIGURATION      1

               // CONFIGURATION
#define NB_INTERFACE       2     //! The number of interface for this configuration
#define CONF_NB            1     //! Number of this configuration
#define CONF_INDEX         0
#define CONF_ATTRIBUTES    USB_CONFIG_SELFPOWERED
#define MAX_POWER          50    // 100 mA

             // USB Interface descriptor gen
#define INTERFACE_NB_TEMP        0        //! The number of this interface
#define ALTERNATE_TEMP           0        //! The alt settting nb of this interface
#define NB_ENDPOINT_TEMP         3        //! The number of endpoints this this interface have
#define INTERFACE_CLASS_TEMP     0x00     //! Class
#define INTERFACE_SUB_CLASS_TEMP 0x00     //! Sub Class
#define INTERFACE_PROTOCOL_TEMP  0x00     //! Protocol
#define INTERFACE_INDEX_TEMP     0

            // USB Endpoint 1 descriptor FS
#define ENDPOINT_NB_TEMP1       (EP_TEMP_IN | 0x80)
#define EP_ATTRIBUTES_TEMP1     0x02        // BULK = 0x02, INTERUPT = 0x03
#define EP_IN_LENGTH_TEMP1      64
#define EP_SIZE_TEMP1           EP_IN_LENGTH_TEMP1
#define EP_INTERVAL_TEMP1       0x00        // Interrupt polling interval from host

            // USB Endpoint 2 descriptor FS
#define ENDPOINT_NB_TEMP2       EP_TEMP_OUT
#define EP_ATTRIBUTES_TEMP2     0x02        // BULK = 0x02, INTERUPT = 0x03
#define EP_IN_LENGTH_TEMP2      64
#define EP_SIZE_TEMP2           EP_IN_LENGTH_TEMP2
#define EP_INTERVAL_TEMP2       0x00        // Interrupt polling interval from host

            // USB Endpoint 4 descriptor. This endpoint is used for loading tape data from the host
#define ENDPOINT_NB_TAPE        EP_TAPE_OUT
#define EP_ATTRIBUTES_TAPE      0x03        // BULK = 0x02, INTERUPT = 0x03
#define EP_IN_LENGTH_TAPE       64
#define EP_SIZE_TAPE            EP_IN_LENGTH_TAPE
#define EP_INTERVAL_TAPE        100        // Interrupt polling interval from host


             // USB Second Interface descriptor gen
#define INTERFACE_NB_SECOND             1        //! The number of this interface
#define ALTERNATE_SECOND                0        //! The alt settting nb of this interface
#define NB_ENDPOINT_SECOND              1        //! The number of endpoints this this interface have
#define INTERFACE_CLASS_SECOND          0x00     //! Class
#define INTERFACE_SUB_CLASS_SECOND      0x55     //! Sub Class
#define INTERFACE_PROTOCOL_SECOND       0xAA     //! Protocol
#define INTERFACE_INDEX_SECOND          0

            // USB Endpoint 3 descriptor. This endpoint is used for debugging
#define ENDPOINT_NB_DBG         (EP_DEBUG_IN | 0x80)
#define EP_ATTRIBUTES_DBG       0x02        // BULK = 0x02, INTERUPT = 0x03
#define EP_IN_LENGTH_DBG        64
#define EP_SIZE_DBG             EP_IN_LENGTH_DBG
#define EP_INTERVAL_DBG         0x00        // Interrupt polling interval from host

#endif
