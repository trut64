/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef TRUTPROTO_H
#define TRUTPROTO_H

// Define the interfaces
#define TRUT_IF_COMM 0
#define TRUT_IF_DATA 1

// Define data interface alternate settings
#define TRUT_ALTIF_DATA_BULKOUT 0
#define TRUT_ALTIF_DATA_BULKIN  1
#define TRUT_ALTIF_DATA_INTOUT  2
#define TRUT_ALTIF_DATA_INTIN   3

// Define the number of endpoints
#define NUM_EPS 4

// Define the endpoints used for communication.
#define EP_CONTROL 0              // Default endpoint
#define TRUT_EP_MSG 1             // Endpoint for messages to host
#define TRUT_EP_CMD 2             // Endpoint for commands from host
#define TRUT_EP_DATA 3            // Endpoint for data transfers

// Define endpoints with directional bit set
#define TRUT_EP_MSG_IN   TRUT_EP_MSG|0x80
#define TRUT_EP_CMD_OUT  TRUT_EP_CMD
#define TRUT_EP_DATA_IN  TRUT_EP_DATA|0x80
#define TRUT_EP_DATA_OUT TRUT_EP_DATA

// Define the max packet size for the endpoints
#define EP_CONTROL_SIZE 16
#define TRUT_EP_MSG_SIZE 16
#define TRUT_EP_CMD_SIZE 16
#define TRUT_EP_DATA_SIZE 64

// Parameters to TRUT_CMD_SETMODE
#define TRUT_MODE_PASS      0x00    // Pass-through connection
#define TRUT_MODE_LOAD      0x80    // Send data to C64
#define TRUT_MODE_WRITETAPE 0xA0    // Send data to TAPE
#define TRUT_MODE_WRITE     0xA0    // Send data to TAPE
#define TRUT_MODE_C64WRITE  0xC0    // Read data from C64
#define TRUT_MODE_SAVE      0xC0    // Read data from C64
#define TRUT_MODE_DUMP      0xE0    // Read data from TAPE

// Parameters to TRUT_CMD_SETMSG
#define TRUT_CMD_SETMSG_DISABLED 0x00
#define TRUT_CMD_SETMSG_ENABLED 0x01

// CASS MOTOR status messages
#define TRUT_MSG_MOTOR_OFF 0x00
#define TRUT_MSG_MOTOR_ON  0x01

// Communication error specifier
#define TRUT_MSG_COMMERROR_BUFFERUF 0x01
#define TRUT_MSG_COMMERROR_BUFFEROF 0x02
#define TRUT_MSG_COMMERROR_READDATA 0x03
#define TRUT_MSG_COMMERROR_WRITEDATA 0x04

// Defines message types from TRUT to HOST, and the way they should be printed.

// Print the following data as a string.
#define TRUT_MSG_STR         0x01

// The following byte specifies the length of the block. Print the block in
// hex format.
#define TRUT_MSG_HEX         0x02

// Pong! (to check if the device is alive)
#define TRUT_MSG_PONG        0x03

// An invalid command was received.
#define TRUT_MSG_INVALIDCMD  0x04

// The received command packet had an invalid format.
#define TRUT_MSG_PROTOERROR  0x05

// CASS MOTOR status reporting
#define TRUT_MSG_MOTOR       0x06

// Stack usage reporting
#define TRUT_MSG_STACKUSAGE  0x07

// Buffer usage reporting
#define TRUT_MSG_BUFFERUSAGE 0x08

// Communication error
#define TRUT_MSG_COMMERROR   0x09

// Sent to acknowledge receipt of TRUT_CMD_DATASTART
#define TRUT_MSG_ACCEPTINGDATA 0x0A

// CRC reporting
#define TRUT_MSG_CRC 0x0B

#define TRUT_MSG_TIMERERROR 0x0C

// Report that dump is started/finished
#define TRUT_MSG_DUMPSTARTED 0x0D
#define TRUT_MSG_DUMPFINISHED 0x0E

// Report that load is started/finished
#define TRUT_MSG_LOADSTARTED 0x0F
#define TRUT_MSG_LOADFINISHED 0x10

// Report profiling (worst-case). The packet has the following structure:
// Byte 0: 0x11
// Byte 1: Measurement index
// Byte 2: Measurement data (msb)
// Byte 3: Measurement data (lsb)
#define TRUT_MSG_PROFILE_WC 0x11

// Profile not enabled.
#define TRUT_MSG_NOPROFILE 0x12

// Defines command types from HOST to TRUT.

// Sent at start of a packet. A following word (msb-first) specifies the
// number of TRUT_EP_DATA_SIZE blocks that the data consists of.
#define TRUT_CMD_DATASTART        0x81

// One following byte specifies TRUT_MODE_{PASS,LOAD,WRITETAPE,C64WRITE,DUMP}
#define TRUT_CMD_SETMODE          0x82

// Ping! (to check if the device is alive)
#define TRUT_CMD_PING             0x83

// One following byte specifies msg enabled (0x01) or msg disabled (0x00)
#define TRUT_CMD_SETMSG           0x84

// Query stack usage
#define TRUT_CMD_QUERYSTACKUSAGE  0x87

// Query buffer usage
#define TRUT_CMD_QUERYBUFFERUSAGE 0x88

// Query CRC
#define TRUT_CMD_QUERYCRC 0x89

// Query profiling (worst-case). One-byte argument with measurement index.
#define TRUT_CMD_PROFILE_WC 0x90

#endif
