#ifndef USBW_H
#define USBW_H

#include <stdint.h>
#include "waitable.h"
#include "common.h"
#include "sys/time.h"

struct usbw_waitables
{
	int timeout_pending;
	struct timeval timeout;
	int num_readobj;
	waitable* readobj;
	int num_writeobj;
	waitable* writeobj;
};

enum usbw_xfertype { Control, Isochronous, Bulk, Interrupt };

typedef struct usbw_handle usbw_handle;
typedef struct usbw_async usbw_async;
typedef struct usbw_waitables usbw_waitables;
typedef enum usbw_xfertype usbw_xfertype;

// All functions returning int return 0 on success and -1 on error.
// All functions returning pointers return 0 on error.

int usbw_init();
void usbw_debug(int level);

usbw_handle* usbw_scanbyvid(int vid, int pid);
int usbw_close(usbw_handle* h);

int usbw_get_configuration(usbw_handle* h, int* c);
int usbw_set_configuration(usbw_handle* h, int c);

int usbw_claim_interface(usbw_handle* h, int iface);
int usbw_release_interface(usbw_handle* h, int iface);
int usbw_set_interface_alt_setting(usbw_handle* h, int iface, int setting);

int usbw_sync(usbw_handle* h, usbw_xfertype type, int ep, uint8_t* buffer,
			  int length, int* xferd);

usbw_async* usbw_asyncinit(usbw_handle* h, usbw_xfertype type, int ep,
						   uint8_t* buffer, int length);
int usbw_asyncfree(usbw_async* a);
int usbw_asynccomplete(usbw_async* a, bool* complete);
int usbw_asyncxferlen(usbw_async* a, int* length);
int usbw_asyncresubmit(usbw_async* a);
int usbw_asynccancel(usbw_async* a);
int usbw_asynccancelled(usbw_async* a, bool* cancelled);

usbw_waitables* usbw_getwaitables(usbw_handle* h);
int usbw_freewaitables(usbw_handle* h, usbw_waitables* w);
int usbw_handleevents(usbw_handle* h);

#endif
