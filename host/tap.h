/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef TAP_H
#define TAP_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <math.h>

#define C64_CLK_MHZ 0.985248
#define AVR_CLK_MHZ 8.000000
#define AVR_PRESCALING 8.0

/* data[x] represents a pulse of length data[x]/985248 seconds. */
struct tape_data
{
	int length;
	int* data;
};

struct buffer
{
	int length;
	uint8_t* data;
};

struct tape_data* load_tap(FILE* f);
struct tape_data* decode_tap(struct buffer* buf);
struct tape_data* load_tap_file(const char* file);

// Store 'data' in TAPv1 to the file 'f'. 'f' is closed before the
// function returns.
int save_tap(FILE* f, const struct tape_data* data);

struct buffer* fredrik_internal(const struct tape_data* indata);
struct buffer* jakob_internal(const struct tape_data* indata);
struct tape_data* from_internal(const struct buffer* avr);

void free_tap(struct tape_data* data);
void free_buffer(struct buffer* buffer);

#endif
