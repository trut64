/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <usb.h>

#include "common.h"
#include "trut_usb.h"

#define USE_AVRUSB 1

#if USE_AVRUSB
#  include "trutproto.h"
// Anton's code
#  define ENDPOINT 1 | 0x80
#  define ENDPOINT_OUT 2
#  define INTERFACE 0
#else
// Atmel's code
#  define ENDPOINT ENDPOINT_NB_DBG
#  define INTERFACE 1
#endif

void print_msg(const char* data, int size);
struct usb_dev_handle* init_trut(int interface);

void dump_data(const char* data, int size)
{
	int i;
	for(i = 0; i < size; i++) {
		if(data[i] == 0)
			continue;

		if(isprint(data[i]) || isspace(data[i]))
			putchar(data[i]);
		else
			printf("\nnon-printable: %d '%c'\n", data[i], data[i]);
	}
	fflush(stdout);
}

int main(int argc, char** argv)
{
	struct usb_dev_handle *usb_handle;

	usb_handle = init_trut(INTERFACE);
	if(!usb_handle) {
		fprintf(stderr, "Failed to initialize TRUT\n");
		return 1;
	}

#if USE_AVRUSB
	char buf[3] = { TRUT_CMD_SETMSG, 1, TRUT_CMD_SETMSG_ENABLED };
	int r = usb_bulk_write(usb_handle, ENDPOINT_OUT, buf, sizeof(buf), 10);
	if(r < 0) {
		printf("Failed to enable messages\n");
		exit(1);
	}
#endif

	printf("Debugging channel initiated!\n");

	while(1) {
		char data[64];
		int ret = usb_bulk_read(usb_handle, ENDPOINT, data, sizeof(data), 10);
		if(ret < 0) {
			//printf("read %d bytes\n", ret);
		} else {
#if USE_AVRUSB
			print_msg(data, ret);
#else
			dump_data(data, ret);
#endif
		}
	}

	if(usb_handle)
		usb_close(usb_handle);

	return 0;
}

#define STATE_MSG 0
#define STATE_STR 1
#define STATE_STRLEN 0x101
#define STATE_HEX 2
#define STATE_HEXLEN 0x102
#define STATE_MOTOR 3
int state = STATE_MSG;
int len = 0;
void print_msg(const char* data, int size)
{
	int i;
	char c;

	for(i = 0; i < size; i++)
	{
		c = data[i];

		switch(state)
		{
		case STATE_MSG:
			switch(c)
			{
			case TRUT_MSG_STR:
				state = STATE_STRLEN;
				break;
			case TRUT_MSG_HEX:
				state = STATE_HEXLEN;
				break;
			case TRUT_MSG_PONG:
				printf("Pong!\n");
				break;
			case TRUT_MSG_INVALIDCMD:
				printf("TRUT says invalid command.\n");
				break;
			case TRUT_MSG_PROTOERROR:
				printf("Protocol error\n");
				break;
			case TRUT_MSG_MOTOR:
				state = STATE_MOTOR;
				break;
			default:
				// Illegal data, just print it for now.
				if(isprint(c) || isspace(c))
					putchar(c);
			}
			break;
		case STATE_STR:
			if(isprint(c))
				putchar(c);
			len--;
			if(len == 0)
			{
				putchar('\n');
				state = STATE_MSG;
			}
			break;
		case STATE_STRLEN:
			len = c;
			state = STATE_STR;
			break;
		case STATE_HEX:
			printf("%02hhX ", c);
			len--;
			if(len == 0)
			{
				putchar('\n');
				state = STATE_MSG;
			}
			break;
		case STATE_HEXLEN:
			len = c;
			state = STATE_HEX;
			break;
		case STATE_MOTOR:
			switch(c)
			{
			case TRUT_MSG_MOTOR_OFF:
				printf("Motor is off\n");
				break;
			case TRUT_MSG_MOTOR_ON:
				printf("Motor is on\n");
				break;
			default:
				printf("Motor in invalid state\n");
				break;
			}
			state = STATE_MSG;
			break;
		default:
			fprintf(stderr, "Data printer in invalid state. This is probably serious.\n");
		}
	}

	fflush(stdout);
}

static struct usb_device *device_init(void)
{
	struct usb_bus *usb_bus;
	struct usb_device *dev;
	usb_init();
	usb_find_busses();
	usb_find_devices();
	for (usb_bus = usb_busses; usb_bus; usb_bus = usb_bus->next)
	{
		for (dev = usb_bus->devices; dev; dev = dev->next)
		{
			if ((dev->descriptor.idVendor  == VENDOR_ID) &&
				(dev->descriptor.idProduct == PRODUCT_ID))
				return dev;
		}
	}

	return NULL;
}

struct usb_dev_handle* init_trut(int interface)
{
	struct usb_device *usb_dev;
	struct usb_dev_handle *usb_handle = NULL;

	usb_dev = device_init();
	if (usb_dev == NULL) {
		fprintf(stderr, "Device not found\n");
		goto err;
	}

	usb_handle = usb_open(usb_dev);
	if (usb_handle == NULL) {
		fprintf(stderr, "Not able to open the USB device. libusb: %s\n",
				usb_strerror());
		goto err;
	}

	usb_set_configuration(usb_handle, 1);

	int ret = usb_claim_interface(usb_handle, interface);
	if (ret < 0) {
		fprintf(stderr, "Failed to claim interface: %s. libusb: %s\n",
				strerror(-ret), usb_strerror());
		goto err;
	}

	return usb_handle;

err:
	if(usb_handle)
		usb_close(usb_handle);

	return NULL;
}
