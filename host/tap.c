/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <assert.h>

#define DEBUG 1

#include "common.h"
#include "tap.h"
#include "pulse.h"

/*

From http://www.computerbrains.com/tapformat.html

C64 RAW TAPE (.TAP) FILE FORMAT.

0000 'C64-TAPE-RAW'
000C UBYTE Version
000D UBYTE[3] For future use...
0010 ULONG data length (Intel format LSB,MSB)
0014 UBYTE[] data
...

Version=0:
Each data byte represent the length of a pulse (the time until the
c64's hardware triggers again).  The length is (8*data) cycles (PAL
C64), i.e. the data byte of 2F represent (47*8/985248) seconds.  The
data byte value of 00 represents overflow, any pulselength of more
than 255*8 cycles.

Version=1:
As above but data value of 00 is now followed by three bytes,
representing a 24 bit value of C64 cyles (NOT times 8). The order is
as follow: 00 <bit0-7> <bit8-15> <bit16-24>.

*/

static const char tap_header[] = "C64-TAPE-RAW";

static void dump_tape(const char* file, const struct tape_data* t)
{
    FILE* f;
    int i;

    f = fopen(file, "w");
    assert(f);

    fprintf(f, "length: %d\n", t->length);
    for(i = 0; i < t->length; i++) {
        fprintf(f, "%d\n", t->data[i]);
    }
    fclose(f);
}

// This fuction can be removed. It's only used to test the new, more flexible
// loead_tap() written in 2023.
struct tape_data* load_tap_original(FILE* f)    // The original load_tap used by the console tools.
{
    // Enough room to hold "C64-TAPE-RAW", 1 byte version, 3 bytes
    // padding, and 4 bytes representing the length
    uint8_t buf[12+1+3+4];

    uint8_t version;
    uint32_t data_length;
    uint8_t* data;
    struct tape_data* ret = NULL;
    long file_size;
    long data_pos;

    if(fread(buf, sizeof(buf), 1, f) < 1) {
        warning("first fread returned early");
        return NULL;
    }

    if(memcmp(buf, tap_header, sizeof(tap_header)-1)) {
        warning("Not a TAP file, TAP header not found");
        return NULL;
    }

    version = buf[0xc];

    if(version != 0 && version != 1) {
        warning("Unrecognized TAP-file version: %d", version);
        return NULL;
    }

    // The length is stored in little-endian byte order.
    data_length = buf[0x10] + (buf[0x10 + 1] << 8) + (buf[0x10 + 2] << 16) +
        (buf[0x10 + 3] << 24);

    printf("Size of data block: %d\n",data_length);

    // Calculate the size of the file
    data_pos = ftell(f);
    if(fseek(f, 0, SEEK_END) != 0) {
        warning("fseek failed: %s", strerror(errno));
        return NULL;
    }
    file_size = ftell(f);

    // Ok, let's move back to the same spot (data_pos is always 20 by the way... hehe)
    if(fseek(f, data_pos, SEEK_SET) != 0) {
        warning("fseek failed: %s", strerror(errno));
        return NULL;
    }

    printf("Data size according to file: %ld\n",file_size-20);

    data = xmalloc(data_length * sizeof(int));

    if(fread(data, data_length, 1, f) < 1) {
        if (feof(f)) {
            warning("Unexpected end of file");
        } else {
            warning("second fread returned early: %s", strerror(errno));
        }
        printf("Current file position: %ld\n", ftell(f));
    }

    {
        long offset = ftell(f);
        if(fseek(f, 0, SEEK_END) != 0) {
            warning("fseek failed: %s", strerror(errno));
            free(data);
            return NULL;
        }

        if(offset != ftell(f)) {
            warning("Ignoring garbage at end of file");
            printf("ftell(f): %ld\n", ftell(f));
            printf("offset: %ld\n",offset);
        }
    }

    debug("Loading version %d TAP-file. Data length: %d", version, data_length);

    ret = xmalloc(sizeof(struct tape_data));

    // This is always enough. It might be too much in TAPv1, but we
    // don't care.
    ret->data = xmalloc(data_length*sizeof(int));

    if(version == 0) {
        int tapv0_overflows=0;
        ret->length = data_length;
        for(int i = 0; i < data_length; i++) {
            if(data[i] == 0) {
                //warning("Overflow detected! offset: %d", i);
                tapv0_overflows++;
                //ret->data[i] = (0xff + 1)*8;
                ret->data[i] = 20000;		// VICE default settings
            } else {
                ret->data[i] = data[i]*8;
            }
        }
        if (tapv0_overflows>0) {
            printf("%d tapv0 overflows detected!\n",tapv0_overflows);
        }
    }
    else /* version == 1 */
    {
        int j = 0;
        for(int i = 0; i < data_length; j++) {
            if(data[i] == 0) {
                if(i+3 >= data_length) {
                    warning("Got 00 in TAPv1, but less than 3 bytes remains! Ignoring this pulse.");
                    break;
                }

                ret->data[j] = data[i+1] + (data[i+2] << 8) + (data[i+3] << 16);
                i += 4;
                // debug("Got 00 offset: %d value: %d", i, ret->data[j]);
            } else {
                ret->data[j] = data[i]*8;
                i++;
            }
        }

        ret->length = j;
    }

    free(data);

    dump_tape("debug-tap-load", ret);
    return ret;
}

#define TAP_V1_HEADER "C64-TAPE-RAW\001\000\000\000"
int save_tap(FILE* f, const struct tape_data* data)
{
    int i;
    uint32_t data_len;

    dump_tape("debug-tap-save", data);

    if(fwrite(TAP_V1_HEADER, sizeof(TAP_V1_HEADER) - 1, 1, f) != 1)
        goto err;

    data_len = 0;
    for(i = 0; i < data->length; i++) {
        if(data->data[i]/8 > 0xff)
            data_len += 4;
        else
            data_len += 1;
    }

    {
        uint8_t buf[4];
        buf[0] = data_len & 0x000000ff;
        buf[1] = (data_len & 0x0000ff00) >> 8;
        buf[2] = (data_len & 0x00ff0000) >> 16;
        buf[3] = (data_len & 0xff000000) >> 24;
        if(fwrite(buf, 4, 1, f) != 1)
            goto err;
    }

    for(i = 0; i < data->length; i++) {
        int pulse = data->data[i];
        uint8_t buf[4];

        if(pulse/8 > 0xff) {
            buf[0] = 0;
            buf[1] = pulse & 0x0000ff;
            buf[2] = (pulse & 0x00ff00) >> 8;
            buf[3] = (pulse & 0xff0000) >> 16;
            if(fwrite(buf, 4, 1, f) != 1)
                goto err;
        } else {
            buf[0] = pulse/8;
            if(fwrite(buf, 1, 1, f) != 1)
                goto err;
        }
    }

    if(fclose(f))
        goto err;

    return 0;

err:
    return 1;
}



struct buffer* jakob_internal(const struct tape_data* indata)
{
    struct buffer* avr = xmalloc(sizeof(struct buffer));
    int* data;
    int overflows = 0;
    int extended_pulses = 0;

    // Convert to internal format
    printf("Converting to Jakob's internal format...\n");

    data = xmalloc(indata->length*sizeof(int));
    overflows=0;
    for (int i = 0; i < indata->length; i++) {

        // Scale the pulse length
        data[i]=floor(0.5+(1.0*indata->data[i]/C64_CLK_MHZ)*(1.0*AVR_CLK_MHZ/AVR_PRESCALING));

        // Eliminate too small pulses
        if (data[i]<100) {
            //printf("Warning: Extending short pulse from %x to %x\n", ret->data[i], 100);
            extended_pulses++;
            data[i]=100;
        }

        // Divide by two (TEMPORARY SOLUTION!)
        data[i] >>= 1;

        // Skew compensation (TEMPORARY)
        //data[i]+=-20;

        // Find out how much the internal representation will expand with respect to the tap data
        // This could probably be implemented in a much nicer way using realloc.
        overflows+=data[i]/0xffff;
        if (data[i]%0xffff==0) {
            printf("jDEBUG: Multiple of 0xffff encountered\n");
            overflows--;	// Multiples of 0xffff must end with 0xffff
        }
    }

    if (extended_pulses>0) {
        printf("Warning: %d pulses were extended!\n",extended_pulses);
    }

    avr = xmalloc(sizeof(struct buffer));
    avr->length = 2*(indata->length+overflows);
    avr->data = xmalloc(avr->length);

    // Since we didn't use realloc, we have to loop again.
    int j=0;
    for (int i = 0; i < indata->length; i++) {
        if (data[i]<=0xffff) {
            // No overflow
            avr->data[j++]=(data[i] & 0xff00)>>8;
            avr->data[j]=(data[i] & 0x00ff);

        } else {

            // Overflow!
            overflows=data[i]/0xffff;
            if (data[i]%0xffff==0) overflows--;	// Multiples of 0xffff must end with 0xffff
            for (int k=0; k<overflows; k++) {
                avr->data[j++]=0x00;
                avr->data[j]=0x00;
                j++;
            }
            if (data[i]%0xffff==0) {
                // The pulse is a multiple of 0xffff, hence it must end with a 0xffff instead of 0x0000.
                avr->data[j++]=0xff;
                avr->data[j]=0xff;
            } else {
                // The pulse is not a multiple of 0xffff
                avr->data[j++]=((data[i]%0xffff) & 0xff00) >> 8;
                avr->data[j]=(data[i]%0xffff) & 0x00ff;
            }
        }
        j++;
    }

    if (j!=avr->length) {
        // A so called sanity check
        printf("Error! The algorithm for calculating buffer size in tap.c is not working.\n");
        printf("DEBUG: avr length (precalculated): %d\n",avr->length);
        printf("DEBUG: j (actual value): %d\n",j);
    }

    free(data);
    return avr;
}


// Note: The input format to this function is not the same as the
// output format to jakob_internal.
struct tape_data* from_internal(const struct buffer* avr)
{
    struct tape_data* ret = xmalloc(sizeof(struct tape_data));
    int j = 0;

    FILE* f = fopen("debug-tap-save.raw", "wb");
    if(fwrite(avr->data, avr->length, 1, f) != 1)
      assert(0);
    fclose(f);

    // This is always enough.
    ret->data = xmalloc(sizeof(int)*avr->length/2);
    ret->length = 0;

    while(j < avr->length) {
        uint16_t word = (avr->data[j] << 8) + avr->data[j+1];
        int pulse;
        j += 2;

        if(word == 0) {
            // The overflow case.
            if(j+6 < avr->length) {

                // First the number of multiples of 0x10000
                uint16_t mult = (avr->data[j] << 8) + avr->data[j+1];
                j += 2;

                // ... and then the remainder.
                uint16_t rem = (avr->data[j] << 8) + avr->data[j+1];
                j += 2;

                pulse = mult*0x10000 + rem;
                printf("overflow mult: %d rem: %d\n", mult, rem);
            } else {
                break;
            }
        } else {
            pulse = word;
        }

        // Scale the pulse length
        ret->data[ret->length] = floor(0.5+(1.0*pulse/(AVR_CLK_MHZ/AVR_PRESCALING))/(1.0*C64_CLK_MHZ));
        ret->length++;
    }

    return ret;
}

struct buffer* fredrik_internal(const struct tape_data* indata)
{
    uint8_t* outdata = xmalloc(5*indata->length);
    struct buffer* ret = xmalloc(sizeof(struct buffer));
    int out = 0;
    int i;

    // Convert to internal format
    printf("Converting to Fredrik's internal format...\n");

    for(i = 0; i < indata->length; i++) {
        int pulse = indata->data[i];
        pulse = lrint(pulse/C64_CLK_MHZ)*(AVR_CLK_MHZ/AVR_PRESCALING);

        pulse = pulse / 2;

        if(pulse <= MINIMUM_PULSE_LENGTH)
            pulse = 0;
        else
            pulse -= MINIMUM_PULSE_LENGTH;

        if(indata->data[i] % 8 == 0 && pulse/8 <= 127) {
            outdata[out++] = pulse/8;
        } else {
            outdata[out++] = ((pulse & 0xff0000) >> 16) | 0x80;
            outdata[out++] = ((pulse & 0x00ff00) >> 8);
            outdata[out++] = (pulse & 0x0000ff);
        }
    }

    ret->length = out;
    ret->data = outdata;
    return ret;
}

void free_tap(struct tape_data* data)
{
    free(data->data);
    free(data);
}

void free_buffer(struct buffer* buffer)
{
    free(buffer->data);
    free(buffer);
}

// *****************************
// *** Jakobs additions 2023 ***
// *****************************

bool compare_tape_data(struct tape_data* t1, struct tape_data* t2)
{
    if (t1->length != t2->length) {
        warning("compare_tape_data: different tape lengths");
        return false;
    }

    for (int i=0; i<t1->length; i++) {
        if (t1->data[i] != t2->data[i]) {
            warning("compare_tape_data: there are differences");
            return false;
        }
    }

    return true;
}

struct tape_data* decode_tap(struct buffer* buf)
{
    // The first 20 bytes of the buffer data contains
    // "C64-TAPE-RAW", 1 byte version, 3 bytes
    // padding, and 4 bytes representing the length
    uint8_t version;
    uint32_t data_length;
    uint8_t* data;
    struct tape_data* ret = NULL;

    if(memcmp(buf->data, tap_header, sizeof(tap_header)-1)) {
        warning("Not a TAP file, TAP header not found");
        return NULL;
    }

    version = buf->data[0xc];

    if(version != 0 && version != 1) {
        warning("Unrecognized TAP-file version: %d", version);
        return NULL;
    }

    // The length is stored in little-endian byte order.
    data_length = buf->data[0x10] + (buf->data[0x10 + 1] << 8) + (buf->data[0x10 + 2] << 16) +
                  (buf->data[0x10 + 3] << 24);

    printf("Size of data block: %d\n",data_length);
    printf("Data size according to file: %d\n",buf->length-20);

    if(data_length < buf->length-20) {
        warning("Ignoring garbage at end of file");
    } else if (data_length > buf->length-20) {
        warning("Unexpected end of file");
        return NULL;
    }

    debug("Loading version %d TAP-file. Data length: %d", version, data_length);

    data=buf->data+20;
    ret = xmalloc(sizeof(struct tape_data));

    // This is always enough. It might be too much in TAPv1, but we
    // don't care.
    ret->data = xmalloc(data_length*sizeof(int));

    if(version == 0) {
        int tapv0_overflows=0;
        ret->length = data_length;
        for(int i = 0; i < data_length; i++) {
            if(data[i] == 0) {
                //warning("Overflow detected! offset: %d", i);
                tapv0_overflows++;
                //ret->data[i] = (0xff + 1)*8;
                ret->data[i] = 20000;		// VICE default settings
            } else {
                ret->data[i] = data[i]*8;
            }
        }
        if (tapv0_overflows>0) {
            printf("%d tapv0 overflows detected!\n",tapv0_overflows);
        }
    }
    else /* version == 1 */
    {
        int j = 0;
        for(int i = 0; i < data_length; j++) {
            if(data[i] == 0) {
                if(i+3 >= data_length) {
                    warning("Got 00 in TAPv1, but less than 3 bytes remains! Ignoring this pulse.");
                    break;
                }

                ret->data[j] = data[i+1] + (data[i+2] << 8) + (data[i+3] << 16);
                i += 4;
                // debug("Got 00 offset: %d value: %d", i, ret->data[j]);
            } else {
                ret->data[j] = data[i]*8;
                i++;
            }
        }

        ret->length = j;
    }

    //dump_tape("debug-tap-load", ret);
    return ret;
}

struct tape_data* load_tap(FILE* f)
{

    long file_size;
    struct tape_data* tap;
    struct buffer* buf;

    // Calculate the size of the file
    if(fseek(f, 0, SEEK_END) != 0) {
        warning("fseek failed: %s", strerror(errno));
        return NULL;
    }
    file_size = ftell(f);
    fseek(f, 0, SEEK_SET);

    buf = xmalloc(sizeof(struct buffer));
    buf->data = xmalloc(file_size);
    if(fread(buf->data, file_size, 1, f) < 1) {
        warning("Error reading file");
        return NULL;
    }

    buf->length=file_size;

    tap=decode_tap(buf);

    if (true) { // Debug block. Remove (together with load_tap_orininal() when the new load_tap() is enough tested)
        struct tape_data* tap_debug;
        fseek(f, 0, SEEK_SET);
        tap_debug=load_tap_original(f);
        if (compare_tape_data(tap, tap_debug)) {
            printf("Debug (tap.c): Tap file verified successfully!\n");
        } else {
            printf("Debug (tap.c): Error! Tap processing routine does not work! Rewrite!\n");
            return NULL;
        }
        free_tap(tap_debug);
    } // End of debug block
    free_buffer(buf);
    return tap;
}

struct tape_data* load_tap_file(const char* file)
{
    struct tape_data* tap;
    FILE* f;
    f = fopen(file, "rb");
    if(!f) {
        printf("Unable to open '%s': %s\n", file, strerror(errno));
        return NULL;
    }
    tap=load_tap(f);
    fclose(f);
    return tap;
}
