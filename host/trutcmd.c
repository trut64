/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <usb.h>
//#include <sys/select.h>
//#include <time.h>
#include <stdint.h>
#include <unistd.h>

#include "trutproto.h"

#define VENDOR_ID             0x03EB // Atmel vendor ID = 03EBh
#define PRODUCT_ID            0x0000

struct usb_device* current_device;
struct usb_dev_handle *usb_handle;

struct usb_device* device_data;
struct usb_dev_handle* handle_data;

int trut_connected = 0;
uint8_t devnum;

const int control_interface = 0;
#define CONTROLEP_IN 1
#define CONTROLEP_OUT 2

int claim_dataif = 0;
const int data_interface = 1;

static struct usb_device* scan_trut(void);
void find_trut();
void release_trut();
void print_msg(const char* data, int size);

static struct usb_device* scan_trut(void)
{
    struct usb_bus *usb_bus;
    struct usb_device *dev;

    for (usb_bus = usb_busses; usb_bus; usb_bus = usb_bus->next)
    {
        for (dev = usb_bus->devices; dev; dev = dev->next)
        {
            if ((dev->descriptor.idVendor  == VENDOR_ID) &&
                (dev->descriptor.idProduct == PRODUCT_ID))
                return dev;
        }
    }

    return NULL;
}

void find_trut()
{
    int changes;
    int ret;
    struct usb_device* dev;
    struct usb_dev_handle* h = NULL;

    usb_find_busses();
    changes = usb_find_devices();

    if(changes == 0)
	return;

    dev = scan_trut();
    if(dev == NULL)
    {
	if(trut_connected)
	{
	    // Release the trut.
	    ret = usb_close(usb_handle);
	    if(ret < 0)
		fprintf(stderr, "usb_close: %s\n", usb_strerror());

	    if(claim_dataif)
	    {
		ret = usb_close(handle_data);
		if(ret < 0)
		    fprintf(stderr, "usb_close: %s\n", usb_strerror());
	    }

	    fprintf(stderr, "TRUT released\n");
	    trut_connected = 0;
	}
	return;
    }

    if(dev->devnum == devnum)
	return;

    if(trut_connected)
    {
	// Release the previous trut.
	ret = usb_close(usb_handle);
	if(ret < 0)
	    fprintf(stderr, "usb_close: %s\n", usb_strerror());

	if(claim_dataif)
	{
	    ret = usb_close(handle_data);
	    if(ret < 0)
		fprintf(stderr, "usb_close: %s\n", usb_strerror());
	}

	fprintf(stderr, "TRUT released\n");
	trut_connected = 0;
    }

    h = usb_open(dev);
    if (h == NULL)
    {
	fprintf(stderr, "Not able to open the USB device. libusb: %s\n", usb_strerror());
	return;
    }

    usb_set_configuration(h, 1);

    ret = usb_claim_interface(h, control_interface);
    if (ret < 0)
    {
        fprintf(stderr, "Failed to claim interface: %s. libusb: %s\n",
		strerror(-ret), usb_strerror());
	//usb_close(h);
        //return;
    }

    fprintf(stderr, "TRUT is connected\n");

    trut_connected = 1;
    usb_handle = h;

    if(claim_dataif)
    {
        handle_data = 0;
	h = usb_open(dev);
	if (h == NULL)
	{
	    fprintf(stderr, "Not able to open device to claim data interface: %s\n", usb_strerror());
	    return;
	}

	usb_set_configuration(h, 1);

	ret = usb_claim_interface(h, data_interface);
	if (ret < 0)
	{
	    fprintf(stderr, "Failed to claim data interface: %s\n", usb_strerror());
	    usb_close(h);
	    return;
	}

        handle_data = h;

	ret = usb_set_altinterface(h, 0);
	if(ret < 0)
	{
	    fprintf(stderr, "Not able to set alternate setting 0 of data interface: %s\n", usb_strerror());
            return;
	}
    }
}

void release_trut()
{
    if(usb_release_interface(usb_handle, control_interface) < 0)
    {
	fprintf(stderr, "Failed to release control interface: %s\n", usb_strerror());
    }

    if(usb_close(usb_handle) < 0)
    {
	fprintf(stderr, "Failed to close the USB device: %s\n", usb_strerror());
    }

    if(claim_dataif)
    {
	if(usb_release_interface(handle_data, data_interface) < 0)
	{
	    fprintf(stderr, "Failed to release data interface: %s\n", usb_strerror());
	}

	if(usb_close(handle_data) < 0)
	{
	    fprintf(stderr, "Failed to close the USB device for data interface: %s\n", usb_strerror());
	}
    }
}

void dump_data(const char* data, int size)
{
    int i;
    for(i = 0; i < size; i++) {
        if(data[i] == 0)
            continue;

        if(isprint(data[i]) || isspace(data[i]))
            putchar(data[i]);
        else
            printf("\nnon-printable: %d '%c'\n", data[i], data[i]);
    }
    fflush(stdout);
}

void dump_hex(const char* data, int size)
{
    int i;
    for(i = 0; i < size; i++)
    {
	printf("%02hhX ", data[i]);
    }
    printf("\n");
    fflush(stdout);
}

#define STATE_MSG 0
#define STATE_STR 1
#define STATE_STRLEN 0x101
#define STATE_HEX 2
#define STATE_HEXLEN 0x102
int state = STATE_MSG;
int len = 0;
void print_msg(const char* data, int size)
{
    int i;
    char c;

    /*
    fprintf(stderr, "print_msg, size=%d, data:\n", size);
    for(i = 0; i < size; i++)
	fprintf(stderr, "%02hhX ", data[i]);
    fprintf(stderr, "\n");
    */

    for(i = 0; i < size; i++)
    {
	c = data[i];

	switch(state)
	{
	case STATE_MSG:
	    switch(c)
	    {
	    case TRUT_MSG_STR:
		state = STATE_STRLEN;
		break;
	    case TRUT_MSG_HEX:
		state = STATE_HEXLEN;
		break;
	    default:
		// Illegal data, just print it for now.
		if(isprint(c) || isspace(c))
		    putchar(c);
	    }
            break;
	case STATE_STR:
            if(isprint(c))
		putchar(c);
	    len--;
	    if(len == 0)
	    {
                putchar('\n');
		state = STATE_MSG;
	    }
	    break;
	case STATE_STRLEN:
	    len = c;
	    state = STATE_STR;
	    break;
	case STATE_HEX:
	    printf("%02hhX ", c);
	    len--;
	    if(len == 0)
	    {
		putchar('\n');
		state = STATE_MSG;
	    }
	    break;
	case STATE_HEXLEN:
	    len = c;
	    state = STATE_HEX;
	    break;
	default:
            fprintf(stderr, "Data printer in invalid state. This is probably serious.\n");
	}
    }

    fflush(stdout);
}

typedef struct
{
    unsigned char requestType;
    unsigned char request;
    unsigned short value;
    unsigned short index;
    unsigned short length;
} setup_request;

const char* requestcodes[] =
{
    "GETSTATUS,        ",
    "CLEARFEATURE,     ",
    "RESERVED2,        ",
    "SETFEATURE,       ",
    "RESERVED4,        ",
    "SETADDRESS,       ",
    "GETDESCRIPTOR,    ",
    "SETDESCRIPTOR,    ",
    "GETCONFIGURATION, ",
    "SETCONFIGURATION, ",
    "GETINTERFACE,     ",
    "SETINTERFACE,     ",
    "SYNCHFRAME,       "
};

const char* typedir[] = { "OUT", " IN" };
const char* typetype[] = { "STD", "CLS", "VND", "RES" };
const char* typerec[] = { "Dev", "Int", "End", "Oth" };

void print_setup_request(const char* data)
{
    setup_request* r = (setup_request*) data;

    printf("Request: ");
    if(r->request > 12)
	printf("INVALID");
    else
	printf("%s", requestcodes[r->request]);

    printf("%s %s ", typedir[r->requestType >> 7], typetype[(r->requestType >> 5) & 0x03]);
    if((r->requestType & 0x1f) > 3)
	printf("RESRV");
    else
	printf("%s", typerec[r->requestType & 0x1f]);

    printf(", ");
    printf("val: %04hX, ", r->value);
    printf("ind: %04hX, ", r->index);
    printf("len: %04hX, ", r->length);
    printf("\n");
}

int main(int argc, char** argv)
{
    int i;
    int ret;
    int count;
    char data[64];
    char strdesc[255];
    char* cmd = argv[1];
    unsigned short len = sizeof(strdesc);

    usb_init();
    find_trut();

    if(!trut_connected)
    {
	fprintf(stderr, "Found no TRUT. :(\n");
	exit(1);
    }

    if(argc > 1)
    {
	if(argc > 2)
	    len = atoi(argv[2]);

	if(strlen(cmd) >= 4 && strncmp(cmd, "help", 4) == 0)
	    fprintf(stderr, "NAH\n");
	else if(strlen(cmd) >= 4 && strncmp(cmd, "str", 3) == 0)
	{
	    if(cmd[3] < 0x30 || cmd[3] > 0x39)
	    {
		fprintf(stderr, "NAH\n");
	    }
	    else
	    {
		ret = usb_get_string(usb_handle, cmd[3]-0x30, 0x809, strdesc, len);
		if(ret < 0)
		{
		    perror("usb_get_string");
		}
		else
		{
		    dump_hex(strdesc, ret);
		    if(cmd[3]-0x30 == 9)
		    {
			for(i = 0; i < (ret-2)/8; i++)
			    print_setup_request(strdesc+2+8*i);
		    }
		}
	    }
	}
	else if(strlen(cmd) >= 4 && strncmp(cmd, "dsc", 3) == 0)
	{
	    if(cmd[3] < 0x30 || cmd[3] > 0x39)
	    {
		fprintf(stderr, "NAH\n");
	    }
	    else
	    {
		ret = usb_get_descriptor(usb_handle, cmd[3]-0x30, 0x0, strdesc, len);
		if(ret < 0)
		{
		    perror("usb_get_string");
		}
		else
		{
		    dump_hex(strdesc, ret);
		}
	    }
	}
	else
	{
	    fprintf(stderr, "Invalid command\n");
	}
    }
    else
    {
	printf("Enter some funny text: ");
        fflush(stdout);

	fgets(data, sizeof(data), stdin);

	ret = usb_bulk_write(usb_handle, CONTROLEP_OUT, data, strlen(data), 1000);
        //usleep(1000000);
	ret = usb_bulk_write(usb_handle, CONTROLEP_OUT, data, strlen(data), 1000);
	if(ret < 0)
	{
	    fprintf(stderr, "usb write error: %s, %s\n", usb_strerror(), strerror(errno));
	}
	else if(ret < strlen(data))
	{
	    fprintf(stderr, "wrote %d bytes of %d\n", ret, count);
	}

	printf("TRUT answer: "); fflush(stdout);
	for(;;)
	{
	    //count = usb_bulk_read(usb_handle, CONTROLEP_IN, data, sizeof(data), 2000);
	    count = usb_bulk_read(usb_handle, CONTROLEP_IN, data, 64, 2000);
	    if(count < 0)
	    {
		fprintf(stderr, "usb read error: %s, %s\n", usb_strerror(), strerror(errno));
	    }
	    else
	    {
		//print_msg(data, count);
		dump_data(data, count);
                printf("\n");
	    }
            break;
	}
    }

    //find_trut();
    release_trut();

    fprintf(stderr, "Good bye\n");

    return 0;
}
