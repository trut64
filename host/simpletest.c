/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

/*
	Just loads a TAP file and displays the header.
 */
#include <stdio.h>
#include <string.h>

#include "tap.h"

void dump_data(const char* data, int size)
{
	int i;
	for(i = 0; i < size; i++) {
		if(data[i])
			printf("%d '%c'\n", data[i], data[i]);
	}
}

int main(int argc, char **argv)
{

	if (argc == 1) {
		fprintf(stderr, "Please specify a filename!\n");
			return 0;
	}

	const char* file = argv[1];
	FILE* f = fopen(file, "rb");
	if(!f) {
		fprintf(stderr, "Unable to open '%s': %s\n", file, strerror(errno));
		return 1;
	}

	printf("Loading %s\n",file);

	struct tape_data* t = load_tap(f);

	printf("TAP-file loaded successfully. Number of pulses: %d\n", t->length);

	return 0;
}
