/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <usb.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>

#ifdef __unix__
#  include <sys/select.h>
#endif

#ifdef WIN32
#  include <windows.h>

static void init_win32(void);
#endif

#include "common.h"
#include "trutproto.h"
#include "tap.h"

#define VENDOR_ID             0x03EB // Atmel vendor ID = 03EBh
#define PRODUCT_ID            0x0000

struct usb_device* current_device;
struct usb_dev_handle *usb_handle;

struct usb_device* device_data;
struct usb_dev_handle* handle_data;

const int comm_interface = TRUT_IF_COMM;
#define EP_COMMIN  TRUT_EP_MSG | 0x80
#define EP_COMMOUT TRUT_EP_CMD
#define EP_DATAOUT TRUT_EP_DATA

#define DATAEP_OUT 3
#define DATAEP_IN 0x83

#define ALT_IF_LOAD 2
#define ALT_IF_DUMP 3

int claim_dataif = 1;
const int data_interface = TRUT_IF_DATA;

#define NUM_ASYNCS 10

// This array of pointers is used during load AND dump.
static void* data_async_context[NUM_ASYNCS];

// Number of bytes we have submitted as write requests to the USB
// subsystem. -1 if there is no write (load) data transfer going on.
static int data_tx_submit = -1;
static struct buffer* data_buffer;

// Number of bytes we have submitted as read requests to the USB
// subsystem. -1 if there is no read (dump) data transfer going on.
static int data_rx_submit = -1;

// Number of bytes we have received.
static int data_received;
static struct buffer* data_rx_buffer;
static FILE* data_rx_file;

int trut_connected = 0;
int quit;

static struct usb_device* scan_trut(void);
int find_trut();
void release_trut();
void print_msg(const char* data, int size);
void printprompt();

void usb_write(const char* data, int len);

int cmd_intarg(const char* cmdargs, int* iarg);
static void data_submit();
static void data_read();
static void data_read_stop();
static void wait_for_stuff();

static struct usb_device* scan_trut(void)
{
	struct usb_bus *usb_bus;
	struct usb_device *dev;

	for (usb_bus = usb_busses; usb_bus; usb_bus = usb_bus->next)
	{
		for (dev = usb_bus->devices; dev; dev = dev->next)
		{
			if ((dev->descriptor.idVendor  == VENDOR_ID) &&
				(dev->descriptor.idProduct == PRODUCT_ID))
				return dev;
		}
	}

	return NULL;
}

int find_trut()
{
	int changes;
	int ret;
	struct usb_device* dev;
	struct usb_dev_handle* h = NULL;

	usb_find_busses();
	changes = usb_find_devices();

	if(changes == 0)
		return 0;

	dev = scan_trut();
	if(dev == NULL)
	{
		if(trut_connected)
		{
			// Release the trut.
			ret = usb_close(usb_handle);
			if(ret < 0)
				fprintf(stderr, "usb_close: %s\n", usb_strerror());

			if(claim_dataif)
			{
				ret = usb_close(handle_data);
				if(ret < 0)
					fprintf(stderr, "usb_close: %s\n", usb_strerror());
			}

			trut_connected = 0;
			return 1;
		}
		else
			return 0;
	}

	if(trut_connected)
	{
		// Release the previous trut.
		ret = usb_close(usb_handle);
		if(ret < 0)
			fprintf(stderr, "usb_close: %s\n", usb_strerror());

		if(claim_dataif)
		{
			ret = usb_close(handle_data);
			if(ret < 0)
				fprintf(stderr, "usb_close: %s\n", usb_strerror());
		}

		trut_connected = 0;
	}

	h = usb_open(dev);
	if (h == NULL)
	{
		fprintf(stderr, "Not able to open the USB device. libusb: %s\n", usb_strerror());
		return 1;
	}

	usb_set_configuration(h, 1);

	ret = usb_claim_interface(h, comm_interface);
	if (ret < 0)
	{
		fprintf(stderr, "Failed to claim interface: %s. libusb: %s\n",
				strerror(-ret), usb_strerror());
		usb_close(h);
		return 1;
	}

	trut_connected = 1;
	usb_handle = h;

	if(claim_dataif)
	{
		handle_data = 0;
		h = usb_open(dev);
		if (h == NULL)
		{
			fprintf(stderr, "Not able to open device to claim data interface: %s\n", usb_strerror());
			return 1;
		}

		usb_set_configuration(h, 1);

		ret = usb_claim_interface(h, data_interface);
		if (ret < 0)
		{
			fprintf(stderr, "Failed to claim data interface: %s\n", usb_strerror());
			usb_close(h);
			return 1;
		}

		handle_data = h;

		ret = usb_set_altinterface(h, ALT_IF_LOAD);
		if(ret < 0)
		{
			fprintf(stderr, "Not able to set alternate setting %d "
				"of data interface: %s\n", ALT_IF_LOAD,
				usb_strerror());
			return 1;
		}
	}
	return 1;
}

void release_trut()
{
	if(usb_release_interface(usb_handle, comm_interface) < 0)
	{
		fprintf(stderr, "Failed to release control interface: %s\n", usb_strerror());
	}

	if(usb_close(usb_handle) < 0)
	{
		fprintf(stderr, "Failed to close the USB device: %s\n", usb_strerror());
	}

	if(claim_dataif)
	{
		if(usb_release_interface(handle_data, data_interface) < 0)
		{
			fprintf(stderr, "Failed to release data interface: %s\n", usb_strerror());
		}

		if(usb_close(handle_data) < 0)
		{
			fprintf(stderr, "Failed to close the USB device for data interface: %s\n", usb_strerror());
		}
	}
}

void dump_hex(const char* data, int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		printf("%02hhX ", data[i]);
	}
	fflush(stdout);
}

// Copied from the avr-libc documentation
static uint16_t crc16_update(uint16_t crc, uint8_t a)
{
        int i;

	crc ^= a;
        for (i = 0; i < 8; ++i)
        {
            if (crc & 1)
                crc = (crc >> 1) ^ 0xA001;
            else
                crc = (crc >> 1);
        }

        return crc;
}

static uint16_t compute_crc(uint8_t* buf, int len)
{
	uint16_t crc = 0xffff;
	int i;

	for(i = 0; i < len; i++) {
		crc = crc16_update(crc, buf[i]);
	}

	return crc;
}

// True (1) if the user submitted the last CRC query (with the 'crc' command)
// False (0) if the last CRC query was automatically submitted
static bool crc_query_from_user;

static void send_query_crc(bool userquery)
{
	uint8_t buf[2];
	buf[0] = TRUT_CMD_QUERYCRC;
	buf[1] = 0;
	usb_write(buf, 2);
	if(!crc_query_from_user)
		crc_query_from_user = userquery;
}

static void send_start(int len)
{
	char buffer[4];
	buffer[0] = TRUT_CMD_DATASTART;
	buffer[1] = 2;
	buffer[2] = len >> 8;
	buffer[3] = len & 0xff;
	usb_write(buffer, 4);
}

static void send_set_mode(uint8_t mode)
{
	uint8_t buffer[3];

	buffer[0] = TRUT_CMD_SETMODE;
	buffer[1] = 1;
	buffer[2] = mode;
	usb_write(buffer, 3);
}

void msg_error(const char* data, int size)
{
    int i;
	printf("Invalid message received. Printing verbatim:\n");
	for(i = 0; i < size; i++)
		printf("%02hhX ", data[i]);
	putchar('\n');
}

void print_msg(const char* data, int size)
{
	int arglen = size - 1;
	const char* args = data + 1;
	int i = 0;
	bool print_prompt = true;

	switch(data[0])
	{
	case TRUT_MSG_STR:
		for(i = 1; i < arglen; i++)
			putchar(args[i]);

		if(arglen == 0)
			fputs("Got zero length TRUT_MSG_STR", stdout);
		putchar('\n');
		break;
	case TRUT_MSG_HEX:
		for(i = 0; i < arglen; i++)
			printf("%02hhX ", args[i]);

		if(arglen == 0)
			fputs("Got zero length TRUT_MSG_HEX", stdout);
		putchar('\n');
		break;
	case TRUT_MSG_PONG:
		printf("Pong!\n");
		break;
	case TRUT_MSG_INVALIDCMD:
		printf("TRUT says invalid command.\n");
		break;
	case TRUT_MSG_PROTOERROR:
		printf("Protocol error\n");
		break;
	case TRUT_MSG_MOTOR:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		switch(args[0])
		{
		case TRUT_MSG_MOTOR_OFF:
			printf("Motor is off\n");
			break;
		case TRUT_MSG_MOTOR_ON:
			printf("Motor is on\n");
			break;
		default:
			printf("Motor in invalid state\n");
			break;
		}
		break;
	case TRUT_MSG_STACKUSAGE:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		printf("Maximum stack usage: %d bytes\n", args[0]);
		break;
	case TRUT_MSG_BUFFERUSAGE:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		printf("Minimum/maximum buffer usage: %d bytes\n", args[0]);
		break;
	case TRUT_MSG_COMMERROR:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		switch(args[0])
		{
		case TRUT_MSG_COMMERROR_BUFFERUF:
			printf("TRUT: Buffer underflow!\n");
			break;
		case TRUT_MSG_COMMERROR_BUFFEROF:
			printf("TRUT: Buffer overflow!\n");
			break;
		case TRUT_MSG_COMMERROR_READDATA:
			printf("TRUT: Error reading received usb data to buffer\n");
			break;
		default:
			printf("TRUT: Unknown communication error.\n");
		}
		break;
	case TRUT_MSG_ACCEPTINGDATA:
		printf("TRUT is now accepting data.\n");
		break;
	case TRUT_MSG_CRC:
	{
		if(arglen != 6)
		{
			msg_error(data, size);
			break;
		}
		uint8_t args0 = args[0];
		uint8_t args1 = args[1];
		uint8_t args2 = args[2];
		uint8_t args3 = args[3];
		uint32_t crc_len = (args0 << 24) |  (args1 << 16) |  (args2 << 8) | args3;
		crc_len *= 2; // the received length is in words

		args0 = args[4];
		args1 = args[5];
		uint16_t received_crc = (args0 << 8) | args1;

		if(!data_buffer) {
			printf("Got CRC %x. Cannot compute expected CRC as no "
			       "TAP-file is loaded.\n", received_crc);
			break;
		}

		uint16_t computed_crc = compute_crc(data_buffer->data, crc_len);
		if(computed_crc != received_crc) {
			printf("CRC FAILURE: Got CRC %x expected %x (length in bytes: %d)\n",
			       received_crc, computed_crc, crc_len);
		} else if(crc_query_from_user) {
			printf("Got expected CRC %x (length in bytes: %d)\n",
			       computed_crc, crc_len);
			crc_query_from_user = false;
		} else {
			print_prompt = false;
		}
	}
	break;
	case TRUT_MSG_TIMERERROR:
		printf("TRUT: Timer error!\n");
		break;
	case TRUT_MSG_DUMPSTARTED:
		printf("TRUT: Dump started.\n");
		break;
	case TRUT_MSG_DUMPFINISHED:
		printf("TRUT: Dump finished.\n");
		break;
	case TRUT_MSG_LOADSTARTED:
		printf("TRUT: Load started.\n");
		break;
	case TRUT_MSG_LOADFINISHED:
		printf("TRUT: Load finished.\n");
		break;
	default:
		printf("Invalid message received. Printing verbatim:\n");
		for(i = 0; i < size; i++)
			printf("%02hhX ", data[i]);
		putchar('\n');
	}

	if(print_prompt) {
		putchar('\n');
		printprompt();
	}
}

typedef struct
{
	unsigned char requestType;
	unsigned char request;
	unsigned short value;
	unsigned short index;
	unsigned short length;
} setup_request;

const char* requestcodes[] =
{
	"GETSTATUS,        ",
	"CLEARFEATURE,     ",
	"RESERVED2,        ",
	"SETFEATURE,       ",
	"RESERVED4,        ",
	"SETADDRESS,       ",
	"GETDESCRIPTOR,    ",
	"SETDESCRIPTOR,    ",
	"GETCONFIGURATION, ",
	"SETCONFIGURATION, ",
	"GETINTERFACE,     ",
	"SETINTERFACE,     ",
	"SYNCHFRAME,       "
};

const char* typedir[] = { "OUT", " IN" };
const char* typetype[] = { "STD", "CLS", "VND", "RES" };
const char* typerec[] = { "Dev", "Int", "End", "Oth" };

void print_setup_request(const char* data)
{
	setup_request* r = (setup_request*) data;

	printf("Request: ");
	if(r->request > 12)
		printf("INVALID");
	else
		printf("%s", requestcodes[r->request]);

	printf("%s %s ", typedir[r->requestType >> 7], typetype[(r->requestType >> 5) & 0x03]);
	if((r->requestType & 0x1f) > 3)
		printf("RESRV");
	else
		printf("%s", typerec[r->requestType & 0x1f]);

	printf(", ");
	printf("val: %04hX, ", r->value);
	printf("ind: %04hX, ", r->index);
	printf("len: %04hX, ", r->length);
	printf("\n");
}

#define CMD_UNKNOWN 0
#define CMD_QUIT 1
#define CMD_HELP 2
#define CMD_STRDESC 3
#define CMD_DESC 4
#define CMD_DATAMODE 5
#define CMD_PING 6
#define CMD_BUFFER 8
#define CMD_STACK 9
#define CMD_MODE 10
#define CMD_START 11
#define CMD_LOAD 12
#define CMD_CRC 13
#define CMD_DUMP 14
#define CMD_STOP_DUMP 15
#define CMD_CWR 16

void parse_cmd(const char* cmd, int* cmdtype, char* cmdargs)
{
	char cmdstr[16];
	int cmdlen;
	const char* a;

	while(isspace(*cmd))
		cmd++;

	a = cmd;
	while(*a) {
		if(isspace(*a))
			break;
		else
			a++;
	}
	cmdlen = a-cmd;
	if(cmdlen < sizeof(cmdstr))
	{
		strncpy(cmdstr, cmd, cmdlen);
		cmdstr[cmdlen] = 0;

		while(isspace(*a))
			a++;

		// Get rid of trailing whitespace
		cmdlen = strlen(a);
		while(isspace(a[cmdlen-1]) && cmdlen > 0)
			cmdlen--;
		strncpy(cmdargs, a, cmdlen);
		cmdargs[cmdlen] = 0;
	}
	else
	{
		*cmdtype = CMD_UNKNOWN;
		cmdargs[0] = 0;
		return;
	}

	if(strcmp(cmdstr, "quit") == 0)
		*cmdtype = CMD_QUIT;
	else if(strcmp(cmdstr, "help") == 0)
		*cmdtype = CMD_HELP;
	else if(strcmp(cmdstr, "str") == 0)
		*cmdtype = CMD_STRDESC;
	else if(strcmp(cmdstr, "desc") == 0)
		*cmdtype = CMD_DESC;
	else if(strcmp(cmdstr, "data") == 0)
		*cmdtype = CMD_DATAMODE;
	else if(strcmp(cmdstr, "ping") == 0)
		*cmdtype = CMD_PING;
	else if(strcmp(cmdstr, "buffer") == 0)
		*cmdtype = CMD_BUFFER;
	else if(strcmp(cmdstr, "stack") == 0)
		*cmdtype = CMD_STACK;
	else if(strcmp(cmdstr, "mode") == 0)
		*cmdtype = CMD_MODE;
	else if(strcmp(cmdstr, "start") == 0)
		*cmdtype = CMD_START;
	else if(strcmp(cmdstr, "load") == 0)
		*cmdtype = CMD_LOAD;
	else if(strcmp(cmdstr, "crc") == 0)
		*cmdtype = CMD_CRC;
	else if(strcmp(cmdstr, "dump") == 0)
		*cmdtype = CMD_DUMP;
	else if(strcmp(cmdstr, "stopdump") == 0)
		*cmdtype = CMD_STOP_DUMP;
	else if(strcmp(cmdstr, "cwr") == 0)
		*cmdtype = CMD_CWR;
	else
		*cmdtype = CMD_UNKNOWN;
}

void print_unicode(char* str, int len)
{
	while(len--)
	{
		printf("%c", *str);
		str += 2;
	}
}

void print_strdesc(char* str, int len)
{
	if(len >= 2)
		print_unicode(str+2, len-2);
}

void process_cmd(const char* cmd)
{
	char strdesc[256];
	char buffer[256];
	char cmdargs[256];
	int cmdtype;
	int iarg;
	int ret;

	parse_cmd(cmd, &cmdtype, cmdargs);

	switch(cmdtype)
	{
	case CMD_QUIT:
		buffer[0] = TRUT_CMD_SETMSG;
		buffer[1] = 1;
		buffer[2] = TRUT_CMD_SETMSG_DISABLED;

		usb_write(buffer, 3);

		quit = 1;
		break;
	case CMD_HELP:
		printf("Available commands:\n");
		printf("quit      : Quit the program.\n");
		printf("help      : This help text.\n");
		printf("str n     : Request string descriptor n.\n");
		printf("desc n    : Request descriptor n.\n");
		printf("data mode : Use the specified mode for data transfers. mode can be:\n");
		printf("            bulk (bulk), int (interrupt) or iso (isochronous).\n");
		printf("ping      : Check if trut is alive.\n");
		printf("buffer    : Check minimum/maximum buffer usage.\n");
		printf("stack     : Check maximum stack usage.\n");
		printf("mode m    : Set operation mode. m can be: pass, load, wtap, cwr, dump\n");
		printf("            The two dubious modes are \"writetape\" and \"C64write\".\n");
		printf("            Feel free to suggest more intuitive names.\n");
		printf("start len : Tell trut the length in 64-byte blocks of the data to be sent.\n");
		printf("            For now, data is always accepted anyway, but no buffer\n");
		printf("            messages will be sent unless a start message is sent to TRUT. Use\n");
		printf("            for example \"start 1000\" before running loadtap to get buffer\n");
		printf("            reporting for the first 32000 pulses. You should use start\n");
		printf("            between every file sent for trut to reset internal data. The\n");
		printf("            load command will automatically send an appropriate start message.\n");
		printf("load file : Load a TAP-file.\n");
		printf("crc       : Make an explicit CRC query.\n");
		printf("cwr file  : Dump to a TAP-file (from C64).\n");
		printf("dump file : Dump to a TAP-file (from tape).\n");
		printf("stopdump  : Stop an ongoing dump.\n");
		break;
	default:
		if(trut_connected == 0)
		{
			printf("TRUT is not connected.\n");
			break;
		}
		switch(cmdtype)
		{
		case CMD_STRDESC:
            if(cmd_intarg(cmdargs, &iarg))
			{
				printf("Invalid command argument\n");
				break;
			}

			ret = usb_get_string(usb_handle, iarg, 0x809, strdesc, sizeof(strdesc));
			if(ret < 0)
			{
				perror("usb_get_string");
			}
			else
			{
				printf("String descriptor %d: \"", iarg);
				print_strdesc(strdesc, ret);
				printf("\"\n");
			}
			break;
		case CMD_DESC:
            if(cmd_intarg(cmdargs, &iarg))
			{
				printf("Invalid command argument\n");
				break;
			}

			ret = usb_get_descriptor(usb_handle, iarg, 0x0, strdesc, sizeof(strdesc));
			if(ret < 0)
			{
				perror("usb_get_descriptor");
			}
			else
			{
				printf("Descriptor %d: ", iarg);
				dump_hex(strdesc, ret);
				printf("\n");
			}
			break;
		case CMD_DATAMODE:
			printf("Not implemented\n");
			break;
		case CMD_PING:
			buffer[0] = TRUT_CMD_PING;
			buffer[1] = 0;
			usb_write(buffer, 2);
			break;
		case CMD_BUFFER:
			buffer[0] = TRUT_CMD_QUERYBUFFERUSAGE;
			buffer[1] = 0;
			usb_write(buffer, 2);
			break;
		case CMD_STACK:
			buffer[0] = TRUT_CMD_QUERYSTACKUSAGE;
			buffer[1] = 0;
			usb_write(buffer, 2);
			break;
		case CMD_MODE:
		{
			uint8_t mode;
			if(strcmp(cmdargs, "pass") == 0)
				mode = TRUT_MODE_PASS;
			else if(strcmp(cmdargs, "load") == 0)
				mode = TRUT_MODE_LOAD;
			else if(strcmp(cmdargs, "wtap") == 0)
				mode = TRUT_MODE_WRITETAPE;
			else if(strcmp(cmdargs, "cwr") == 0)
				mode = TRUT_MODE_C64WRITE;
			else if(strcmp(cmdargs, "dump") == 0)
				mode = TRUT_MODE_DUMP;
			else
			{
				printf("Invalid mode. Try pass, load, wtap, cwr or dump.\n");
				break;
			}

			send_set_mode(mode);
			break;
		}
		case CMD_START:
			if(cmd_intarg(cmdargs, &iarg))
			{
				printf("Invalid start argument.\n");
				break;
			}

			send_start(iarg);
			break;
		case CMD_LOAD:
		{
			if(data_rx_submit != -1) {
				printf("Already dumping. Cannot dump and load at the same time.\n");
				break;
	       		}

			FILE* f = fopen(cmdargs, "rb");
			if(!f) {
				printf("Unable to open '%s': %s\n", cmdargs, strerror(errno));
				break;
			}

			printf("Loading '%s'\n", cmdargs);
			struct tape_data* tap = load_tap(f);
			fclose(f);

			data_buffer = jakob_internal(tap);
			printf("Buffer length: %d\n", data_buffer->length);

			int ret = usb_set_altinterface(handle_data, ALT_IF_LOAD);
			if(ret < 0)
			{
				fprintf(stderr, "Not able to set alternate "
					"setting %d of data interface: %s\n",
					ALT_IF_LOAD, usb_strerror());
				break;
			}

			send_set_mode(TRUT_MODE_LOAD);
			if(data_buffer->length % 64 == 0)
				send_start(data_buffer->length/64);
			else
				send_start(data_buffer->length/64 + 1);

			int i;
			for(i = 0; i < NUM_ASYNCS; i++)
				data_async_context[i] = NULL;
			data_tx_submit = 0;
			data_submit();
		}
		break;
		case CMD_CWR:
		case CMD_DUMP:
		{
			if(data_tx_submit != -1) {
				printf("Already loading. Cannot dump and load at the same time.\n");
				break;
	       		}

			int ret = usb_set_altinterface(handle_data, ALT_IF_DUMP);
			if(ret < 0)
			{
				fprintf(stderr, "Not able to set alternate "
					"setting %d of data interface: %s\n",
					ALT_IF_DUMP, usb_strerror());
				break;
			}

			data_rx_file = fopen(cmdargs, "wb");
			if(!data_rx_file) {
				printf("Unable to open '%s': %s\n", cmdargs, strerror(errno));
				break;
			}

			printf("Dumping to '%s'\n", cmdargs);

			if(cmdtype == CMD_CWR)
				send_set_mode(TRUT_MODE_C64WRITE);
			else
				send_set_mode(TRUT_MODE_DUMP);
			int i;
			for(i = 0; i < NUM_ASYNCS; i++)
				data_async_context[i] = NULL;
			data_rx_submit = 0;
			data_received = 0;
			printf("calling data_read...\n");
			data_read();
			break;
		}
		case CMD_STOP_DUMP:
		{
			if(data_rx_submit == -1) {
				printf("No dump in progress.\n");
				break;
			}

			data_read_stop();
			break;
		}
		case CMD_CRC:
			send_query_crc(true);
			break;
		default:
			printf("Invalid command\n");
		}
	}
}

int cmd_intarg(const char* cmdargs, int* iarg)
{
	char* arge;

	*iarg = strtol(cmdargs, &arge, 10);
	if(strlen(cmdargs) == 0 || cmdargs+strlen(cmdargs) > arge)
		return 1;

	return 0;
}

void printprompt()
{
	printf("> ");
	fflush(stdout);
}

int main(int argc, char** argv)
{
	int ret;
	int count;
	time_t lastscan = 0;
	time_t now;
	char data[256];

	data_rx_buffer = xmalloc(sizeof(struct buffer));
	data_rx_buffer->length = 30*1024*1024;
	data_rx_buffer->data = xmalloc(data_rx_buffer->length);

#ifdef WIN32
	init_win32();
#endif

	printf("TRUT console. Use \"help\" for help, \"quit\" to quit.\n");

	usb_init();
	find_trut();

	if(trut_connected == 1)
	{
		printf("TRUT connected\n");
	}
	else
	{
		printf("Found no TRUT :(. Waiting for connection.\n");
	}

	if(trut_connected)
	{
		data[0] = TRUT_CMD_SETMSG;
		data[1] = 1;
		data[2] = TRUT_CMD_SETMSG_ENABLED;

		usb_write(data, 3);
	}

	quit = 0;

	printprompt();

	while(!quit) {
		time(&now);

		// For some reason the connect/reconnect code makes
		// things go wrong in windows. Disable it for now.
#ifdef WIN32
		if(0)
#else
		if(now > lastscan)
#endif
		{
			ret = trut_connected;

			if(find_trut())
			{
				if(ret == 0 && trut_connected == 1)
				{
					printf("TRUT connected\n");
					printprompt();
				}
				else if(ret == 1 && trut_connected == 0)
				{
					printf("TRUT disconnected\n");
					printprompt();
				}
				else if(ret == 1 && trut_connected == 1)
				{
					printf("TRUT reconnected\n");
					printprompt();
				}

				if(trut_connected)
				{
					data[0] = TRUT_CMD_SETMSG;
					data[1] = 1;
					data[2] = TRUT_CMD_SETMSG_ENABLED;

					usb_write(data, 3);
				}
			}
		}

		wait_for_stuff();

		if(trut_connected)
		{
			count = usb_bulk_read(usb_handle, EP_COMMIN, data, sizeof(data), 10);
			if(count > 0)
				print_msg(data, count);
		}
	}

	if(trut_connected)
		release_trut();

	fprintf(stderr, "Good bye\n");

	return 0;
}

#ifdef __unix__
static void wait_for_stuff()
{
	struct timeval timeout;
	fd_set readfd;
	fd_set writefd;
	int ret, i;

	FD_ZERO(&readfd);
	FD_SET(0, &readfd);
	FD_ZERO(&writefd);
	int max_fd = 0;

	for(i = 0; i < NUM_ASYNCS; i++) {
		if(data_async_context[i]) {
			// All async contexts share the same fd
			max_fd = usb_async_get_fd(data_async_context[i]);
			FD_SET(max_fd, &writefd);
			FD_SET(max_fd, &readfd);
			break;
		}
	}

	timeout.tv_sec = 0;
	timeout.tv_usec = 10000;   // 10 ms timeout
	ret = select(max_fd + 1, &readfd, &writefd, NULL, &timeout);
	if(ret == -1)
		perror("select");
	else if(ret > 0)
	{
		if(FD_ISSET(0, &readfd)) {
			char cmd[256];
			if(!fgets(cmd, sizeof(cmd), stdin))
				die("fgets: read error: %s\n", strerror(errno));
			process_cmd(cmd);
			printprompt();
		}
 		data_submit();
		data_read();
	}
}
#endif

#ifdef WIN32
#define ETIMEDOUT 116

static char cmd_buf[1024];
static HANDLE has_cmd_event;
static HANDLE cmd_done_event;
static HANDLE fgets_thread_handle;

static DWORD WINAPI fgets_thread(LPVOID param);

static void init_win32(void)
{
	has_cmd_event = CreateEvent(NULL, FALSE, FALSE, NULL);
	if(!has_cmd_event)
		die("CreateEvent failed (has_cmd_event). Error code: %d\n",
		    GetLastError());

	cmd_done_event = CreateEvent(NULL, FALSE, FALSE, NULL);
	if(!cmd_done_event)
		die("CreateEvent failed (cmd_done_event). Error code: %d\n",
		    GetLastError());

	fgets_thread_handle = CreateThread(NULL, 0, fgets_thread, NULL, 0, NULL);
	if(!fgets_thread_handle)
		die("CreateThread failed: %d\n", GetLastError());
}

static DWORD WINAPI fgets_thread(LPVOID param)
{
	while(true) {
		if(quit)
			break;

		fgets(cmd_buf, sizeof(cmd_buf), stdin);
		if(!SetEvent(has_cmd_event))
			die("SetEvent failed (has_cmd_event): %d\n",
			    GetLastError());

		if(WaitForSingleObject(cmd_done_event, INFINITE) != WAIT_OBJECT_0)
			die("WaitForSingleObject failed: %d\n", GetLastError());
	}

	return 0;
}

static void wait_for_stuff()
{
	HANDLE handles[1+NUM_ASYNCS];
	int num = 1, i;

//	printf("wait_for_stuff: data_tx/data_rx %d %d\n", data_tx_submit, data_rx_submit);
	handles[0] = has_cmd_event;
	if(data_tx_submit != -1 || data_rx_submit != -1) {
		for(i = 0; i < NUM_ASYNCS; i++) {
			if(data_async_context[i]) {
				handles[num] = usb_async_get_handle(data_async_context[i]);
				num++;
			}
		}
	}

	int ret = WaitForMultipleObjects(num, handles, FALSE, 10);
	// printf("WaitFor.. num: %d ret: %d\n", num, ret);

	if(ret < 0) {
		printf("WaitForMultipleObjects failed: %ld\n", GetLastError());
	}

	if(ret == WAIT_OBJECT_0) {
		process_cmd(cmd_buf);
		printprompt();
		if(!SetEvent(cmd_done_event))
			die("SetEvent failed (cmd_done_event): %d\n", GetLastError());
	}

        if((WAIT_OBJECT_0 < ret && ret < WAIT_OBJECT_0 + num) ||
           (ret == WAIT_OBJECT_0 && num > 1)) {
		printf("WaitFor.. data %d\n", ret);
		// We cannot load and dump at the same time.
		assert(data_tx_submit == -1 || data_rx_submit == -1);
		data_submit();
		data_read();

/*
		int ret;
		ret = usb_async_is_done(data_async_context);

		if(ret < 0) {
			printf("usb_async_is_done failed: %s\n", strerror(errno));
		} else if(ret) {
			data_tx_submit = -1;
			printf("Loading finished.\n");
		}
*/
	}
}
#endif

void usb_write(const char* data, int len)
{
	int ret;

	ret = usb_bulk_write(usb_handle, EP_COMMOUT, (char*) data, len, 100);
	if(ret < 0)
	{
		fprintf(stderr, "usb write error: %s\n", strerror(errno));
	}
	else if(ret < len)
	{
		fprintf(stderr, "wrote %d bytes of %d\n", ret, len);
	}
}

static int reap_all(bool cancel)
{
	int i, bytes_reaped = 0;
	for(i = 0; i < NUM_ASYNCS; i++) {
		if(data_async_context[i]) {
			int ret;
			if(cancel)
				ret = usb_reap_async(data_async_context[i], 0);
			else
				ret = usb_reap_async_nocancel(data_async_context[i], 0);
			if(ret < 0 && ret != -ETIMEDOUT) {
				printf("reap_all: usb_reap_async_(nocancel) failed: %s\n", usb_strerror());
			} else if(ret >= 0) {
				printf("reap_all: Transmitted %d bytes, reaping %d\n", ret, i);
				usb_free_async(data_async_context[i]);
				data_async_context[i] = NULL;
				bytes_reaped += ret;
			} else {
				;
				// printf("reap_all: Reaping %d failed with timeout\n", i);
			}
		}
	}

	return bytes_reaped;
}

static void data_submit()
{
	int i;

	if(data_tx_submit < 0)
		return;

	reap_all(false);
	send_query_crc(false);

	for(i = 0; i < NUM_ASYNCS; i++) {
		if(data_buffer->length <= data_tx_submit)
			break;

		if(data_async_context[i])
			continue;

		if(usb_interrupt_setup_async(handle_data, &data_async_context[i], DATAEP_OUT) < 0) {
			printf("Failed to initialize async data transfer: %s\n", usb_strerror());
			break;
		}

		int request = data_buffer->length - data_tx_submit;
		if(request > usb_max_read_write())
			request = usb_max_read_write();

		// printf("Submitting %d request: %d\n", i, request);
		int ret = usb_submit_async(data_async_context[i], (char*) data_buffer->data+data_tx_submit,
					   request);
		if(ret < 0) {
			printf("usb_submit_async failed: %s\n", usb_strerror());
			return;
		} else {
			data_tx_submit += request;
		}
	}
}


static void data_read()
{
	int i;

	if(data_rx_submit < 0)
		return;

	printf("data_read: rx_submit: %d\n", data_rx_submit);
	data_received += reap_all(false);
	// FIXME send_query_crc(false);

	for(i = 0; i < NUM_ASYNCS; i++) {
		if(data_async_context[i])
			continue;

		if(usb_interrupt_setup_async(handle_data, &data_async_context[i], DATAEP_IN) < 0) {
			printf("data_read: Failed to initialize async data transfer: %s\n", usb_strerror());
			break;
		}

		// We use a fairly small request size because it seems
		// that we either get nothing or an entire request of
		// data from use_submit_async and
		// usb_reap_async(_nocancel) (at least on Windows).
		int request = 256;

		printf("Submitting %d request: %d\n", i, request);
		int ret = usb_submit_async(data_async_context[i],
					   (char*) data_rx_buffer->data+data_rx_submit,
					   request);
		if(ret < 0) {
			printf("data_read: usb_submit_async failed: %s\n", usb_strerror());
			return;
		} else {
			data_rx_submit += request;
		}
	}
}

// static void reap_all(bool cancel)
static void data_read_stop()
{
	assert(data_rx_submit != -1);

	data_received += reap_all(true);
	printf("data_read_stop %d\n", data_received);
	// This is slightly hacky... We have only received
	// data_received bytes so we need to adjust
	// data_rx_buffer->length temporarily.
	int rx_size = data_rx_buffer->length;
	data_rx_buffer->length = data_received;

	struct tape_data* tape = from_internal(data_rx_buffer);
	printf("tape length: %d %p\n", tape->length, data_rx_file);
	data_rx_buffer->length = rx_size;

	if(save_tap(data_rx_file, tape)) {
		printf("Failed to write data to file: %s\n",
		       strerror(errno));
	}

	free(tape->data);
	free(tape);

	data_rx_file = NULL;
	data_rx_submit = -1;
}
