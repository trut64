#ifndef COMMANDS_H
#define COMMANDS_H

#include "common.h"

enum CmdCode {
	CmdUnknown,
	CmdQuit,
	CmdHelp,
	CmdAdvHelp,
	CmdPing,
	CmdLoad,
	CmdDump,
	CmdWrite,
	CmdSave,
	CmdStrDesc,
	CmdDesc,
	CmdDataMode,
	CmdBuffer,
	CmdStack,
	CmdCrc,
	CmdMode,
	CmdStart,
	CmdStop
};

typedef struct
{
	bool valid;          // set to true if the command has correct arguments
	enum CmdCode code;
	int argInt0;
	const char* argStr;
} Cmd;

Cmd parse_cmd(const char* cmd);

#endif
