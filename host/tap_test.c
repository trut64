/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "tap.h"

// Small test program
int main(int argc, char** argv)
{
    if(argc < 2) {
        fprintf(stderr, "Usage: tap <tap-file>\n");
        return 1;
    }

    const char* file = argv[1];
    FILE* f = fopen(file, "r");
    if(!f) {
        fprintf(stderr, "Unable to open '%s': %s\n", file, strerror(errno));
        return 1;
    }

    struct tape_data* t = load_tap(f);

    printf("TAP-file loaded successfully. Number of pulses: %d\n", t->length);

    printf("Initial pulse lengths: ");
    for(int i = 0; i < 20 && i < t->length; i++)
        printf("%d ", t->data[i]);
    putchar('\n');

    return 0;
}
