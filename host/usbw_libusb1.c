#include "usbw.h"

#include <libusb-1.0/libusb.h>
#include <stdlib.h>
#include <poll.h>

struct usbw_handle
{
	libusb_device_handle* device;
	const struct libusb_pollfd** pollfd;
};

struct usbw_async
{
	bool completed;
	bool cancelled;
	bool failed;
	struct libusb_transfer* xfer;
};

static libusb_context* context;
static int wdebug = 0;
static const int timeout = 0;

void usbw_asynccb(struct libusb_transfer* t);

int usbw_init()
{
	int ret;

	ret = libusb_init(&context);
	libusb_set_debug(context, 3);
	return ret;
}

void usbw_debug(int level)
{
	wdebug = level;
}

usbw_handle* usbw_scanbyvid(int vid, int pid)
{
	libusb_device_handle* dev;
	usbw_handle* h;

	h = 0;

	if(wdebug >= 2)
		fprintf(stderr, "usbw_scanbyvid(vid = %d, pid = %d)\n", vid, pid);

	dev = libusb_open_device_with_vid_pid(context, vid, pid);
	if(dev == 0)
		goto out;

	h = malloc(sizeof(usbw_handle));
	if(h == 0)
	{
		libusb_close(dev);
		dev = 0;
		goto out;
	}

out:
	if(dev)
		h->device = dev;

	if(wdebug >= 2)
	{
		if(dev)
			fprintf(stderr, "usbw_scanbyvid = %p\n", h);
		else
			fprintf(stderr, "usbw_scanbyvid = 0\n");
	}

	return h;
}

int usbw_close(usbw_handle* h)
{
	libusb_close(h->device);
	free(h);
	libusb_exit(context);

	return 0;
}

int usbw_get_configuration(usbw_handle* h, int* c)
{
	int ret;

	//ret = libusb_get_configuration(h->device, &config);
	*c = 1;
	ret = 0;

	if(wdebug >= 2)
		fprintf(stderr, "usbw_get_configuration(*c = %d) = %d\n", *c, ret);

	return ret;
}

int usbw_set_configuration(usbw_handle* h, int c)
{
	int ret;

	ret = libusb_set_configuration(h->device, c);

	if(wdebug >= 2)
		fprintf(stderr, "usbw_set_configuration(%d) = %d\n", c, ret);

	if(ret)
		return -1;
	return 0;
}

int usbw_claim_interface(usbw_handle* h, int iface)
{
	int ret;

	ret = libusb_claim_interface(h->device, iface);

	if(wdebug >= 2)
		fprintf(stderr, "usbw_claim_interface(%d) = %d\n", iface, ret);

	if(ret)
		return -1;
	return 0;
}

int usbw_release_interface(usbw_handle* h, int iface)
{
	int ret;

	ret = libusb_release_interface(h->device, iface);

	if(wdebug >= 2)
		fprintf(stderr, "usbw_release_interface(%d) = %d\n", iface, ret);

	if(ret)
		return -1;
	return 0;
}

int usbw_set_interface_alt_setting(usbw_handle* h, int iface, int setting)
{
	int ret;

	ret = libusb_set_interface_alt_setting(h->device, iface, setting);

	if(wdebug >= 2)
		fprintf(stderr, "usbw_set_interface_alt_setting(%d, %d) = %d\n",
				iface, setting, ret);
	if(ret)
		return -1;
	return 0;
}

int usbw_sync(usbw_handle* h, usbw_xfertype type, int ep, uint8_t* buffer,
			  int length, int* xferd)
{
	int ret;

	if(wdebug >= 2)
		fprintf(stderr, "usbw_sync(type = %d, ep = %d, length = %d)\n",
				type, ep, length);

	if(type == Bulk)
		ret = libusb_bulk_transfer(h->device, ep, buffer, length, xferd,
								   timeout);
	else if(type == Interrupt)
		ret = libusb_interrupt_transfer(h->device, ep, buffer, length,
										xferd, timeout);
	else
	{
		if(wdebug >= 1)
			fprintf(stderr, "usbw_sync: invalid type\n");
		return -1;
	}

	if(wdebug >= 2)
		fprintf(stderr, "usbw_sync(*xferd = %d) = %d\n", *xferd, ret);

	if(ret)
		return -1;
	return 0;
}

usbw_async* usbw_asyncinit(usbw_handle* h, usbw_xfertype type, int ep,
						   uint8_t* buffer, int length)
{
	struct libusb_transfer* xfer;
	usbw_async* async;

	if(wdebug >= 2)
		fprintf(stderr, "usbw_asyncinit(type = %d, ep = %d, length = %d)\n",
				type, ep, length);

	xfer = libusb_alloc_transfer(0);
	if(!xfer)
	{
		if(wdebug >= 1)
			fprintf(stderr, "usbw_asyncinit: failed to allocate transfer\n");
		return 0;
	}

	async = malloc(sizeof(usbw_async));
	if(!async)
	{
		if(wdebug >= 1)
			fprintf(stderr, "usbw_asyncinit: failed to allocate async\n");
		return 0;
	}

	if(type == Bulk)
	{
		libusb_fill_bulk_transfer(xfer, h->device, ep, buffer, length,
								  usbw_asynccb, async, timeout);
	}
	else if(type == Interrupt)
	{
		libusb_fill_interrupt_transfer(xfer, h->device, ep, buffer, length,
									   usbw_asynccb, async, timeout);
	}
	else
	{
		if(wdebug >= 1)
			fprintf(stderr, "usbw_asyncinit: invalid type\n");
		return 0;
	}

	async->xfer = xfer;
	async->completed = false;
	async->cancelled = false;
	async->failed = false;

	if(libusb_submit_transfer(xfer))
	{
		if(wdebug >= 1)
			fprintf(stderr, "usbw_asyncinit: submit failed\n");
		return 0;
	}

	if(wdebug >= 2)
		fprintf(stderr, "usbw_asyncinit() = %p\n", async);

	return async;
}

int usbw_asyncfree(usbw_async* a)
{
	if(wdebug >= 2)
		fprintf(stderr, "usbw_asyncfree(%p)\n", a);

	libusb_free_transfer(a->xfer);
	free(a);

	return 0;
}

int usbw_asynccomplete(usbw_async* a, bool* complete)
{
	int ret;

	if(a)
	{
		*complete = a->completed;
		ret = 0;
	}
	else
		ret = -1;

	return ret;
}

int usbw_asyncxferlen(usbw_async* a, int* length)
{
	int ret;

	if(a)
	{
		*length = a->xfer->actual_length;
		ret = 0;
	}
	else
		ret = -1;

	if(wdebug >= 2)
		fprintf(stderr, "usbw_asyncxferlen(*length = %d) = %d\n", *length, ret);

	return ret;
}

int usbw_asyncresubmit(usbw_async* a)
{
	int ret;

	if(wdebug >= 2)
		fprintf(stderr, "usbw_asyncresubmit(%p) ", a);

	if(a->completed)
	{
		if(wdebug >= 2)
			fprintf(stderr, "(completed)\n");

		a->completed = false;
		a->cancelled = false;
		a->failed = false;
		ret = libusb_submit_transfer(a->xfer);

		if(wdebug >= 1)
			if(ret != 0)
				fprintf(stderr, "usbw_asyncresubmit(): submit failed");
	}
	else
	{
		if(wdebug >= 2)
			fprintf(stderr, "(pending)\n");

		ret = -1;
	}

	if(wdebug >= 2)
		fprintf(stderr, "usbw_asyncresubmit() = %d\n", ret);

	if(ret)
		return -1;
	return 0;
}

int usbw_asynccancel(usbw_async* a)
{
	int ret;

	ret = libusb_cancel_transfer(a->xfer);

	if(wdebug >= 2)
		fprintf(stderr, "usbw_asynccancel(%p) = %d\n", a, ret);

	if(ret)
		return -1;
	return 0;
}

int usbw_asynccancelled(usbw_async* a, bool* cancelled)
{
	int ret;

	if(a)
	{
		*cancelled = a->cancelled;
		ret = 0;
	}
	else
		ret = -1;

	return ret;
}

usbw_waitables* usbw_getwaitables(usbw_handle* h)
{
	const struct libusb_pollfd** fds;
	const struct libusb_pollfd* fd;
	usbw_waitables* w;
	int i;
	int rd;
	int wr;

	fds = libusb_get_pollfds(context);

	if(!fds)
		return 0;

	w = malloc(sizeof(usbw_waitables));
	if(!w)
		return 0;

	w->num_readobj = 0;
	w->num_writeobj = 0;

	for(i = 0; fds[i]; i++)
	{
		fd = fds[i];
		if(fd->events & POLLIN)
			w->num_readobj++;
		if(fd->events & POLLOUT)
			w->num_writeobj++;
	}

	w->readobj = malloc(w->num_readobj*sizeof(waitable));
	w->writeobj = malloc(w->num_writeobj*sizeof(waitable));
	if(w->readobj == 0 || w->writeobj == 0)
		return 0;

	rd = 0;
	wr = 0;
	for(i = 0; fds[i]; i++)
	{
		fd = fds[i];
		if(fd->events & POLLIN)
			w->readobj[rd++] = fd->fd;
		if(fd->events & POLLOUT)
			w->writeobj[wr++] = fd->fd;
	}

	w->timeout_pending = libusb_get_next_timeout(context, &w->timeout);

	h->pollfd = fds;

	return w;
}

int usbw_freewaitables(usbw_handle* h, usbw_waitables* w)
{
	free(w->readobj);
	free(w->writeobj);
	free(w);
	free(h->pollfd);

	return 0;
}

int usbw_handleevents(usbw_handle* h)
{
	int ret;
	struct timeval tv;

	if(wdebug >= 2)
		fprintf(stderr, "usbw_handleevents()\n");

	tv.tv_sec = 0;
	tv.tv_usec = 0;
	ret = libusb_handle_events_timeout(context, &tv);

	if(wdebug >= 2)
		fprintf(stderr, "usbw_handleevents() = %d\n", ret);

	if(ret)
		return -1;
	return 0;
}

void usbw_asynccb(struct libusb_transfer* t)
{
	usbw_async* a = (usbw_async*) t->user_data;

	if(wdebug >= 2)
		fprintf(stderr, "usbw_asynccb(%p, ep = %d, status = %d, length = %d, xferd = %d)\n",
				a, t->endpoint, t->status, t->length, t->actual_length);

	a->completed = true;
	if(t->status != LIBUSB_TRANSFER_COMPLETED)
		a->failed = true;
	if(t->status == LIBUSB_TRANSFER_CANCELLED)
		a->cancelled = true;
}
