#include "commands.h"
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

typedef struct
{
	const char* cmd;
	enum CmdCode code;
	int argType;         // 0 - no args, 1 - one int arg, 2 - string arg
} CmdSpec;

const CmdSpec cmdspec[] = {
	{"quit", CmdQuit, 0},
	{"help", CmdHelp, 0},
	{"advhelp", CmdAdvHelp, 0},
	{"ping", CmdPing, 0},
	{"load", CmdLoad, 2},
	{"dump", CmdDump, 2},
	{"write", CmdWrite, 2},
	{"save", CmdSave, 2},
	{"strdesc", CmdStrDesc, 1},
	{"desc", CmdDesc, 1},
	{"data", CmdDataMode, 2},
	{"buffer", CmdBuffer, 0},
	{"stack", CmdStack, 0},
	{"crc", CmdCrc, 0},
	{"mode", CmdMode, 2},
	{"start", CmdStart, 1},
	{"stop", CmdStop, 0},
	{0, CmdUnknown, 0}
};

int cmd_intarg(const char* cmdargs, int* iarg);

static char argstr[256];

Cmd parse_cmd(const char* cmd)
{
	char cmdstr[16];
	int cmdlen;
	int arglen;
	const char* a;
	int i;
	Cmd c;

	c.valid = false;
	c.code = CmdUnknown;
	c.argInt0 = 0;
	c.argStr = 0;

	while(*cmd)
	{
		if(isspace(*cmd))
			cmd++;
		else
			break;
	}

	a = cmd;
	while(*a)
	{
		if(isspace(*a))
			break;
		else
			a++;
	}
	cmdlen = a-cmd;
	if(cmdlen < sizeof(cmdstr))
	{
		strncpy(cmdstr, cmd, cmdlen);
		cmdstr[cmdlen] = 0;
	}
	else
	{
		return c;
	}

	while(isspace(*a))
		a++;

	// Get rid of trailing whitespace
	arglen = strlen(a);
	while(isspace(a[arglen-1]) && arglen > 0)
		arglen--;

	if(arglen < sizeof(argstr))
	{
		strncpy(argstr, a, arglen);
		argstr[arglen] = 0;
	}
	else
	{
		return c;
	}

	i = 0;
	while(cmdspec[i].cmd)
	{
		if(strcmp(cmdstr, cmdspec[i].cmd) == 0)
		{
			c.code = cmdspec[i].code;
			switch(cmdspec[i].argType)
			{
			case 0: // No arguments
				if(arglen == 0)
				{
					c.valid = true;
				}
				break;
			case 1: // 1 int argument
				if(cmd_intarg(argstr, &c.argInt0) == 0)
				{
					c.valid = true;
				}
				break;
			case 2: // 1 string argument
				if(arglen > 0)
				{
					c.valid = true;
					c.argStr = argstr;
				}
				break;
			}
			break;
		}
		i++;
	}

	return c;
}

int cmd_intarg(const char* cmdargs, int* iarg)
{
	char* arge;

	*iarg = strtol(cmdargs, &arge, 10);
	if(strlen(cmdargs) == 0 || cmdargs+strlen(cmdargs) > arge)
		return 1;

	return 0;
}
