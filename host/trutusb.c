#include "trutusb.h"

#include <string.h>
#include "usbw.h"
#include "trutproto.h"

static usbw_handle* trutdev;
static usbw_async* trutdata;
static usbw_async* trutmsg;

static uint8_t msgbuffer[16];

static enum usbw_xfertype xfertype;

int trutusb_sendcmd(int cmd, uint8_t* arg, int arglen);

int trutusb_init()
{
	int ret;

	trutdev = 0;
	trutdata = 0;
	trutmsg = 0;

	xfertype = Interrupt;

	ret = usbw_init();
	return ret;
}

int trutusb_scan()
{
	trutdev = usbw_scanbyvid(VENDOR_ID, PRODUCT_ID);

	return 0;
}

bool trutusb_isattached()
{
	return trutdev != 0;
}

int trutusb_claim()
{
	int ret;
	int config;

	ret = usbw_get_configuration(trutdev, &config);
	if(ret < 0)
		return ret;

	if(config != 1)
	{
		ret = usbw_set_configuration(trutdev, 1);
		if(ret < 0)
			return ret;
	}

	ret = usbw_claim_interface(trutdev, TRUT_IF_COMM);
	if(ret < 0)
		return ret;

	ret = usbw_claim_interface(trutdev, TRUT_IF_DATA);
	if(ret < 0)
		return ret;

	trutmsg = usbw_asyncinit(trutdev, Bulk, TRUT_EP_MSG_IN, msgbuffer,
							 sizeof(msgbuffer));
	if(trutmsg == 0)
		return -1;

	return 0;
}

int trutusb_release()
{
	int ret;

	if(trutmsg)
		usbw_asyncfree(trutmsg);

	if(trutdata)
		usbw_asyncfree(trutdata);

	ret = usbw_release_interface(trutdev, TRUT_IF_DATA);
	if(ret < 0)
		return ret;

	ret = usbw_release_interface(trutdev, TRUT_IF_COMM);
	if(ret < 0)
		return ret;

	ret = usbw_close(trutdev);
	if(ret < 0)
		return ret;

	return 0;
}

usbw_waitables* trutusb_getwaitables()
{
	return usbw_getwaitables(trutdev);
}

void trutusb_freewaitables(usbw_waitables* waitables)
{
	usbw_freewaitables(trutdev, waitables);
}

int trutusb_setxfertype(enum usbw_xfertype type)
{
	if(trutdata)
		return -1;

	xfertype = type;
	return 0;
}

int trutusb_xferinit(int mode, uint8_t* buffer, int length)
{
	int dir;
	int ret;
	int l;
	int ep;
	uint8_t arg[2];

	switch(mode)
	{
	case TRUT_MODE_LOAD:
	case TRUT_MODE_WRITE:
		dir = 0;
		ep = TRUT_EP_DATA_OUT;
		break;
	case TRUT_MODE_DUMP:
	case TRUT_MODE_SAVE:
		dir = 1;
		ep = TRUT_EP_DATA_IN;
		break;
	default:
		return -1;
	}

	if(dir == 0)
	{
		if(xfertype == Bulk)
			ret = usbw_set_interface_alt_setting(trutdev, TRUT_IF_DATA,
												 TRUT_ALTIF_DATA_BULKOUT);
		else if(xfertype == Interrupt)
			ret = usbw_set_interface_alt_setting(trutdev, TRUT_IF_DATA,
												 TRUT_ALTIF_DATA_INTOUT);
		else
			return -1;
	}
	else
	{
		if(xfertype == Bulk)
			ret = usbw_set_interface_alt_setting(trutdev, TRUT_IF_DATA,
												 TRUT_ALTIF_DATA_BULKIN);
		else if(xfertype == Interrupt)
			ret = usbw_set_interface_alt_setting(trutdev, TRUT_IF_DATA,
												 TRUT_ALTIF_DATA_INTIN);
		else
			return -1;
	}

	if(ret < 0)
		return ret;

	arg[0] = mode;
	trutusb_sendcmd(TRUT_CMD_SETMODE, arg, 1);

	if(length % 64 == 0)
		l = length/64;
	else
		l = length/64+1;

	arg[0] = l >> 8;
	arg[1] = l & 0xff;
	trutusb_sendcmd(TRUT_CMD_DATASTART, arg, 2);

	if(xfertype == Bulk)
		trutdata = usbw_asyncinit(trutdev, Bulk, ep, buffer, length);
	else
		trutdata = usbw_asyncinit(trutdev, Interrupt, ep, buffer, length);

	if(trutdata == 0)
		return -1;

	return 0;
}

bool trutusb_xferactive()
{
	return trutdata != 0;
}

bool trutusb_xfercomplete()
{
	int ret;
	bool complete;

	ret = usbw_asynccomplete(trutdata, &complete);

	if(ret < 0)
		return false;
	return complete;
}

int trutusb_xferclean()
{
	int ret;
	bool complete;

	if(trutdata == 0)
		return -1;

	ret = usbw_asynccomplete(trutdata, &complete);
	if(ret < 0)
		return ret;

	if(complete)
	{
		usbw_asyncfree(trutdata);
		trutdata = 0;
	}
	else
	{
		ret = usbw_asynccancel(trutdata);
		if(ret < 0)
			return ret;
	}

	return 0;
}

void trutusb_handleevents()
{
	int ret;
	bool cancelled;

	usbw_handleevents(trutdev);

	if(trutdata)
	{
		ret = usbw_asynccancelled(trutdata, &cancelled);
		if(ret < 0)
			return;

		if(cancelled)
		{
			usbw_asyncfree(trutdata);
			trutdata = 0;
		}
	}
}

int trutusb_setmsg(bool enabled)
{
	uint8_t arg;

	if(enabled)
		arg = TRUT_CMD_SETMSG_ENABLED;
	else
		arg = TRUT_CMD_SETMSG_DISABLED;

	return trutusb_sendcmd(TRUT_CMD_SETMSG, &arg, 1);
}

int trutusb_ping()
{
	return trutusb_sendcmd(TRUT_CMD_PING, 0, 0);
}

int trutusb_querybufferusage()
{
	return trutusb_sendcmd(TRUT_CMD_QUERYBUFFERUSAGE, 0, 0);
}

int trutusb_querystackusage()
{
	return trutusb_sendcmd(TRUT_CMD_QUERYSTACKUSAGE, 0, 0);
}

int trutusb_setmode(uint8_t mode)
{
	uint8_t arg[1];

	arg[0] = mode;

	return trutusb_sendcmd(TRUT_CMD_SETMODE, arg, 1);
}

int trutusb_start(uint16_t length)
{
	uint8_t arg[2];

	arg[0] = length >> 8;
	arg[1] = length & 0xff;

	return trutusb_sendcmd(TRUT_CMD_DATASTART, arg, 2);
}

bool trutusb_hasmsg()
{
	int ret;
	bool complete;

	ret = usbw_asynccomplete(trutmsg, &complete);

	if(ret < 0)
		return false;
	return complete;
}

int trutusb_getmsg(uint8_t* buffer, int length)
{
	int ret;
	int bytes;

	ret = usbw_asyncxferlen(trutmsg, &bytes);
	if(ret < 0)
		return ret;

	if(bytes <= length)
	{
		memcpy(buffer, msgbuffer, bytes);
	}
	else
	{
		memcpy(buffer, msgbuffer, length);
		bytes = length;
	}

	//usbw_asyncresubmit(trutmsg);
	usbw_asyncfree(trutmsg);
	trutmsg = usbw_asyncinit(trutdev, Bulk, TRUT_EP_MSG_IN, msgbuffer,
							 sizeof(msgbuffer));
	if(trutmsg == 0)
		return -1;

	return bytes;
}

int trutusb_sendcmd(int cmd, uint8_t* arg, int arglen)
{
	int ret;
	int bytes;
	uint8_t buffer[16];

	if(arglen > sizeof(buffer)-2)
		return -1;

	buffer[0] = cmd;
	buffer[1] = arglen;
	memcpy(&buffer[2], arg, arglen);

	ret = usbw_sync(trutdev, Bulk, TRUT_EP_CMD_OUT, buffer, 2+arglen, &bytes);
	if(ret < 0)
		return ret;

	if(bytes == 2+arglen)
		return 0;
	else
		return -1;
}
