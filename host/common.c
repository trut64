/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// We need to define DEBUG here, otherwise debug will be replaced by
// debug_nop and we won't be able to define debug below.
#define DEBUG 1

#include "common.h"
/* // We now use stdbool.h instead
const bool true = 1;
const bool false = 0;
*/
void die(const char *err, ...)
{
	va_list params;

	va_start(params, err);
	vfprintf(stderr, err, params);
	va_end(params);

	exit(1);
}

void debug(const char *err, ...)
{
	va_list params;
	fputs("Debug: ", stderr);

	va_start(params, err);
	vfprintf(stderr, err, params);
	va_end(params);

	fputc('\n', stderr);
}

void debug_nop(const char *err, ...)
{ }

void warning(const char *warn, ...)
{
	va_list params;
	fputs("Warning: ", stderr);

	va_start(params, warn);
	vfprintf(stderr, warn, params);
	va_end(params);

	fputc('\n', stderr);
}

void *xmalloc(size_t size)
{
	void *ret = malloc(size);
	if (!ret && !size)
		ret = malloc(1);
	if (!ret)
		die("Out of memory, malloc failed");
	return ret;
}
