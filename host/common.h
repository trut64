/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h> // Instead of defining our own bools (as done previously)
/* // We now use stdbool.h instead
#ifndef bool
typedef int bool;
#endif
#ifndef true
extern const bool true;
#endif
#ifndef false
extern const bool false;
#endif
*/
void die(const char *err, ...);
void warning(const char *err, ...) __attribute__((format (printf, 1, 2)));
void debug(const char *err, ...) __attribute__((format (printf, 1, 2)));
void debug_nop(const char *err, ...) __attribute__((format (printf, 1, 2)));

#ifndef DEBUG
#  define debug debug_nop
#endif

void* xmalloc(size_t);

#endif
