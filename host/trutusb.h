/*
 * Copyright (C) 2008 Anton Blad
 * Copyright (C) 2008 Fredrik Kuivinen
 * Copyright (C) 2008 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#ifndef TRUTUSB_H
#define TRUTUSB_H

#include <stdint.h>
#include "common.h"
#include "usbw.h"
#include "trutproto.h"

#define VENDOR_ID             0x03EB // Atmel vendor ID = 03EBh
#define PRODUCT_ID            0x0000

// Call at program start.
int trutusb_init();

// Call to scan the usb bus for TRUT. Always returns zero, but may be
// changed later to implement dynamic scanning.
int trutusb_scan();

// Returns true if a TRUT was found.
bool trutusb_isattached();

// Set configuration, claim interfaces, send init commands. Returns zero
// if successful.
int trutusb_claim();

// Release the device. Returns zero if successful.
int trutusb_release();

// Returns a list of waitable objects.
usbw_waitables* trutusb_getwaitables();

// Free waitables after use.
void trutusb_freewaitables(usbw_waitables* waitables);

// Sets Bulk or Interrupt transfer.
int trutusb_setxfertype(enum usbw_xfertype type);

// Sets alternate interface and initializes data transfer. Returns zero if
// successful.
int trutusb_xferinit(int mode, uint8_t* buffer, int length);

// Returns true if there is a data transfer active.
bool trutusb_xferactive();

// Returns true if all requested data have been transferred.
bool trutusb_xfercomplete();

// Call to clean up resources associated with a transfer. The function will
// correctly cancel the transfer if it is not completed. However, the
// cancellation may be asynchronous, so no new transfer shall be initiated
// until trutusb_xferactive() returns false. The function returns zero if
// successful.
int trutusb_xferclean();

// Call when the corresponding waitable object does not block. Returns zero
// if successful.
void trutusb_handleevents();

// TRUT commands. Returns zero if successful.
int trutusb_setmsg(bool enabled);
int trutusb_ping();
int trutusb_querybufferusage();
int trutusb_querystackusage();
int trutusb_setmode(uint8_t mode);
int trutusb_start(uint16_t length);

// Returns true if there is a message received.
bool trutusb_hasmsg();

// Read a message from the message pipe. Returns number of bytes read.
int trutusb_getmsg(uint8_t* buffer, int length);

#endif
