/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>

#if defined(__unix__) || defined(__APPLE__)
#  include <sys/select.h>
#endif

#ifdef WIN32
#  include <windows.h>

static void init_win32(void);
#endif

#include "commands.h"
#include "common.h"
#include "tap.h"
#include "trutusb.h"
#include "trutproto.h"

void usage();
int parse_opts(int argc, char* argv[]);

enum TapTransferMode { TapIdle, TapSending, TapReceiving, TapCancellingSending };

static int quit = 0;

// Specifies if a command was given on the command line. opt_singlecmd
// is 1 if a command was given on the command line.
static int opt_singlecmd = 0;
static enum CmdCode opt_cmd = 0;
static const char* opt_cmdarg = 0;

// -c option: Do not quit after the command has been completed.
static int opt_cmdnoquit = 0;

// -d option: Debug level.
static int opt_debug = 0;

static enum TapTransferMode tap_mode = TapIdle;
static struct buffer* data_buffer;
static bool crc_query_from_user;

int parse_opts(int argc, char* argv[]);
void print_msg(const char* data, int size);
void printprompt();
int cmd_intarg(const char* cmdargs, int* iarg);

static int wait_for_stuff(char* cmd, int cmdsize,
						  usbw_waitables* waitables, char* events);

void dump_hex(const char* data, int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		printf("%02hhX ", data[i]);
	}
	fflush(stdout);
}

// Copied from the avr-libc documentation
static uint16_t crc16_update(uint16_t crc, uint8_t a)
{
	int i;

	crc ^= a;
	for (i = 0; i < 8; ++i)
	{
		if (crc & 1)
			crc = (crc >> 1) ^ 0xA001;
		else
			crc = (crc >> 1);
	}

	return crc;
}

static uint16_t compute_crc(uint8_t* buf, int len)
{
	uint16_t crc = 0xffff;
	int i;

	for(i = 0; i < len; i++) {
		crc = crc16_update(crc, buf[i]);
	}

	return crc;
}

void msg_error(const char* data, int size)
{
	int i;
	printf("Invalid message received. Printing verbatim:\n");
	for(i = 0; i < size; i++)
		printf("%02hhX ", data[i]);
	putchar('\n');
}

void print_msg(const char* data, int size)
{
	int arglen = size - 1;
	const char* args = data + 1;
	int i = 0;
	bool print_prompt = true;

	switch(data[0])
	{
	case TRUT_MSG_STR:
		for(i = 1; i < arglen; i++)
			putchar(args[i]);

		if(arglen == 0)
			fputs("Got zero length TRUT_MSG_STR", stdout);
		putchar('\n');
		break;
	case TRUT_MSG_HEX:
		for(i = 0; i < arglen; i++)
			printf("%02hhX ", args[i]);

		if(arglen == 0)
			fputs("Got zero length TRUT_MSG_HEX", stdout);
		putchar('\n');
		break;
	case TRUT_MSG_PONG:
		printf("Pong!\n");
		break;
	case TRUT_MSG_INVALIDCMD:
		printf("TRUT says invalid command.\n");
		break;
	case TRUT_MSG_PROTOERROR:
		printf("Protocol error\n");
		break;
	case TRUT_MSG_MOTOR:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		switch(args[0])
		{
		case TRUT_MSG_MOTOR_OFF:
			printf("Motor is off\n");
			break;
		case TRUT_MSG_MOTOR_ON:
			printf("Motor is on\n");
			break;
		default:
			printf("Motor in invalid state\n");
			break;
		}
		break;
	case TRUT_MSG_STACKUSAGE:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		printf("Maximum stack usage: %d bytes\n", args[0]);
		break;
	case TRUT_MSG_BUFFERUSAGE:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		printf("Minimum/maximum buffer usage: %d bytes\n", args[0]);
		break;
	case TRUT_MSG_COMMERROR:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		switch(args[0])
		{
		case TRUT_MSG_COMMERROR_BUFFERUF:
			printf("TRUT: Buffer underflow!\n");
			break;
		case TRUT_MSG_COMMERROR_BUFFEROF:
			printf("TRUT: Buffer overflow!\n");
			break;
		case TRUT_MSG_COMMERROR_READDATA:
			printf("TRUT: Error reading received usb data to buffer\n");
			break;
		default:
			printf("TRUT: Unknown communication error.\n");
		}
		break;
	case TRUT_MSG_ACCEPTINGDATA:
		printf("TRUT is now accepting data.\n");
		break;
	case TRUT_MSG_CRC:
	{
		if(arglen != 6)
		{
			msg_error(data, size);
			break;
		}
		uint8_t args0 = args[0];
		uint8_t args1 = args[1];
		uint8_t args2 = args[2];
		uint8_t args3 = args[3];
		uint32_t crc_len = (args0 << 24) |  (args1 << 16) |  (args2 << 8) | args3;
		crc_len *= 2; // the received length is in words

		args0 = args[4];
		args1 = args[5];
		uint16_t received_crc = (args0 << 8) | args1;

		if(!data_buffer) {
			printf("Got CRC %x. Cannot compute expected CRC as no "
			       "TAP-file is loaded.\n", received_crc);
			break;
		}

		uint16_t computed_crc = compute_crc(data_buffer->data, crc_len);
		if(computed_crc != received_crc) {
			printf("CRC FAILURE: Got CRC %x expected %x (length in bytes: %d)\n",
			       received_crc, computed_crc, crc_len);
		} else if(crc_query_from_user) {
			printf("Got expected CRC %x (length in bytes: %d)\n",
			       computed_crc, crc_len);
			crc_query_from_user = false;
		} else {
			print_prompt = false;
		}
	}
	break;
	case TRUT_MSG_TIMERERROR:
		printf("TRUT: Timer error!\n");
		break;
	case TRUT_MSG_DUMPSTARTED:
		printf("TRUT: Dump started.\n");
		break;
	case TRUT_MSG_DUMPFINISHED:
		printf("TRUT: Dump finished.\n");
		break;
	case TRUT_MSG_LOADSTARTED:
		printf("TRUT: Load started.\n");
		break;
	case TRUT_MSG_LOADFINISHED:
		printf("TRUT: Load finished.\n");
		break;
	default:
		printf("Invalid message received. Printing verbatim:\n");
		for(i = 0; i < size; i++)
			printf("%02hhX ", data[i]);
		putchar('\n');
	}

	if(print_prompt) {
		putchar('\n');
		printprompt();
	}
}

typedef struct
{
	unsigned char requestType;
	unsigned char request;
	unsigned short value;
	unsigned short index;
	unsigned short length;
} setup_request;

void print_unicode(char* str, int len)
{
	while(len--)
	{
		printf("%c", *str);
		str += 2;
	}
}

void print_strdesc(char* str, int len)
{
	if(len >= 2)
		print_unicode(str+2, len-2);
}

void start_tap_send(int mode, const char* file)
{
	FILE* f;
	struct tape_data* tap;

	f = fopen(file, "rb");
	if(!f) {
		printf("Unable to open '%s': %s\n", file, strerror(errno));
		return;
	}

	printf("Loading '%s'\n", file);
	tap = load_tap(f);
	fclose(f);

	data_buffer = jakob_internal(tap);
	free_tap(tap);
	printf("Buffer length: %d\n", data_buffer->length);

	if(trutusb_xferinit(mode, data_buffer->data, data_buffer->length) == 0)
	{
		printf("Initialized data transfer of %d bytes\n", data_buffer->length);
		tap_mode = TapSending;
	}
	else
	{
		printf("Failed to initialize data transfer\n");
		free_buffer(data_buffer);
	}
}

void start_tap_receive(int mode, const char* file)
{
	printf("Later...\n");
}

void process_cmd(const char* cmdstr)
{
	Cmd cmd;

	cmd = parse_cmd(cmdstr);

	if(!cmd.valid)
	{
		switch(cmd.code)
		{
		case CmdQuit:
		case CmdHelp:
		case CmdAdvHelp:
		case CmdPing:
		case CmdBuffer:
		case CmdStack:
		case CmdCrc:
		case CmdStop:
			printf("Command does not take arguments.\n");
			break;
		case CmdLoad:
		case CmdDump:
		case CmdWrite:
		case CmdSave:
			printf("Command needs file name as argument.\n");
			break;
		case CmdStrDesc:
		case CmdDesc:
			printf("Command needs descriptor number as argument.\n");
			break;
		case CmdDataMode:
			printf("Arguments are: bulk, int and iso.\n");
			break;
		case CmdMode:
			printf("Arguments are: pass, load, dump, write, save.\n");
			break;
		case CmdStart:
			printf("Command needs number of blocks as argument.\n");
			break;
		case CmdUnknown:
		default:
			printf("Unrecognized command.\n");
			break;
		}
		return;
	}

	switch(cmd.code)
	{
	case CmdQuit:
		quit = 1;

		break;
	case CmdHelp:
		printf("Available commands:\n");
		printf("quit      : Quit the program.\n");
		printf("help      : This help text.\n");
		printf("advhelp   : Help for advanced commands.\n");
		printf("ping      : Check if trut is alive.\n");
		printf("load file : Load a TAP-file.\n");
		printf("dump file : Dump to a TAP-file (from tape).\n");
		printf("write file: Write a TAP-file to tape (not implemented)\n");
		printf("save file : Dump to a TAP-file (from C64).\n");
		printf("stop      : Stop an ongoing load, dump, write or save.\n");
		break;
	case CmdAdvHelp:
		printf("str n     : Request string descriptor n.\n");
		printf("desc n    : Request descriptor n.\n");
		printf("data mode : Use the specified mode for data transfers. mode can be:\n");
		printf("            bulk (bulk), int (interrupt) or iso (isochronous).\n");
		printf("buffer    : Check minimum/maximum buffer usage.\n");
		printf("stack     : Check maximum stack usage.\n");
		printf("mode m    : Set operation mode. m can be: pass, load, write, save, dump\n");
		printf("          : pass:  Pass-through connection TAPE <-> C64\n");
		printf("          : load:  Host -> C64\n");
		printf("          : write: Host -> TAPE\n");
		printf("          : save:  C64  -> Host\n");
		printf("          : dump:  TAPE -> Host\n");
		printf("start len : Tell trut the length in 64-byte blocks of the data to be sent.\n");
		printf("            load will automatically send an appropriate start message.\n");
		printf("crc       : Make an explicit CRC query.\n");
		break;
	case CmdStrDesc:
	case CmdDesc:
		printf("Not implemented anymore.\n");
		break;
	case CmdDataMode:
		if(trutusb_xferactive())
		{
			printf("Can not change transfer mode while transfer is active.\n");
		}
		else
		{
			enum usbw_xfertype t;

			if(strcmp(cmd.argStr, "bulk") == 0)
			{
				t = Bulk;
			}
			else if(strcmp(cmd.argStr, "int") == 0)
			{
				t = Interrupt;
			}
			else if(strcmp(cmd.argStr, "iso") == 0)
			{
				t = Isochronous;
			}
			else // To get rid of the "t might be uninitialized" warning
			{
				printf("Sanity check: This will never happen\n");
				exit(1);
			}

			if(trutusb_setxfertype(t) == 0)
			{
				printf("Successfully set %s transfer mode\n", cmd.argStr);
			}
			else
			{
				printf("Error while setting transfer mode\n");
			}
		}
		break;
	case CmdPing:
		if(trutusb_ping() == 0)
			printf("Sent ping...\n");
		else
			printf("Error while sending ping.\n");
		break;
	case CmdBuffer:
		if(trutusb_querybufferusage() == 0)
			printf("Sent buffer usage query...\n");
		else
			printf("Error while sending buffer usage query.\n");
		break;
	case CmdStack:
		if(trutusb_querystackusage() == 0)
			printf("Sent stack usage query...\n");
		else
			printf("Error while sending stack usage query.\n");
		break;
	case CmdMode:
		printf("Sending mode (but consider using basic commands)\n");
		{
			uint8_t mode;
			if(strcmp(cmd.argStr, "pass") == 0)
				mode = TRUT_MODE_PASS;
			else if(strcmp(cmd.argStr, "load") == 0)
				mode = TRUT_MODE_LOAD;
			else if(strcmp(cmd.argStr, "write") == 0)
				mode = TRUT_MODE_WRITE;
			else if(strcmp(cmd.argStr, "save") == 0)
				mode = TRUT_MODE_SAVE;
			else if(strcmp(cmd.argStr, "dump") == 0)
				mode = TRUT_MODE_DUMP;
			else
			{
				printf("Invalid mode. Try pass, load, write, save or dump.\n");
				break;
			}

			trutusb_setmode(mode);
			break;
		}
	case CmdStart:
		printf("Sending start (but consider using basic commands)\n");
		trutusb_start(cmd.argInt0);
		break;
	case CmdLoad:
	case CmdWrite:
		if(tap_mode == TapIdle)
		{
			if(cmd.code == CmdLoad)
				start_tap_send(TRUT_MODE_LOAD, cmd.argStr);
			else
				start_tap_send(TRUT_MODE_WRITE, cmd.argStr);
		}
		else
		{
			printf("Data transfer active. Abort it first with \"stop\".\n");
		}
		break;
	case CmdDump:
	case CmdSave:
		if(tap_mode == TapIdle)
		{
			if(cmd.code == CmdDump)
				start_tap_receive(TRUT_MODE_DUMP, cmd.argStr);
			else
				start_tap_receive(TRUT_MODE_SAVE, cmd.argStr);
		}
		else
		{
			printf("Data transfer active. Abort it first with \"stop\".\n");
		}
		break;
	case CmdStop:
		if(tap_mode == TapIdle)
		{
			printf("No data transfer in progress.\n");
		}
		else if(tap_mode == TapSending)
		{
			if(trutusb_xferactive())
			{
				trutusb_xferclean();
				printf("Cancelled sending tap, wait until completion..\n");
				tap_mode = TapCancellingSending;
			}
			else
			{
				printf("Already finished?!\n");
			}
		}
		else
		{
			printf("Later...\n");
		}
		break;
	case CmdCrc:
		printf("Later...\n");
		break;
	default:
		printf("Invalid command\n");
	}
}

void printprompt()
{
	printf("> ");
	fflush(stdout);
}

int parse_opts(int argc, char* argv[])
{
	int arg;

	for(arg = 1; arg < argc; arg++)
	{
		if(argv[arg][0] == '-')
		{
			switch(argv[arg][1])
			{
			case 'c':
				opt_cmdnoquit = 1;
				break;
			case 'd':
				opt_debug++;
				break;
			case 'h':
				usage();
				break;
			default:
				fprintf(stderr, "Unknown option: %c\n", argv[arg][1]);
				usage();
				break;
			}
		}
		else
		{
			break;
		}
	}

	// Look for commands given on command line.
	if(arg < argc)
	{
		if(strcmp(argv[arg], "load") == 0)
			opt_cmd = CmdLoad;
		else if(strcmp(argv[arg], "dump") == 0)
			opt_cmd = CmdDump;
		else if(strcmp(argv[arg], "write") == 0)
			opt_cmd = CmdWrite;
		else if(strcmp(argv[arg], "save") == 0)
			opt_cmd = CmdSave;
		else
		{
			fprintf(stderr, "Unrecognized command: %s\n", argv[arg]);
			usage();
		}

		if(opt_cmd == CmdLoad || opt_cmd == CmdDump || opt_cmd == CmdWrite ||
		   opt_cmd == CmdSave)
		{
			opt_singlecmd = 1;
			arg++;
			if(arg < argc)
			{
				opt_cmdarg = argv[arg];
				arg++;
				if(arg < argc)
				{
					fprintf(stderr, "Too many commands given.\n");
					usage();
				}
			}
			else
			{
				fprintf(stderr, "Command requires a file argument.\n");
				usage();
			}
		}
	}

	return 0;
}

int main(int argc, char** argv)
{
	int ret;
	char cmd[256];
	char data[256];
	char events[2];
	usbw_waitables* waitables;

#ifdef WIN32
	init_win32();
#endif

	parse_opts(argc, argv);

	printf("bitcoach! Use \"help\" for help, \"quit\" to quit.\n");

	trutusb_init();
	usbw_debug(opt_debug);
	trutusb_scan();

	if(trutusb_isattached())
	{
		printf("TRUT connected! Get ready to rambo!\n");
	}
	else
	{
		printf("Found no TRUT :(. You must always connect TRUT! :(\n");
		return 0;
	}

	if(trutusb_claim())
	{
		printf("Can not claim TRUT :(\n");
		return 0;
	}

	if(trutusb_setmsg(true))
	{
		printf("Failed to enable messages\n");
	}

	quit = 0;

	printprompt();

	while(!quit)
	{
		if(trutusb_hasmsg())
		{
			ret = trutusb_getmsg((uint8_t*) data, sizeof(data));
			if(ret > 0)
				print_msg(data, ret);
			else
				printf("Error getting TRUT message\n");
		}

		if(trutusb_xferactive() && trutusb_xfercomplete())
		{
			trutusb_xferclean();

			if(tap_mode == TapIdle)
			{
				printf("Cleaning transfer in idle mode (should not happen).\n");
			}
			else if(tap_mode == TapSending)
			{
				printf("Finished sending tap.\n");
				free_buffer(data_buffer);
				data_buffer = 0;
				tap_mode = TapIdle;
			}
			else if(tap_mode == TapCancellingSending)
			{
				printf("Tap send cancelled.\n");
				free_buffer(data_buffer);
				data_buffer = 0;
				tap_mode = TapIdle;
			}
			else if(tap_mode == TapReceiving)
			{
				printf("Finished receiving tap.\n");
				free_buffer(data_buffer);
				data_buffer = 0;
				tap_mode = TapIdle;
			}
			else
			{
				printf("Undefined mode while cleaning transfer.\n");
				tap_mode = TapIdle;
			}
			printprompt();
		}

		if(opt_singlecmd > 0 && tap_mode == TapIdle)
		{
			switch(opt_cmd)
			{
			case CmdLoad:
				start_tap_send(TRUT_MODE_LOAD, opt_cmdarg);
				break;
			case CmdDump:
				start_tap_receive(TRUT_MODE_DUMP, opt_cmdarg);
				break;
			case CmdWrite:
				start_tap_send(TRUT_MODE_WRITE, opt_cmdarg);
				break;
			case CmdSave:
				start_tap_send(TRUT_MODE_SAVE, opt_cmdarg);
				break;
			case CmdQuit:
			default:
				quit = 1;
			}

			if(tap_mode == TapIdle) // an error occurred
				quit = 1;

			if(opt_singlecmd == 2)
				opt_singlecmd = 0;
			else
				opt_cmd = CmdQuit;
		}

		waitables = trutusb_getwaitables();
		if(waitables == 0)
		{
			printf("Error getting list of USB waitables\n");
			break;
		}

		ret = wait_for_stuff(cmd, sizeof(cmd), waitables, events);

		if(ret < 0)
		{
			printf("wait_for_stuff error..\n");
		}

		if(events[0])
		{
			if(opt_debug >= 1)
				fprintf(stderr, "Woke on stdin...\n");

			// Woke on stdin
			process_cmd(cmd);
			printprompt();
		}

		if(events[1])
		{
			if(opt_debug >= 1)
				fprintf(stderr, "Woke on usb...\n");

			// Woke on usb
			trutusb_handleevents();
		}

		trutusb_freewaitables(waitables);
	}

	if(trutusb_setmsg(false))
		printf("Failed to disable messages\n");

	if(trutusb_isattached())
		trutusb_release();

	fprintf(stderr, "Good bye\n");

	return 0;
}

#if defined(__unix__) || defined(__APPLE__)
# define BCEXE "bitcoach"
#else
# define BCEXE "bitcoach.exe"
#endif

void usage()
{
	fprintf(stderr, "usage: " BCEXE " [options]\n");
	fprintf(stderr, "       " BCEXE " [options] [-c] load file.tap\n");
	fprintf(stderr, "       " BCEXE " [options] [-c] dump file.tap\n");
	fprintf(stderr, "       " BCEXE " [options] [-c] write file.tap\n");
	fprintf(stderr, "       " BCEXE " [options] [-c] save file.tap\n");
	fprintf(stderr, "Options:\n");
	fprintf(stderr, "  -c: Do not quit after command has been completed.\n");
	fprintf(stderr, "  -d: Increase usb debug level (can be used up to two times)\n");
	fprintf(stderr, "load/dump/write/save: Do the requested operation and quit.\n");
	exit(0);
}

#if defined(__unix__) || defined(__APPLE__)
static int wait_for_stuff(char* cmd, int cmdsize,
						  usbw_waitables* waitables, char* events)
{
	struct timeval timeout;
	fd_set readfd;
	fd_set writefd;
	int ret, i;
	int fd;
	int max_fd = 0;

	FD_ZERO(&readfd);
	FD_SET(0, &readfd);
	FD_ZERO(&writefd);

	events[0] = 0;
	events[1] = 0;

	for(i = 0; i < waitables->num_readobj; i++)
	{
		fd = waitables->readobj[i];
		if(fd > max_fd)
			max_fd = fd;

		FD_SET(fd, &readfd);
	}

	for(i = 0; i < waitables->num_writeobj; i++)
	{
		fd = waitables->writeobj[i];
		if(fd > max_fd)
			max_fd = fd;

		FD_SET(fd, &writefd);
	}

	if(opt_debug >= 2)
		fprintf(stderr, "Sleeping with %d readfd and %d writefd, ", waitables->num_readobj,
				waitables->num_writeobj);
	if(waitables->timeout_pending == 1)
	{
		if(opt_debug >= 2)
			fprintf(stderr, "timeout %d sec %d us\n", (int)waitables->timeout.tv_sec,
					(int)waitables->timeout.tv_usec);
		timeout = waitables->timeout;
		ret = select(max_fd + 1, &readfd, &writefd, NULL, &timeout);
	}
	else
	{
		if(opt_debug >= 2)
			fprintf(stderr, "no timeout\n");
		ret = select(max_fd + 1, &readfd, &writefd, NULL, NULL);
	}

	if(ret == -1)
	{
		perror("select");
		return 1;
	}
	else if(ret > 0)
	{
		if(FD_ISSET(0, &readfd))
		{
			if(!fgets(cmd, cmdsize, stdin))
				die("fgets: read error: %s\n", strerror(errno));
			events[0] = 1;
		}

		for(i = 0; i < waitables->num_readobj; i++)
			if(FD_ISSET(waitables->readobj[i], &readfd))
				events[1] = 1;

		for(i = 0; i < waitables->num_writeobj; i++)
			if(FD_ISSET(waitables->writeobj[i], &writefd))
				events[1] = 1;
	}

	if(events[0] == 0 && events[1] == 0)
		events[1] = 1;

	return 0;
}
#endif

#ifdef WIN32
#define ETIMEDOUT 116

static char cmd_buf[1024];
static HANDLE has_cmd_event;
static HANDLE cmd_done_event;
static HANDLE fgets_thread_handle;

static DWORD WINAPI fgets_thread(LPVOID param);

static void init_win32(void)
{
	has_cmd_event = CreateEvent(NULL, FALSE, FALSE, NULL);
	if(!has_cmd_event)
		die("CreateEvent failed (has_cmd_event). Error code: %d\n",
		    GetLastError());

	cmd_done_event = CreateEvent(NULL, FALSE, FALSE, NULL);
	if(!cmd_done_event)
		die("CreateEvent failed (cmd_done_event). Error code: %d\n",
		    GetLastError());

	fgets_thread_handle = CreateThread(NULL, 0, fgets_thread, NULL, 0, NULL);
	if(!fgets_thread_handle)
		die("CreateThread failed: %d\n", GetLastError());
}

static DWORD WINAPI fgets_thread(LPVOID param)
{
	while(true) {
		if(quit)
			break;

		fgets(cmd_buf, sizeof(cmd_buf), stdin);
		if(!SetEvent(has_cmd_event))
			die("SetEvent failed (has_cmd_event): %d\n",
			    GetLastError());

		if(WaitForSingleObject(cmd_done_event, INFINITE) != WAIT_OBJECT_0)
			die("WaitForSingleObject failed: %d\n", GetLastError());
	}

	return 0;
}

static void wait_for_stuff()
{
	HANDLE handles[1+NUM_ASYNCS];
	int num = 1, i;

//	printf("wait_for_stuff: data_tx/data_rx %d %d\n", data_tx_submit, data_rx_submit);
	handles[0] = has_cmd_event;
	if(data_tx_submit != -1 || data_rx_submit != -1) {
		for(i = 0; i < NUM_ASYNCS; i++) {
			if(data_async_context[i]) {
				handles[num] = usb_async_get_handle(data_async_context[i]);
				num++;
			}
		}
	}

	int ret = WaitForMultipleObjects(num, handles, FALSE, 10);
	// printf("WaitFor.. num: %d ret: %d\n", num, ret);

	if(ret < 0) {
		printf("WaitForMultipleObjects failed: %ld\n", GetLastError());
	}

	if(ret == WAIT_OBJECT_0) {
		process_cmd(cmd_buf);
		printprompt();
		if(!SetEvent(cmd_done_event))
			die("SetEvent failed (cmd_done_event): %d\n", GetLastError());
	}

	if((WAIT_OBJECT_0 < ret && ret < WAIT_OBJECT_0 + num) ||
	   (ret == WAIT_OBJECT_0 && num > 1)) {
		printf("WaitFor.. data %d\n", ret);
		// We cannot load and dump at the same time.
		assert(data_tx_submit == -1 || data_rx_submit == -1);
		data_submit();
		data_read();

/*
		int ret;
		ret = usb_async_is_done(data_async_context);

		if(ret < 0) {
			printf("usb_async_is_done failed: %s\n", strerror(errno));
		} else if(ret) {
			data_tx_submit = -1;
			printf("Loading finished.\n");
		}
*/
	}
}
#endif
