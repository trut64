#ifndef WAITABLE_H
#define WAITABLE_H

#if defined(__unix__) || defined(__APPLE__)
typedef int waitable;
#endif

#ifdef WIN32
typedef HANDLE waitable;
#endif

#endif
