/*
 * Copyright (C) 2007 Anton Blad
 * Copyright (C) 2007 Fredrik Kuivinen
 * Copyright (C) 2007 Jakob Ros�n
 *
 * This file is licensed under GPL v2.
 */

#include <stdio.h>
#include <string.h>
#include <usb.h>

#include "common.h"
#include "tap.h"

#define USE_ASYNC 1                 // Has priority over USE_LOW_LATENCY
#define USE_LOW_LATENCY 0
#define PROGRESS_BAR_SPEED 64		// 64 = Jakob (Slow),	1 = Fredrik (Fast)

#define NUM_ASYNCS 10

#if 0
// Atmel's code
#  define ENDPOINT 2
#  define INTERFACE 0
#  define ALTINTERFACE 0
#  define USE_INTERRUPT_WRITE 0
#else
// Anton's code
#  define USE_INTERRUPT_WRITE 1
#  define ENDPOINT 3
#  define INTERFACE 1

#  if USE_INTERRUPT_WRITE
#    define ALTINTERFACE 2
#  else
#    define ALTINTERFACE 0
#  endif
#endif

#ifdef WIN32
#  define ETIMEDOUT 116
#endif

#define VENDOR_ID             0x03EB // Atmel vendor ID = 03EBh
#define PRODUCT_ID            0x0000

struct usb_dev_handle* init_trut(int interface);

void dump_data(const char* data, int size)
{
	int i;
	for(i = 0; i < size; i++) {
		if(data[i])
			printf("%d '%c'\n", data[i], data[i]);
	}
}

#define TRUT_CMD_DATASTART        0x81

static void send_set_mode(struct usb_dev_handle* handle, int mode)
{
	uint8_t buffer[3];

	buffer[0] = 0x82;
	buffer[1] = 1;
	buffer[2] = mode;
	usb_bulk_write(handle, 2, buffer, 3, 100);
}


static void send_start(struct usb_dev_handle* handle, int len)
{
	char buffer[4];
	buffer[0] = TRUT_CMD_DATASTART;
	buffer[1] = 2;
	buffer[2] = len >> 8;
	buffer[3] = len & 0xff;
	usb_bulk_write(handle, 2, buffer, 4, 100);
}

int main(int argc, char **argv)
{
	struct usb_dev_handle *usb_handle;
	int ret;
	int retval = 0;

	usb_handle = init_trut(INTERFACE);
	if(!usb_handle) {
		fprintf(stderr, "Failed to initialize TRUT\n");
                return 1;
	}

	if (argc == 1) {
		fprintf(stderr, "Usage: %s <file>\n", argv[0]);
		retval = 1;
		goto exit;
	}

	ret = usb_set_altinterface(usb_handle, ALTINTERFACE);
	if(ret < 0)
		fprintf(stderr, "usb_set_altinterface failed: %s\n", usb_strerror());

	if (USE_INTERRUPT_WRITE) {
		printf("Using INTERRUPT transfers...\n");
	} else {
		printf("Using BULK transfers...\n");
	}

	const char* file = argv[1];
	FILE* f = fopen(file, "rb");
	if(!f) {
		fprintf(stderr, "Unable to open '%s': %s\n", file, strerror(errno));
		retval = 1;
		goto exit;
	}

	printf("Loading %s\n",file);

	struct tape_data* tap = load_tap(f);
	struct buffer* intern = jakob_internal(tap);
//	struct buffer* intern = fredrik_internal(tap);

	send_set_mode(usb_handle, 0x80);
	send_start(usb_handle, intern->length/64 + 1);

	printf("TAP-file loaded successfully. Number of pulses: %d\n", tap->length);

	printf("Initial pulse lengths: ");
	for(int i = 0; i < 20 && i < tap->length; i++)
		printf("%d ", tap->data[i]);
	putchar('\n');

	if(USE_ASYNC) {
		int i;
		int data_sent;
		static void* data_async_context[NUM_ASYNCS];

		for(i = 0; i < NUM_ASYNCS; i++)
			data_async_context[i] = NULL;
		data_sent = 0;
		while(1)
		{
			int any_async = 0;
			for(i = 0; i < NUM_ASYNCS; i++) {
				if(data_async_context[i])
					any_async = 1;
			}
			if(data_sent >= intern->length && !any_async)
				break;

			for(i = 0; i < NUM_ASYNCS; i++) {
				if(data_async_context[i]) {
					int ret = usb_reap_async_nocancel(data_async_context[i], 0);
					if(ret < 0 && ret != -ETIMEDOUT) {
						printf("usb_reap_async_nocancel failed: %s\n", usb_strerror());
					} else if(ret >= 0) {
						printf("Reaping %d\n", i);
						usb_free_async(data_async_context[i]);
						data_async_context[i] = NULL;
					}
				}
			}

			for(i = 0; i < NUM_ASYNCS; i++) {
				if(data_sent >= intern->length)
					continue;

				if(data_async_context[i])
					continue;

				if(usb_interrupt_setup_async(usb_handle, &data_async_context[i], ENDPOINT) < 0) {
					printf("Failed to initialize async data transfer: %s\n", usb_strerror());
					break;
				}

				int request = intern->length - data_sent;
				if(request > usb_max_read_write())
					request = usb_max_read_write();

				printf("Submitting %d request: %d\n", i, request);
				int ret = usb_submit_async(data_async_context[i], (char*) intern->data+data_sent,
										   request);
				if(ret < 0) {
					printf("usb_submit_async failed: %s\n", usb_strerror());
				} else {
					data_sent += request;
				}
			}


		}
	}
    else if(USE_LOW_LATENCY) {
		if(USE_INTERRUPT_WRITE) {
			ret = usb_interrupt_write_low_latency(usb_handle, ENDPOINT,
							      (char*) intern->data,
							      intern->length, 2000000000);
		} else {
			ret = usb_bulk_write_low_latency(usb_handle, ENDPOINT,
							 (char*) intern->data,
							 intern->length, 2000000000);
		}

		if(ret < 0) {
			printf("error: %s\n", strerror(errno));
		}
	} else {
		char data[1*64];
		int data_index = 0;
		while (data_index < intern->length) {
			//printf("%d ",data_index);
			for (int i = 0; i < sizeof(data); i++) {
				if (data_index < intern->length) {
					data[i] = intern->data[data_index];
					data_index++;
				} else {
					data[i] = 0xff; // To fill the last packet
				}
			}
			do {
				if(USE_INTERRUPT_WRITE) {
					ret = usb_interrupt_write(usb_handle, ENDPOINT, data,
								  sizeof(data), 2000000000);
				} else {
					ret = usb_bulk_write(usb_handle, ENDPOINT, data,
							     sizeof(data), 2000000000);
				}

				if (ret != sizeof(data)) {
					printf("Resending package... Return code: %d\n",ret);
				} else {
					if (data_index%(sizeof(data)*PROGRESS_BAR_SPEED)==0) putchar('.');
					fflush(stdout);
				}
			} while (ret != sizeof(data));
		}
	}

	printf("End of tape! What follows next is nonsense.\n");

  exit:
	if(usb_handle)
		usb_close(usb_handle);
	return retval;
}

static struct usb_device *device_init(void)
{
	struct usb_bus *usb_bus;
	struct usb_device *dev;
	usb_init();
	usb_find_busses();
	usb_find_devices();
	for (usb_bus = usb_busses; usb_bus; usb_bus = usb_bus->next)
	{
		for (dev = usb_bus->devices; dev; dev = dev->next)
		{
			if ((dev->descriptor.idVendor  == VENDOR_ID) &&
				(dev->descriptor.idProduct == PRODUCT_ID))
				return dev;
		}
	}

	return NULL;
}

struct usb_dev_handle* init_trut(int interface)
{
	struct usb_device *usb_dev;
	struct usb_dev_handle *usb_handle = NULL;

	usb_dev = device_init();
	if (usb_dev == NULL) {
		fprintf(stderr, "Device not found\n");
		goto err;
	}

	usb_handle = usb_open(usb_dev);
	if (usb_handle == NULL) {
		fprintf(stderr, "Not able to open the USB device. libusb: %s\n",
				usb_strerror());
		goto err;
	}

	usb_set_configuration(usb_handle, 1);

	int ret = usb_claim_interface(usb_handle, interface);
	if (ret < 0) {
		fprintf(stderr, "Failed to claim interface: %s. libusb: %s\n",
				strerror(-ret), usb_strerror());
		goto err;
	}

	return usb_handle;

err:
	if(usb_handle)
		usb_close(usb_handle);

	return NULL;
}
