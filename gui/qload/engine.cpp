/*
 * Copyright (C) 2008 Anton Blad
 * Copyright (C) 2008 Fredrik Kuivinen
 * Copyright (C) 2008 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#include <iostream>

#include "engine.h"

extern "C" {
#include "commands.h"
#include "common.h"
#include "tap.h"
#include "trutusb.h"
#include "trutproto.h"
}

// -d option: Debug level.
static int opt_debug = 0;

enum TapTransferMode { TapIdle, TapSending, TapReceiving, TapCancellingSending };

// Specifies if a command was given on the command line. opt_singlecmd
// is 1 if a command was given on the command line.
static int opt_singlecmd = 0;
static enum CmdCode opt_cmd = CmdUnknown; //Jakob
static const char* opt_cmdarg = 0;

static enum TapTransferMode tap_mode = TapIdle;
static struct buffer* data_buffer;
static bool crc_query_from_user;

static int quit = 0;

#include "bitcoach_routines.h"

static usbw_waitables* waitables;

/*
This function is, for now, not used. It was supposed to be called immediately
after the print_msg() function, to perform different events after printing them.
(For instance quitting the program when a TRUT_MSG_LOAD_FINISHED was
encounterd). Now this is done in the process() function.
void Engine::examine_msg(const char* data, int size)
{
	int arglen = size - 1;
	const char* args = data + 1;	

	switch(data[0])
	{
	case TRUT_MSG_LOADFINISHED:
		quit=1;
		break;
	default:
		// For now, do nothing
		break;
	}
}
*/

void Engine::process() {
	int ret;
//	char cmd[256];
	char data[256];
//	char events[2];

		if(trutusb_hasmsg())
		{
			ret = trutusb_getmsg((uint8_t*) data, sizeof(data));
			if(ret > 0) {
				print_msg(data, ret);
//				examine_msg(data, ret);	// Added by Jakob for qload
			}
			else
				printf("Error getting TRUT message\n");
		}

		if(trutusb_xferactive() && trutusb_xfercomplete())
		{
			trutusb_xferclean();

			if(tap_mode == TapIdle)
			{
				printf("Cleaning transfer in idle mode (should not happen).\n");
			}
			else if(tap_mode == TapSending)
			{
				printf("Finished sending tap.\n");
				free_buffer(data_buffer);
				data_buffer = 0;
				tap_mode = TapIdle;
				quit=1;		// Added by Jakob. Quit when finished!
			}
			else if(tap_mode == TapCancellingSending)
			{
				printf("Tap send cancelled.\n");
				free_buffer(data_buffer);
				data_buffer = 0;
				tap_mode = TapIdle;
			}
			else if(tap_mode == TapReceiving)
			{
				printf("Finished receiving tap.\n");
				free_buffer(data_buffer);
				data_buffer = 0;
				tap_mode = TapIdle;
			}
			else
			{
				printf("Undefined mode while cleaning transfer.\n");
				tap_mode = TapIdle;
			}
			printprompt();
		}

		if(opt_singlecmd > 0 && tap_mode == TapIdle)
		{
			switch(opt_cmd)
			{
			case CmdLoad:
				start_tap_send(TRUT_MODE_LOAD, opt_cmdarg);
				break;
			case CmdDump:
				start_tap_receive(TRUT_MODE_DUMP, opt_cmdarg);
				break;
			case CmdWrite:
				start_tap_send(TRUT_MODE_WRITE, opt_cmdarg);
				break;
			case CmdSave:
				start_tap_send(TRUT_MODE_SAVE, opt_cmdarg);
				break;
			case CmdQuit:
			default:
				quit = 1;
			}

			if(tap_mode == TapIdle) // an error occurred
				quit = 1;

			if(opt_singlecmd == 2)
				opt_singlecmd = 0;
			else
				opt_cmd = CmdQuit;
		}

		waitables = trutusb_getwaitables();
		if(waitables == 0)
		{
			printf("Error getting list of USB waitables\n");
//			break;
		}


//	ret = wait_for_stuff(cmd, sizeof(cmd), waitables, events);

	// waitables is now global
	// The other ones are not needed since we are not using stdin anymore

	// Before waiting for stuff, let's see if it's time to quit
	if (quit!=0) emit application_quit_signal();

	// Can you feel it?
	wait_for_stuff();

/*
		trutusb_handleevents();
		trutusb_freewaitables(waitables);
		...are handled by the wakeup(int) slot
*/

    fflush(stdout);
}

void Engine::wait_for_stuff() {

	if(opt_debug >= 2)
		fprintf(stderr, "Sleeping with %d readfd and %d writefd...\n", waitables->num_readobj,
				waitables->num_writeobj);

    // We do this the Bitcoach/select way, so all socket notifiers are cleared each time an event occurs.
    // This is not nice when using QSocketNotifiers. Instead, we should keep track of them and only add
    // new / delete expired notifiers. I suppose :)

    // Substitute for FD_ZERO(&readfd);
    for (int i=0; i<notify_read.size(); i++) {
            notify_read[i]->setEnabled(false);	// Should be redundant
            delete notify_read[i];
    }
    notify_read.clear();

    // No substitute for FD_SET(0, &readfd);
    // Since we want to monitor stdin

    // Substitute for FD_ZERO(&writefd);
    for (int i=0; i<notify_write.size(); i++) {
            notify_write[i]->setEnabled(false);	// Should be redundant
            delete notify_write[i];
    }
    notify_write.clear();

	struct timeval timeout;
	int fd;
    int i;

    for(i = 0; i < waitables->num_readobj; i++)
    {
        fd = waitables->readobj[i];
        QSocketNotifier* qsn=new QSocketNotifier(fd,QSocketNotifier::Read);
        QObject::connect(qsn, SIGNAL(activated(int)), this, SLOT(usb_event(int)));
        qsn->setEnabled(true);
        notify_read.push_back(qsn);
    }

	for(i = 0; i < waitables->num_writeobj; i++)
	{
		fd = waitables->writeobj[i];
		QSocketNotifier* qsn=new QSocketNotifier(fd,QSocketNotifier::Write);
		QObject::connect(qsn, SIGNAL(activated(int)), this, SLOT(usb_event(int)));
		qsn->setEnabled(true);
		notify_write.push_back(qsn);
	}

	if(waitables->timeout_pending == 1)
	{
		if(opt_debug >= 2)
			fprintf(stderr, "timeout %d sec %d us\n", (int)waitables->timeout.tv_sec,
					(int)waitables->timeout.tv_usec);
//		ret = select(max_fd + 1, &readfd, &writefd, NULL, &timeout);
        timeout = waitables->timeout;
        timer.start((int)timeout.tv_sec*1000+ceil((double)timeout.tv_usec/1000.0));
	}
}

int Engine::init(char* filename) {
	trutusb_init();
	usbw_debug(opt_debug);
	trutusb_scan();

	if(trutusb_isattached())
	{
		printf("TRUT connected! Get ready to rambo!\n");
	}
	else
	{
		printf("Found no TRUT :(. You must always connect TRUT! :(\n");
		return 0;
	}

	if(trutusb_claim())
	{
		printf("Can not claim TRUT :(\n");
		return 0;
	}

	if(trutusb_setmsg(true))
	{
		printf("Failed to enable messages\n");
	}

	opt_cmd=CmdLoad;
//	opt_cmdarg="/home/jakob/_TAP/rambo2.tap";
	opt_cmdarg=filename;
	opt_singlecmd=1;	// A command was supplied via the command line

	init_timeout_timer();

	return 0;
}

void Engine::init_timeout_timer() {
	timer.setSingleShot(true);	// We don't want a periodic timer
	connect(&timer, SIGNAL(timeout()), this, SLOT(timeout_event()));
}

// Called after an usb_event(int) or a timeout_event()
// This is the code that succeeds wait_for_stuff()
void Engine::wakeup() {
	timer.stop();	// If an event was catched before a timeout, disable the timer
	trutusb_freewaitables(waitables);
	process();
}

// Slot called by QSocketNotifier::activate(int)
void Engine::usb_event(int fid) {
	std::cout << "Processing USB event from descriptor " << fid << std::endl;
	trutusb_handleevents();
	wakeup();
}

// Slot called by QTimer::timeout()
void Engine::timeout_event() {
    std::cout << "Timeout timeout occured!" << std::endl;
    assert(!timer.isActive());
    wakeup();
}

void Engine::start() {
	process();
}

void Engine::shut_down() {
	if(trutusb_setmsg(false))
		printf("Failed to disable messages\n");

	if(trutusb_isattached())
		trutusb_release();

	fprintf(stderr, "Good bye\n");
}

