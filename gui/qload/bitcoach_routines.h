/*
 * Copyright (C) 2008 Anton Blad
 * Copyright (C) 2008 Fredrik Kuivinen
 * Copyright (C) 2008 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

/*
Contains the following functions, taken directly from bitcoach.c:

	void printprompt()
	void dump_hex(const char* data, int size)
	static uint16_t crc16_update(uint16_t crc, uint8_t a)
	static uint16_t compute_crc(uint8_t* buf, int len)
	void msg_error(const char* data, int size)
	void print_msg(const char* data, int size)
	void start_tap_send(int mode, const char* file)
	void start_tap_receive(int mode, const char* file)
*/

void printprompt()
{
	printf("> ");
	fflush(stdout);
}

void dump_hex(const char* data, int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		printf("%02hhX ", data[i]);
	}
	fflush(stdout);
}

// Copied from the avr-libc documentation
static uint16_t crc16_update(uint16_t crc, uint8_t a)
{
	int i;

	crc ^= a;
	for (i = 0; i < 8; ++i)
	{
		if (crc & 1)
			crc = (crc >> 1) ^ 0xA001;
		else
			crc = (crc >> 1);
	}

	return crc;
}

static uint16_t compute_crc(uint8_t* buf, int len)
{
	uint16_t crc = 0xffff;
	int i;

	for(i = 0; i < len; i++) {
		crc = crc16_update(crc, buf[i]);
	}

	return crc;
}

void msg_error(const char* data, int size)
{
	int i;
	printf("Invalid message received. Printing verbatim:\n");
	for(i = 0; i < size; i++)
		printf("%02hhX ", data[i]);
	putchar('\n');
}

void print_msg(const char* data, int size)
{
	int arglen = size - 1;
	const char* args = data + 1;
	int i = 0;
	bool print_prompt = true;

	switch(data[0])
	{
	case TRUT_MSG_STR:
		for(i = 1; i < arglen; i++)
			putchar(args[i]);

		if(arglen == 0)
			fputs("Got zero length TRUT_MSG_STR", stdout);
		putchar('\n');
		break;
	case TRUT_MSG_HEX:
		for(i = 0; i < arglen; i++)
			printf("%02hhX ", args[i]);

		if(arglen == 0)
			fputs("Got zero length TRUT_MSG_HEX", stdout);
		putchar('\n');
		break;
	case TRUT_MSG_PONG:
		printf("Pong!\n");
		break;
	case TRUT_MSG_INVALIDCMD:
		printf("TRUT says invalid command.\n");
		break;
	case TRUT_MSG_PROTOERROR:
		printf("Protocol error\n");
		break;
	case TRUT_MSG_MOTOR:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		switch(args[0])
		{
		case TRUT_MSG_MOTOR_OFF:
			printf("Motor is off\n");
			break;
		case TRUT_MSG_MOTOR_ON:
			printf("Motor is on\n");
			break;
		default:
			printf("Motor in invalid state\n");
			break;
		}
		break;
	case TRUT_MSG_STACKUSAGE:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		printf("Maximum stack usage: %d bytes\n", args[0]);
		break;
	case TRUT_MSG_BUFFERUSAGE:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		printf("Minimum/maximum buffer usage: %d bytes\n", args[0]);
		break;
	case TRUT_MSG_COMMERROR:
		if(arglen != 1)
		{
			msg_error(data, size);
			break;
		}
		switch(args[0])
		{
		case TRUT_MSG_COMMERROR_BUFFERUF:
			printf("TRUT: Buffer underflow!\n");
			break;
		case TRUT_MSG_COMMERROR_BUFFEROF:
			printf("TRUT: Buffer overflow!\n");
			break;
		case TRUT_MSG_COMMERROR_READDATA:
			printf("TRUT: Error reading received usb data to buffer\n");
			break;
		default:
			printf("TRUT: Unknown communication error.\n");
		}
		break;
	case TRUT_MSG_ACCEPTINGDATA:
		printf("TRUT is now accepting data.\n");
		break;
	case TRUT_MSG_CRC:
	{
		if(arglen != 6)
		{
			msg_error(data, size);
			break;
		}
		uint8_t args0 = args[0];
		uint8_t args1 = args[1];
		uint8_t args2 = args[2];
		uint8_t args3 = args[3];
		uint32_t crc_len = (args0 << 24) |  (args1 << 16) |  (args2 << 8) | args3;
		crc_len *= 2; // the received length is in words

		args0 = args[4];
		args1 = args[5];
		uint16_t received_crc = (args0 << 8) | args1;

		if(!data_buffer) {
			printf("Got CRC %x. Cannot compute expected CRC as no "
			       "TAP-file is loaded.\n", received_crc);
			break;
		}

		uint16_t computed_crc = compute_crc(data_buffer->data, crc_len);
		if(computed_crc != received_crc) {
			printf("CRC FAILURE: Got CRC %x expected %x (length in bytes: %d)\n",
			       received_crc, computed_crc, crc_len);
		} else if(crc_query_from_user) {
			printf("Got expected CRC %x (length in bytes: %d)\n",
			       computed_crc, crc_len);
			crc_query_from_user = false;
		} else {
			print_prompt = false;
		}
	}
	break;
	case TRUT_MSG_TIMERERROR:
		printf("TRUT: Timer error!\n");
		break;
	case TRUT_MSG_DUMPSTARTED:
		printf("TRUT: Dump started.\n");
		break;
	case TRUT_MSG_DUMPFINISHED:
		printf("TRUT: Dump finished.\n");
		break;
	case TRUT_MSG_LOADSTARTED:
		printf("TRUT: Load started.\n");
		break;
	case TRUT_MSG_LOADFINISHED:
		printf("TRUT: Load finished.\n");
		break;
	default:
		printf("Invalid message received. Printing verbatim:\n");
		for(i = 0; i < size; i++)
			printf("%02hhX ", data[i]);
		putchar('\n');
	}

	if(print_prompt) {
		putchar('\n');
		printprompt();
	}
}

void start_tap_send(int mode, const char* file)
{
	FILE* f;
	struct tape_data* tap;

	f = fopen(file, "rb");
	if(!f) {
		printf("Unable to open '%s': %s\n", file, strerror(errno));
		return;
	}

	printf("Loading '%s'\n", file);
	tap = load_tap(f);
	fclose(f);

	data_buffer = jakob_internal(tap);
	free_tap(tap);
	printf("Buffer length: %d\n", data_buffer->length);

	if(trutusb_xferinit(mode, data_buffer->data, data_buffer->length) == 0)
	{
		printf("Initialized data transfer of %d bytes\n", data_buffer->length);
		tap_mode = TapSending;
	}
	else
	{
		printf("Failed to initialize data transfer\n");
		free_buffer(data_buffer);
	}
}

void start_tap_receive(int mode, const char* file)
{
	file=NULL;	// Added by Jakob to supress warning messages at compile time
	mode=NULL;	// Added by Jakob to supress warning messages at compile time
	printf("Later...\n");
}

