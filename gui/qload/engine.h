/*
 * Copyright (C) 2008 Anton Blad
 * Copyright (C) 2008 Fredrik Kuivinen
 * Copyright (C) 2008 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QSocketNotifier>
#include <QTimer>
#include <QVector>

class Engine : public QObject {
	Q_OBJECT;

private:
	QVector<QSocketNotifier*> notify_read;
	QVector<QSocketNotifier*> notify_write;
	QTimer timer;
	void wait_for_stuff();
	void process();
	void wakeup();
	void init_timeout_timer();
//	void examine_msg(const char* data, int size);

public:
	int init(char* filename);
	void start();
	void shut_down();

signals:
	void application_quit_signal();

public slots:
	void usb_event(int fid);
	void timeout_event();
};

#endif

