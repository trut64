/*
 * Copyright (C) 2008 Anton Blad
 * Copyright (C) 2008 Fredrik Kuivinen
 * Copyright (C) 2008 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#include <QCoreApplication>
#include <iostream>

#include "engine.h"


Engine engine;



int main(int argc, char *argv[])
{

	QCoreApplication app(argc, argv);
	QObject::connect(&engine, SIGNAL(application_quit_signal()), &app, SLOT(quit()));

	if (argc!=2) {
		std::cout << "Usage: " << argv[0] << " <filename>" << std::endl;
		return 0;
	}

	engine.init(argv[1]);

	engine.start();

	int ret=app.exec();

	std::cout << std::endl << "Shutting down..." << std::endl;
	engine.shut_down();

	return ret;
}

