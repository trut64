/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "engine.h"
#include <QInputDialog>
#include <QDir>
#include <QFileDialog>
#include <ziparchive.h>
#include <vector>
#include <QMessageBox>

Engine engine;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    engine.set_gui(ui);
    engine.start_refresh_timer();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_initButton_clicked()
{
    engine.init();
    fflush(stdout);
}


void MainWindow::on_loadButton_clicked()
{
   QString filename = QFileDialog::getOpenFileName(this,"Open tape image", "", "(*.tap *.zip)");
   engine.load(filename);
}


void MainWindow::on_playButton_clicked()

{
    engine.play();
}


void MainWindow::on_stopButton_clicked()
{
    engine.stop();
    fflush(stdout);
}


void MainWindow::on_fileBox_currentIndexChanged(int index)
{
    engine.change_zip_item();
}

void MainWindow::on_setButton_clicked()
{
    bool ok;
    int i;
    do {
        i = QInputDialog::getInt(this, "Set counter", "value", 0, 0, 100000, 1, &ok);
    } while (!ok);
    engine.set_counter(i);
}

void MainWindow::on_memButton_clicked()
{
    ui->lcdMem->display(ui->lcdNumber->value());
}

void MainWindow::on_remButton_clicked()
{
    engine.set_counter(ui->lcdMem->value());
}

void MainWindow::on_saveButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Saving is not yet implemented.");
    msgBox.exec();
}

void MainWindow::on_dumpButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Dumping is not yet implemented.");
    msgBox.exec();
}
