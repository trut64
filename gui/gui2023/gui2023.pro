QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    elapsedtimer.cpp \
    engine.cpp \
    main.cpp \
    mainwindow.cpp \
    ziparchive.cpp

HEADERS += \
    elapsedtimer.h \
    engine.h \
    mainwindow.h \
    ziparchive.h

FORMS += \
    mainwindow.ui

# ***** Added by Jakob
INCLUDEPATH += . ../../include ../../host
INCLUDEPATH += ../qload # bitcoach_routines.h
include(config.pri)
LIBS += -lm -lusb-1.0 -lminizip
# Choose one of the following:
# 1. Compile source files with g++ and put them in .
#SOURCES += commands.c common.c tap.c trutusb.c usbw_libusb1.c
#VPATH += . ../../host
# 2. Use precomplied (with gcc) objects in host dir
LIBS += ../../host/commands.o ../../host/common.o ../../host/tap.o ../../host/trutusb.o ../../host/usbw_libusb1.o
# ****** End of Jakobs additions

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
