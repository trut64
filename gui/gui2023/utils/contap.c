/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

// Just a simple tool used for testing the TRUT64 GUI

#include <stdio.h>
#include <stdlib.h>
#include "tap.h"

int main(int argc, char** argv)
{
	printf("Contap. Concatenates two TAP files and saves the result.\n");
	if (argc < 4)
	{
			printf("Usage: contap <source1> <source2> <destination> [<padding>]\n");
			return -1;
	}

	char* source1_name=argv[1];
	char* source2_name=argv[2];
	char* destination_name=argv[3];
	int padding=0;
	if (argc==5)
	{
		sscanf(argv[4], "%d", &padding);
	}
	printf("Padding %d seconds between tap files\n", padding);

	struct tape_data* tap1;
	struct tape_data* tap2;
	tap1=load_tap_file(source1_name);
	tap2=load_tap_file(source2_name);
	
	struct tape_data* result;
	result=malloc(sizeof(struct tape_data));
	result->length=tap1->length+tap2->length+padding;	
	result->data=(int*)malloc(result->length*sizeof(int));
	int pos=0;
	int i;
	for (i=0; i<tap1->length; i++)
	{
		result->data[pos++]=tap1->data[i];
	}
	for (i=0; i<padding; i++)
	{
		result->data[pos++]=985248;
	}
	for (i=0; i<tap2->length; i++)
	{
		result->data[pos++]=tap2->data[i];
	}
	
	free_tap(tap1);
	free_tap(tap2);

  FILE* f;
  f = fopen(destination_name, "rb");
  if(f) {
      printf("File '%s' already exist! Please delete it first.\n", destination_name);
      return -1;
  }
  fclose(f);
  f = fopen(destination_name, "wb");
	save_tap(f, result);
	free_tap(result);
	printf("All done!\n");

	return 0;
}
