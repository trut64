/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

// Just a simple tool used for testing the TRUT64 GUI

#include <stdio.h>
#include "tap.h"

int main(int argc, char** argv)
{
	if (argc <= 1)
	{
			printf("Please specify filename\n");
			return -1;
	}
	
	char* filename=argv[1];
	struct tape_data* tap;
	tap=load_tap_file(filename);
	free(tap);

	return 0;
}
