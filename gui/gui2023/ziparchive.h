/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#ifndef ZIPARCHIVE_H
#define ZIPARCHIVE_H

#include <vector>
#include <string>

class ZipArchive
{
public:
    ZipArchive(std::string zipname);
    struct zip_buffer {
        uint8_t* data;
        unsigned long length;
    };
    std::string filename;
    std::vector<std::string> getFilenames(const std::string suffix="");
    bool isZipFile();
    bool extractItem(const std::string itemName, struct zip_buffer* zipbuf);

private:
    const int MAX_FILENAME_LENGTH=1024;
    bool strEndsWith(const std::string &str, const std::string &suffix);
    std::string tolower(std::string str);
};

#endif // ZIPARCHIVE_H
