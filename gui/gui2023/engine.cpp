/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#include <iostream>
#include <fstream>

#include "engine.h"
#include "ziparchive.h"

extern "C" {
#include "commands.h"
#include "common.h"
#include "tap.h"
#include "trutusb.h"
#include "trutproto.h"
}

// -d option: Debug level.
static int opt_debug = 0;

enum TapTransferMode { TapIdle, TapSending, TapReceiving, TapCancellingSending };

// Specifies if a command was given on the command line. opt_singlecmd
// is 1 if a command was given on the command line.
static int opt_singlecmd = 0;
static enum CmdCode opt_cmd = CmdUnknown; //Jakob
static const char* opt_cmdarg = 0;

static enum TapTransferMode tap_mode = TapIdle;
static struct buffer* data_buffer = nullptr;
static bool crc_query_from_user;

static int quit = 0;

#include "bitcoach_routines.h"

static usbw_waitables* waitables;

void Engine::examine_msg(const char* data, int size)
{
	int arglen = size - 1;
	const char* args = data + 1;	

	switch(data[0])
    {
    case TRUT_MSG_MOTOR:
        if(arglen != 1)
        {
            set_status("USB message error (motor), check console");
            msg_error(data, size);
            break;
        }
        switch(args[0])
        {
        case TRUT_MSG_MOTOR_OFF:
            elapsed_timer.pause();
            refresh_counter();
            set_status("Motor is off");
            break;
        case TRUT_MSG_MOTOR_ON:
            set_status("Motor is on");
            elapsed_timer.start();
            break;
        default:
            set_status("Motor in invalid state");
            break;
        }
        break;
    case TRUT_MSG_LOADFINISHED:
        fprintf(stderr,"Load finished in %llu msec\n", elapsed_timer.getTime());
        set_status("Load finished");
		break;
	default:
		// For now, do nothing
		break;
	}
}


Engine::Engine() {
    active_filename="";
}

void Engine::process() {
	int ret;
//	char cmd[256];
	char data[256];
//	char events[2];

		if(trutusb_hasmsg())
		{
			ret = trutusb_getmsg((uint8_t*) data, sizeof(data));
			if(ret > 0) {
				print_msg(data, ret);
                examine_msg(data, ret);	// Added by Jakob for qload
			}
			else
				printf("Error getting TRUT message\n");
		}

		if(trutusb_xferactive() && trutusb_xfercomplete())
		{
			trutusb_xferclean();

			if(tap_mode == TapIdle)
			{
				printf("Cleaning transfer in idle mode (should not happen).\n");
			}
			else if(tap_mode == TapSending)
			{
				printf("Finished sending tap.\n");
				free_buffer(data_buffer);
				data_buffer = 0;
				tap_mode = TapIdle;
				quit=1;		// Added by Jakob. Quit when finished!
			}
			else if(tap_mode == TapReceiving)
			{
				printf("Finished receiving tap.\n");
				free_buffer(data_buffer);
				data_buffer = 0;
				tap_mode = TapIdle;
			}
			else
			{
				printf("Undefined mode while cleaning transfer.\n");
				tap_mode = TapIdle;
			}
			printprompt();
		}

		if(opt_singlecmd > 0 && tap_mode == TapIdle)
		{
			switch(opt_cmd)
			{
			case CmdLoad:
				start_tap_send(TRUT_MODE_LOAD, opt_cmdarg);
				break;
			case CmdDump:
				start_tap_receive(TRUT_MODE_DUMP, opt_cmdarg);
				break;
			case CmdWrite:
				start_tap_send(TRUT_MODE_WRITE, opt_cmdarg);
				break;
			case CmdSave:
				start_tap_send(TRUT_MODE_SAVE, opt_cmdarg);
				break;
			case CmdQuit:
			default:
				quit = 1;
			}

			if(tap_mode == TapIdle) // an error occurred
				quit = 1;

			if(opt_singlecmd == 2)
				opt_singlecmd = 0;
			else
				opt_cmd = CmdQuit;
		}

        if(tap_mode == TapCancellingSending)
        {
            printf("Tap send cancelled.\n");
            set_status("Tape stopped");
            // free_buffer(data_buffer);
            // data_buffer = 0;
            tap_mode = TapIdle;
        }

		waitables = trutusb_getwaitables();
		if(waitables == 0)
		{
			printf("Error getting list of USB waitables\n");
//			break;
		}


//	ret = wait_for_stuff(cmd, sizeof(cmd), waitables, events);

	// waitables is now global
	// The other ones are not needed since we are not using stdin anymore

	// Before waiting for stuff, let's see if it's time to quit
	if (quit!=0) emit application_quit_signal();

	// Can you feel it?
	wait_for_stuff();

/*
		trutusb_handleevents();
		trutusb_freewaitables(waitables);
		...are handled by the wakeup(int) slot
*/
    fflush(stdout);
}

void Engine::set_gui(Ui::MainWindow* my_ui) {
    ui=my_ui;
    if (GUI_GHOST_BUTTONS) {
        gui_enable_load_panel();
        gui_disable_play_panel();
    }
}

void Engine::wait_for_stuff() {

	if(opt_debug >= 2)
		fprintf(stderr, "Sleeping with %d readfd and %d writefd...\n", waitables->num_readobj,
				waitables->num_writeobj);

    // We do this the Bitcoach/select way, so all socket notifiers are cleared each time an event occurs.
    // This is not nice when using QSocketNotifiers. Instead, we should keep track of them and only add
    // new / delete expired notifiers. I suppose :)

    // Substitute for FD_ZERO(&readfd);
    for (int i=0; i<notify_read.size(); i++) {
            notify_read[i]->setEnabled(false);	// Should be redundant
            delete notify_read[i];
    }
    notify_read.clear();

    // No substitute for FD_SET(0, &readfd);
    // Since we want to monitor stdin

    // Substitute for FD_ZERO(&writefd);
    for (int i=0; i<notify_write.size(); i++) {
            notify_write[i]->setEnabled(false);	// Should be redundant
            delete notify_write[i];
    }
    notify_write.clear();

	struct timeval timeout;
	int fd;
    int i;

    for(i = 0; i < waitables->num_readobj; i++)
    {
        fd = waitables->readobj[i];
        QSocketNotifier* qsn=new QSocketNotifier(fd,QSocketNotifier::Read);
        QObject::connect(qsn, SIGNAL(activated(int)), this, SLOT(usb_event(int)));
        qsn->setEnabled(true);
        notify_read.push_back(qsn);
    }

	for(i = 0; i < waitables->num_writeobj; i++)
	{
		fd = waitables->writeobj[i];
		QSocketNotifier* qsn=new QSocketNotifier(fd,QSocketNotifier::Write);
		QObject::connect(qsn, SIGNAL(activated(int)), this, SLOT(usb_event(int)));
		qsn->setEnabled(true);
		notify_write.push_back(qsn);
	}

	if(waitables->timeout_pending == 1)
	{
		if(opt_debug >= 2)
			fprintf(stderr, "timeout %d sec %d us\n", (int)waitables->timeout.tv_sec,
					(int)waitables->timeout.tv_usec);
//		ret = select(max_fd + 1, &readfd, &writefd, NULL, &timeout);
        timeout = waitables->timeout;
        timeout_timer.start((int)timeout.tv_sec*1000+ceil((double)timeout.tv_usec/1000.0));
	}
}

int Engine::init() {
	trutusb_init();
	usbw_debug(opt_debug);
	trutusb_scan();

	if(trutusb_isattached())
	{
        printf("TRUT connected! Get ready to rambo!\n");
        set_status("TRUT connected! Get ready to rambo!");
    }
	else
	{
        printf("Found no TRUT :(. You must always connect TRUT! :(\n");
        set_status("Found no TRUT :(. You must always connect TRUT! :(");
        return 0;
    }

	if(trutusb_claim())
	{
        printf("Can not claim TRUT :(\n");
        set_status("Can not claim TRUT :(");
        return 0;
	}

	if(trutusb_setmsg(true))
	{
        printf("Failed to enable messages\n");
        set_status("Failed to enable messages");
    }

    init_timeout_timer();
    start();

    return 0;
}

void Engine::init_timeout_timer() {
    timeout_timer.setSingleShot(true);	// We don't want a periodic timer
    connect(&timeout_timer, SIGNAL(timeout()), this, SLOT(timeout_event()));
}

// Called after an usb_event(int) or a timeout_event()
// This is the code that succeeds wait_for_stuff()
void Engine::wakeup() {
    timeout_timer.stop();	// If an event was catched before a timeout, disable the timer
	trutusb_freewaitables(waitables);
	process();
}

// Slot called by QSocketNotifier::activate(int)
void Engine::usb_event(int fid) {
    if (DEBUG>0) std::cout << "Processing USB event from descriptor " << fid << std::endl;
    trutusb_handleevents();
    wakeup();
}

// Slot called by QTimer::timeout()
void Engine::timeout_event() {
    std::cout << "Timeout timeout occured!" << std::endl;
    assert(!timeout_timer.isActive());
    wakeup();
}

void Engine::start() {
	process();
}

void Engine::shut_down() {
	if(trutusb_setmsg(false))
		printf("Failed to disable messages\n");

	if(trutusb_isattached())
		trutusb_release();

	fprintf(stderr, "Good bye\n");
}

void Engine::print_message(std::string str) {
    printf("%s\n", str.c_str());
}

QString Engine::get_file_name(QString str) {
    str=str.replace("\\", "/");
    int index=str.lastIndexOf("/");
    return str.last(str.length()-index-1);
}

void Engine::load(QString filename) {
    elapsed_timer.init();
    refresh_counter();

    bool is_zip=false;
    if (filename.endsWith(".zip")) {
        printf("Engine: Zip file detected\n");
        is_zip=true;
    } else if (filename.endsWith(".tap")) {
        printf("Engine: Tap file detected\n");
    } else {
        fprintf(stderr, "Engine: Error! No valid file was selected\n");
    }

    active_filename=filename;
    //set_status("Loaded "+get_file_name(active_filename));

    ui->fileBox->blockSignals(true);
    ui->fileBox->clear();

    if (is_zip) {
        ZipArchive zip(filename.toStdString());
        if (!zip.isZipFile()) {
            fprintf(stderr,"ERROR! Not a zip file\n");
        }

        std::vector<std::string> strvec=zip.getFilenames(".tap");

        for (int i=0; i<(int)strvec.size(); i++) {
            ui->fileBox->addItem(QString::fromStdString(strvec.at(i)));

        }
    } else {
        ui->fileBox->addItem(get_file_name(filename));
    }

    if (ui->fileBox->count()<=1) {
        ui->fileBox->setEnabled(false);
    } else {
        ui->fileBox->setEnabled(true);
    }
    ui->fileBox->blockSignals(false);

    gui_disable_play_panel();

    // Ok, the combobox is setup at this point

    if (data_buffer != nullptr) free_buffer(data_buffer);

    if (is_zip && ui->fileBox->count()==0) {
        printf("No valid files in Zip archive\n");
        return;
    }

    struct tape_data* tap;

    // If it's a zip file, the item is loaded via on_fileBox_currentIndexChanged()
    // that is triggered automatically
    if (!is_zip) {
        // Load tap file into buffer
        tap=load_tap_file(filename.toStdString().c_str());
        data_buffer = jakob_internal(tap);
        free_tap(tap);
        set_status("Loaded TAP file: "+ui->fileBox->currentText());
    } else {
        // Load the first tap file in the zip archive
        change_zip_item();
    }
    set_status("PRESS PLAY ON TAPE");
    gui_enable_play_panel();
}

void Engine::change_zip_item() {
    assert(!active_filename.isEmpty());
    if (active_filename.endsWith(".zip")) {
        struct tape_data* tap;
        QString selected_item=ui->fileBox->currentText();
        tap=load_tap_in_zip(active_filename, selected_item);
        data_buffer = jakob_internal(tap);
        free_tap(tap);
        set_status("Loaded from ZIP archive: "+ui->fileBox->currentText());
        elapsed_timer.init();
        refresh_counter();
    }
}

struct tape_data* Engine::load_tap_in_zip(QString filename, QString itemname) {
    //struct buffer* buf;
    //buf = (struct buffer*)xmalloc(sizeof(struct buffer));
    struct tape_data* tap;
    struct ZipArchive::zip_buffer zipbuf;

    ZipArchive zip(filename.toStdString());
    zip.extractItem(itemname.toStdString(), &zipbuf);
    struct buffer buf;
    buf.data=zipbuf.data;
    buf.length=(int)zipbuf.length;

    tap=decode_tap(&buf);
    free(zipbuf.data);
    return tap;
}

void Engine::play() {

    if(tap_mode != TapIdle)
    {
        fprintf(stderr,"TRUT64 is not in idle mode.\n");
        set_status("TRUT64 is working. Do not disturb.");
        return;
    }

    assert(data_buffer != nullptr); // Should not happen

    fprintf(stderr,"Buffer time: %llu\n", calculate_buffer_time(data_buffer));

    start_refresh_timer();

    //uint64_t time_position_msec=elapsed_timer.getTime();
    refresh_counter();
    uint64_t time_position_msec=(uint64_t)ui->lcdNumber->intValue()*1000;

    int buffer_index=calculate_buffer_index(data_buffer, time_position_msec*1000);
    fprintf(stderr, "buffer_index: %d\n", buffer_index);
    if(trutusb_xferinit(TRUT_MODE_LOAD, data_buffer->data+buffer_index, data_buffer->length-buffer_index) == 0)
    {
        printf("Initialized data transfer of %d bytes\n", data_buffer->length);
        tap_mode = TapSending;
        gui_disable_load_panel();
    }
    else
    {
        printf("Failed to initialize data transfer\n");
        free_buffer(data_buffer);
        gui_disable_play_panel();
    }
}

void Engine::gui_enable_load_panel() {
    ui->initButton->setEnabled(true);
    ui->loadButton->setEnabled(true);
    ui->setButton->setEnabled(true);
    ui->remButton->setEnabled(true);
    ui->saveButton->setEnabled(true);
    ui->dumpButton->setEnabled(true);
    ui->fileBox->setEnabled(true);
}

void Engine::gui_disable_load_panel() {
    ui->initButton->setEnabled(false);
    ui->loadButton->setEnabled(false);
    ui->setButton->setEnabled(false);
    ui->remButton->setEnabled(false);
    ui->saveButton->setEnabled(false);
    ui->dumpButton->setEnabled(false);
    ui->fileBox->setEnabled(false);
}

void Engine::gui_enable_play_panel() {
    ui->playButton->setEnabled(true);
    ui->stopButton->setEnabled(true);
}

void Engine::gui_disable_play_panel() {
    ui->playButton->setEnabled(false);
    ui->stopButton->setEnabled(false);
}


void Engine::start_refresh_timer() {
    refresh_timer.setSingleShot(false);
    connect(&refresh_timer, SIGNAL(timeout()), this, SLOT(refresh_event()));
    refresh_timer.setInterval(500);
    refresh_timer.start();
}

void Engine::refresh_event() {
    ui->lcdNumber->display((int)elapsed_timer.getTime()/1000);
}

int Engine::calculate_buffer_index(struct buffer* buf, uint64_t time_position_usec) {
    struct buffer_position result;
    calculate_internal_buffer_position(&result, buf, time_position_usec);
    return result.index;
}

uint64_t Engine::calculate_buffer_time(struct buffer* buf) {
    struct buffer_position result;
    uint64_t time_position=std::numeric_limits<uint64_t>::max();
    calculate_internal_buffer_position(&result, buf, time_position);
    return result.elapsed_time;
}

bool Engine::calculate_internal_buffer_position(struct buffer_position* result, struct buffer* buf, uint64_t time_position_usec) {
    uint64_t elapsed_time=0;
    int current_pulse=0;
    int i=0;
    int bufpos=0;
    assert((1.0*AVR_CLK_MHZ/AVR_PRESCALING)==1.0);
    time_position_usec=time_position_usec*(1.0*AVR_CLK_MHZ/AVR_PRESCALING);

    do {
        elapsed_time+=current_pulse;
        bufpos=i;
        current_pulse=buf->data[i++] << 8;
        current_pulse+=buf->data[i++];
        if (current_pulse==0) {
            int overflows=0;
            do {
                overflows++;
                current_pulse=buf->data[i++] << 8;
                current_pulse+=buf->data[i++];
            } while (current_pulse==0);
            current_pulse+=overflows*0xffff;
        }
        current_pulse <<= 1;
    } while (elapsed_time+current_pulse<time_position_usec && i<buf->length);
    if (i==buf->length) {
        result->index=buf->length;
        result->elapsed_time=elapsed_time+current_pulse;
        return false;   // We did not find time_position (but measured the total buffer time)
    }
    result->index=bufpos;
    result->elapsed_time=elapsed_time;
    return true;
}

// Used for debugging purposes. Does not take extension of short pulses
// (that is performed when converting to jakob_internal) into account.
uint64_t Engine::calculate_tape_data_time(struct tape_data* buf) {
    uint64_t elapsed_time=0;
    for (int i=0; i<buf->length; i++) {
        elapsed_time+=floor(0.5+(1.0*buf->data[i]/C64_CLK_MHZ));
    }
    return elapsed_time;
}

void Engine::stop() {

    if(tap_mode == TapIdle)
    {
        printf("No data transfer in progress.\n");
    }
    else if(tap_mode == TapSending)
    {
        if(trutusb_xferactive())
        {
            trutusb_xferclean();
            printf("Cancelled sending tap, wait until completion..\n");
            tap_mode = TapCancellingSending;
        }
        else
        {
            printf("Already finished?!\n");
        }
    }
    else
    {
        printf("Later...\n");
    }
    elapsed_timer.pause();
    refresh_counter();
    gui_enable_load_panel();
}

void Engine::set_status(QString str) {
    ui->plainTextEdit->appendPlainText(str);
}

void Engine::set_counter(int counter_sec) {
    elapsed_timer.init();
    elapsed_timer.setOffsetSeconds(counter_sec);
    refresh_counter();
}

void Engine::refresh_counter() {
    refresh_event();
}

