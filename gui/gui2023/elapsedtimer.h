#ifndef ELAPSEDTIMER_H
#define ELAPSEDTIMER_H

#include <QElapsedTimer>

class ElapsedTimer
{
public:
    ElapsedTimer();
    void init();
    void start();
    void pause();
    uint64_t getTime();
    int getTimeSeconds();
    void setOffsetSeconds(uint64_t offset_sec);
    void setOffset(uint64_t offset_msec);
    uint64_t getOffset();
    int getOffsetSeconds();

private:
    QElapsedTimer elapsed_timer;
    qint64 elapsed_time;
    bool is_active;
    qint64 offset;
};

#endif // ELAPSEDTIMER_H
