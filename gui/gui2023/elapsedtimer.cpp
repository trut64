/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#include "elapsedtimer.h"

ElapsedTimer::ElapsedTimer()
{
    init();
}

void ElapsedTimer::init() {
    is_active=false;
    elapsed_time=0;
    offset=0;
}

void ElapsedTimer::start() {
    if (!is_active) elapsed_timer.start();
    is_active=true;
}

void ElapsedTimer::pause() {
    if (is_active) {
        elapsed_time+=elapsed_timer.elapsed();
    }
    is_active=false;
}

uint64_t ElapsedTimer::getTime() {
    if (is_active) {
        return offset+elapsed_time+elapsed_timer.elapsed();
    } else {
        return offset+elapsed_time;
    }
}

int ElapsedTimer::getTimeSeconds() {
    return getTime()/1000;
}

void ElapsedTimer::setOffset(uint64_t offset_msec) {
    offset=offset_msec;
}

void ElapsedTimer::setOffsetSeconds(uint64_t offset_sec) {
    setOffset(offset_sec*1000);
}

uint64_t ElapsedTimer::getOffset() {
    return offset;
}

int ElapsedTimer::getOffsetSeconds() {
    return offset/1000000;
}

