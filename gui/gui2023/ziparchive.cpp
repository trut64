/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

// Simple C++ wrapper for minizip
// Uses std

#include "ziparchive.h"
#include <minizip/unzip.h>

ZipArchive::ZipArchive(std::string zipname)
{
    filename=zipname;
}

bool ZipArchive::isZipFile() {
    unzFile zipfile=unzOpen(filename.c_str());
    if (!zipfile) {
        return false;
    }
    return true;
}

bool ZipArchive::strEndsWith(const std::string &str, const std::string &suffix) {
    if (str.length()<suffix.length()) {
        return false;
    }
    return str.compare(str.length()-suffix.length(), suffix.length(), suffix) == 0;
}

/// return list of filenames in zip archive
std::vector<std::string> ZipArchive::getFilenames(const std::string suffix) {
    std::vector<std::string> filenames;
    unzFile zip=unzOpen(filename.c_str());
    if (!zip) {
        return filenames;   // Return empty list
    }
    if (UNZ_OK == unzGoToFirstFile(zip)){
        do {
            char itemname[MAX_FILENAME_LENGTH];
            if (UNZ_OK==unzGetCurrentFileInfo(zip, NULL, itemname, sizeof(itemname), NULL, 0, NULL, 0)) {
                std::string itemstr=std::string(itemname);
                if (strEndsWith(tolower(itemstr), tolower(suffix))) {
                    filenames.push_back(std::string(itemname));
                }
            }
        } while (UNZ_OK == unzGoToNextFile(zip));
    }
    unzClose(zip);
    return filenames;
}

 bool ZipArchive::extractItem(const std::string itemName, struct zip_buffer* zipbuf) {

    unz_file_info info;
    int err;

    unzFile zip = unzOpen(filename.c_str());
    if (!zip) {
        fprintf(stderr,"ZipArchive: Could not open zip file\n");
        return false;
    }

    err = unzLocateFile(zip, itemName.c_str(), 0);
    if (err != UNZ_OK) {
        fprintf(stderr, "ZipArchive: Could not locate file in zip archive\n");
        return false;
    }

    err = unzOpenCurrentFile(zip);
    if (err != UNZ_OK) {
        fprintf(stderr,"ZipArchive: Could not open located file in zip archive\n");
        return false;
    }

    unzGetCurrentFileInfo(zip, &info, NULL, 0, NULL, 0, NULL, 0);
    printf("ZipArchive: Length of extracted file: %lu\n", info.uncompressed_size);
    zipbuf->length=info.uncompressed_size;
    zipbuf->data=(uint8_t*)malloc(info.uncompressed_size);
    if (zipbuf->data == NULL) {
        printf("ZipArchive: Error, cannot allocate tap buffer\n");
        return false;
    }

    bool return_value=true;
    int bytes=unzReadCurrentFile(zip, zipbuf->data, info.uncompressed_size);
    if (bytes<0) {
        fprintf(stderr, "ZipArchive: Error reading from file in zip archive\n");
        free(zipbuf->data);
        return_value=false;
    }

    unzCloseCurrentFile(zip);
    unzClose(zip);

    return return_value;
}

 std::string ZipArchive::tolower(std::string str) {
    std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c){ return std::tolower(c); });
    return str;
 }
