#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_initButton_clicked();

    void on_loadButton_clicked();

    void on_playButton_clicked();

    void on_stopButton_clicked();

    void on_fileBox_currentIndexChanged(int index);

    void on_setButton_clicked();

    void on_memButton_clicked();

    void on_remButton_clicked();

    void on_saveButton_clicked();

    void on_dumpButton_clicked();


private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
