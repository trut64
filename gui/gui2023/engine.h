/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QSocketNotifier>
#include <QTimer>
#include <QVector>
#include "elapsedtimer.h"
#include "ui_mainwindow.h"
#include "ziparchive.h"


class Engine : public QObject {
	Q_OBJECT;

private:
    const int DEBUG=0;
    const bool GUI_GHOST_BUTTONS=false;

    QVector<QSocketNotifier*> notify_read;
    QVector<QSocketNotifier*> notify_write;
    QTimer timeout_timer;
    QTimer refresh_timer;

	void wait_for_stuff();
	void process();
	void wakeup();
    void init_timeout_timer();
    Ui::MainWindow* ui;
    void examine_msg(const char* data, int size);
    QString get_file_name(QString str);
    struct buffer* data_buffer;
    void gui_enable_load_panel();
    void gui_disable_load_panel();
    void gui_enable_play_panel();
    void gui_disable_play_panel();
    struct tape_data* load_tap_in_zip(QString filename, QString itemname);
    QString active_filename;


public:
    Engine();
    int init();
	void start();
	void shut_down();
    void load(QString filename);
    void print_message(std::string str);
    void play();
    void stop();
    void refresh_counter();

    ElapsedTimer elapsed_timer; // Change to private
    void start_refresh_timer();
    void set_gui(Ui::MainWindow* my_ui);
    void change_zip_item();
    void set_status(QString str);
    void set_counter(int counter_sec);
    void dummy();

    struct buffer_position
    {
        int index;
        uint64_t elapsed_time;
    };
    int calculate_buffer_index(struct buffer* buf, uint64_t time_position_usec);
    uint64_t calculate_buffer_time(struct buffer* buf);
    bool calculate_internal_buffer_position(struct buffer_position* result, struct buffer* buf, uint64_t time_position_usec);
    uint64_t calculate_tape_data_time(struct tape_data* buf);


signals:
	void application_quit_signal();

public slots:
	void usb_event(int fid);
	void timeout_event();
    void refresh_event();
};

#endif

