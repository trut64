/*
 * Copyright (C) 2023 Anton Blad
 * Copyright (C) 2023 Fredrik Kuivinen
 * Copyright (C) 2023 Jakob Rosén
 *
 * This file is licensed under GPL v2.
 */

#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
